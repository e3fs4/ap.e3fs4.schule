# Wilkommen

Dieses Wiki soll als Zusammenfassung der Abschlussprüfung Teil 2 für Auszubildende Fachinformatiker der Fachrichtung Systemintegrationen dienen.

Das Wiki ist Open Source, du kannst also gerne mitmachen und etwas hinzufügen, korrigieren oder einfach das Wiki lokal klonen.

Du findest den Quellcode und alle weiteren Informationen unter [https://gitlab.com/e3fs4/ap.e3fs4.schule](https://gitlab.com/e3fs4/ap.e3fs4.schule).

Möchtest du auch am Wiki mitarbeiten? Dann erstelle einfach einen Gitlab-Account unter [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up) oder logge dich mit einem bestehenden Account ein und lass es uns unter [webmaster@e3fs4.schule](mailto:webmaster@e3fs4.schule) wissen.