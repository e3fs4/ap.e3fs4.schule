---
tags:
- FISI
- FIAE
---

# Schaubildanalyse

**Schaubilder präsentieren Informationen grafisch.** 
Sie können Bilder, Symbole, Diagramme und Textelemente enthalten. 
Als Diagramme (z.B. Balken-, Säulen- oder Tortendiagramm) kommen Schaubilder oft in Studien, Fernsehbeiträgen und Zeitungsberichten vor.
Dann stellen sie Daten zu einem bestimmten Thema anschaulich dar.
Die Daten werden durch Umfragen oder Beobachtungen zusammengetragen.
Ein solches Verfahren nennt man auch Datenerhebung.
Außerdem gibt es Schaubilder, die einen Prozess oder Ablauf verdeutlichen (Skizzen, Zeichnungen oder Bilder).
Häufig werden Schaubilder verwendet, um Vergleiche zu verdeutlichen, Verteilungen anzuzeigen oder Entwicklungen zu veranschaulichen.

## Methodische Schritte

1. Die **Beschreibung** enthält alle notwendigen Angaben zur optischen Darstellung des Schaubilds. Z.B.: Aufbau, Farben, Diagrammart/en (Zahlentabellen; Säulen-, Balken-, Kurven- oder Tortendiagramm), Bilder/Symbole, Titel und Untertitel, Vordergrund und Hintergrund und Art des Zahlenmaterials.  
!!! tip "Tipp:"
    Man kann sich vorstellen, dass man das Schaubild jemandem am Telefon erklärt und diese Person das Schaubild nur mit der Beschreibung nachzeichnen soll.
2. Die **Analyse** ist die inhaltliche Untersuchung des Schaubilds. Dabei werden die Hauptaussagen des Schaubilds strukturiert (z.B. von Spitzenwerten ausgehend) erläutert.Dazu gehört, auffällige Werte zu vergleichen oder gegenüberzustellen. Hier sollen die Werte noch nicht interpretiert werden!
3. Die Deutung oder Interpretation ist die Auslegung des Schaubildinhalts. Dabei werden die zentralen Inhalte erklärt. Die zentralen Werte werden miteinander und mit dem thematischen Kontext in Zusammenhang gebracht (Ursachen, Folgen...). Eine bloße Nennung der Werte ist unbedingt zu vermeiden!

??? abstract "Checkliste"

    **Beschreibung**

    - [x] Habe ich das Schaubild mit seinen Bestandteilen umfassend beschrieben?
        - [x] Habe ich die Quelle, das Thema und den Zeitraum des Schaubilds genannt?
        - [x] Habe ich die Diagrammart(en), Symbole, Bilder, Farben, Arten des Zahlenmaterials, Legende, Beschriftungen und den Hintergrund genannt?

    **Analyse**

    - [x] Habe ich die zentralen Aussagen des Schaubilds strukturiert erläutert und in einen Zusammenhang gebracht?
        - [x] Hauptaussagen: Habe ich die wichtigen Daten und Inhalte des Schaubilds genannt und durch die Werte und Zahlen ergänzt?
        - [x] Struktur: Habe ich von den auffälligen Werten ausgehend beschrieben?
        - [x] Zusammenhang: Habe ich die Inhalte gegenübergestellt und miteinander verglichen?
    
    **Deutung/Interpretation**

    - [x] Habe ich die wichtigsten Inhalte des Schaubilds erklärt und interpretiert?
        - [x] Habe ich erklärt, wie die Ergebnisse begründet werden können?
        - [x] Habe ich erklärt, welche Zusammenhänge zwischen den Ergebnissen des Schaubilds und anderen (gesellschaftlichen) Entwicklungen bestehen (geht über das Schaubild hinaus!)?
    - [x] Habe ich überprüft und erklärt, ob und inwiefern die Darstellung den Betrachter manipuliert?
        - [x] Fehlen in der Darstellung wichtige Daten oder Erklärungen? Wird nur ein Ausschnitt dargestellt?