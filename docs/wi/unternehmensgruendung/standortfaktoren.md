---
tags:
- FISI
- FIAE
---

# Standortfaktoren

Die Standortwahl gehört zu den Entscheidungen mit sehr langfristigen Auswirkungen auf das Wohlergehen des Unternehmens.
Eine Fehlentscheidung diesbezüglich kann zu einem späteren Zeitpunkt nur mit erheblichen Kosten korrigiert werden.
Im schlimmsten Fall kann sie das Unternehmen sogar in seiner Existenz bedrohen.  
Zu Standortfaktoren gehören alle Gesichtspunkte (Faktoren), die den Erfolg des Unternehmens beeinflussen.
Sie lassen sich in quantitative (messbare) und qualitative(subjektive Einschätzung) Faktoren unterteilen.

## Quantitative Standortfaktoren

Das sind Standortfaktoren, deren Beitrag zum Unternehmenserfolg direkt gemessen werden kann.

| Standortfaktor | Beispiele |
| -------------- | --------- |
| Produktionskosten | Grundstückkosten; Kosten für die Errichtung der Gebäude; Höhe der Personalkosten; ... |
| Abgaben/Fördermaßnahmen | Steuern (z.B. Grundsteuer); Umweltabgaben; ... | 
| Auflagen | Umweltschutzauflagen; Dauer von Genehmigungsverfahren; ... |
| Absatzkosten/-möglichkeiten | Transportkosten der Produkte vom Standort zum Absatzmarkt; Regionales Marktvolumen; ... |

## Qualitative Standortfaktoren

| Standortfaktor | Beispiele |
| -------------- | --------- |
| Produktionsumfeld | Grundstück (Lage, Bodenbeschaffenheit, Bebauungsvorschriften, Ausdehnungsmöglichkeiten); Verkehrsanbindung (Personen- und Güterverkehrsnetz, Speditionsunternehmen); Arbeitskräftebeschaffung (Bevölkerungsstruktur und –Ausbildung) |
| Absatzbereich | Kaufkraft der Bewohner; Stärke der regionalen Konkurrenz |
| Infrastruktur | Technologie- bzw. Handelszentren, Bildungs- und Kultureinrichtungen, Universität); Energieversorgung |

## Standortanalyse

<table>
    <thead>
        <tr>
            <th rowspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Ausgewählte Standortfaktoren</th>
            <th rowspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Gewichtung</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Standord A</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Standord B</th>
        </tr>
        <tr>
            <td  style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Ungewichtete Punkte</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Gewichtete Punkte</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Ungewichtete Punkte</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Gewichtete Punkte</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">(1)</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">(2)</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">(3)</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">(4)=(2)*(3)</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">(5)</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">(6)=(2)*(5)</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">Mietkosten</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">...</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">Summe der Punkte</td>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center;">100</td>
            <td style="border:1px solid #b3adad; padding:5px; background: #b3adad;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px; background: #b3adad;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
    </tbody>
</table>

Bei der Bewertungstabelle werden zunächst Auswahlkriterien für die Standortwahl festgelegt (z.B. Mietkosten, Verkehrsanbindung usw.) (1).
Im Anschluss werden den Auswahlkriterien Gewichtungen zugeordnet (2).
Je bedeutender ein Standortfaktor eingeschätzt wird, desto höher wird er gewichtet (z.B. mit 30).
In der Summe müssen die Gewichtungen 100 ergeben.
Danach werden die ausgewählten Standortfaktoren einzeln dahingehend analysiert, inwieweit sie die Auswahlkriterien erfüllen.
Hierfür werden Punkte vergeben, z.B. „5“ für hohe Zielerfüllung, „0“ für keine Zielerfüllung (3).
Durch Multiplikation der Gewichtungen mit den einzelnen Punkten erhält man je Standortfaktor die gewichteten Punkte (z.B. 30*5 = 150).
Ausgewählt wird jener Standort, dessen Summe der gewichteten Punkte maximal ist.