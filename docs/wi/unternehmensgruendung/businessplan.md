---
tags:
- FISI
- FIAE
---

# Businessplan

Der Businessplan (englisch für „Geschäftsplan“) ist die ausgearbeitete und ausformulierte Zusammenfassung Ihrer Geschäftsidee.
Im Businessplan erarbeiten und beschreiben Sie z. B., wie Ihre Geschäftsidee funktionieren soll, an wen sich Ihre Geschäftsidee richtet, wo die Chancen und Risiken liegen und ob sich Ihre Geschäftsidee voraussichtlich lohnt.

## Warum erstellt man einen Businessplan?

Mit einem gut ausgearbeiteten Businessplan, bei dem Sie die kritischen Bereiche Ihrer Geschäftsidee prüfen, beugen Sie einem vorzeitigen Aus Ihrer Existenzgründung durch Planungsmängel – einer der Hauptgründe für ein Scheitern innerhalb der ersten 3 Jahre – vor.
Außerdem übernimmt der Businessplan folgende weitere wichtige Funktionen:

* **Businessplan als Machbarkeitsstudie:** Wenn Sie den Businessplan erstellen, setzen Sie sich kritisch mit Ihrer Geschäftsidee auseinander, um diese auf ihre Machbarkeit (Ist die Idee umsetzbar?) und ihre Wirtschaftlichkeit (Lohnt es sich?) hin zu prüfen.
* **Businessplan als Informationsdokument:** Der Businessplan dient als Informationsdokument für potenzielle Investoren und/oder Förderinstitute, Banken, private Eigenkapital- und/oder Darlehensgeber und wird von jedem potenziellen Geldgeber verlangt. Aber auch für mögliche Partner ist der Geschäftsplan ein wichtiges Dokument.
* **Businessplan als Wegweiser:** Ist die Idee umsetzbar und macht sie wirtschaftlich Sinn, so geht es an die Umsetzung. Hierbei hilft der Geschäftsplan als wichtiges Leitinstrument.

## Für wen erstellt man einen Businessplan?

Den Businessplan erstellen Sie in erster Linie für sich selbst.
Aber es gibt auch eine Reihe von anderen Anspruchsgruppen, die einen Businessplan verlangen:

* **Businessplan für das Arbeitsamt:** Wenn Sie sich aus der Arbeitslosigkeit selbstständig machen möchten, so verlangt das zuständige Arbeitsamt einen Businessplan für die Beantragung des Gründungszuschusses.
* **Businessplan für die Geldgeber:** Die meisten Geldgeber, aber auf jeden Fall alle Banken und Eigenkapitalgeber, verlangen einen Businessplan. Vor allem auf den Finanzplan-Teil des Businessplanes sollten Sie achten, der in der Regel besonders kritisch durchleuchtet wird.
* **Businessplan für Geschäftspartner:** Es ist möglich, dass zukünftige Geschäftspartner (z. B. Lieferanten, die Ihnen beispielsweise eine längere Zahlungsfrist einräumen) eine Kurzform des Geschäftsplanes sehen möchten.

