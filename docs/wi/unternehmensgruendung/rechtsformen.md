---
tags:
- FISI
- FIAE
---

# Rechtsformen

Eine Rechtsform gibt den rechtlichen Rahmen eines Unternehmens vor.
Es werden unter anderem folgende Fragen geklärt:

* Wer bringt das erforderliche Kapital auf?
* Wer haftet für die Schulden?
* In welchem Umfang wird gehaftet?
* Wer ist berechtigt, die Unternehmung zu leiten und nach außen zu vertreten?
* Wer erhält den wirtschaftlichen Gewinn?

## Übersicht

<table style="position: relative;">
    <thead style="position: sticky; top: 0;">
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Rechtsform</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Mindestkapital</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Gründer minimum</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Haftung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Entscheidungsbefugnis / Vertretung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Formalitäten / Kosten</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Eintragung in das Handelsregister</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Notar erforderlich</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Vertrag / Formvorschriften</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>Einzelunternehmen</b><br>
                (Nichtkaufleute / Kleingewerbetreibende)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>kein festes Kapital</li>
                    <li>keine Mindesteinlage vorgeschrieben</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">unbeschränkt mit Geschäfts- und Privatvermögen</td>
            <td style="border:1px solid #b3adad; padding:5px;">Alleinentscheidung des Inhabers</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung / gering</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>e.K.</b><br>
                eingetragener Kaufmann (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>kein festes Kapital</li>
                    <li>keine Mindesteinlage vorgeschrieben</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">unbeschränkt mit Geschäfts- und Privatvermögen</td>
            <td style="border:1px solid #b3adad; padding:5px;">Alleinentscheidung des Inhabers</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung und Eintragung in das Handelsregister / relativ gering</td>
            <td style="border:1px solid #b3adad; padding:5px;">ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>GbR</b><br>
                Gesellschaft Bürgerlichen Rechts (Nichtkaufleute / Kleingewerbetreibende)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>kein festes Kapital</li>
                    <li>keine Mindesteinlage vorgeschrieben</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 2</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gesellschaft und Gesellschafter (auch mit Privatvermögen) für Geschäftsschulden, gesamtschuldnerische Haftung</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gemeinsame Geschäftsführung und Vertretung durch alle Gesellschafter, sofern im Gesellschaftsvertrag nicht anders geregelt</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung / gering</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
            <td style="border:1px solid #b3adad; padding:5px;">Nein</td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag nicht zwingend, aber zu empfehlen</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>OHG</b><br>
                Offene Handelsgesellschaft (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>kein festes Kapital</li>
                    <li>keine Mindesteinlage vorgeschrieben</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 2</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gesellschaft und Gesellschafter (auch mit Privatvermögen) für Geschäftsschulden, gesamtschuldnerische Haftung</td>
            <td style="border:1px solid #b3adad; padding:5px;">Einzelgeschäftsführung / Einzelvertretungsmacht jedes Gesellschafters, sofern im Gesellschaftsvertrag nichts anderes geregelt</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung und Eintragung ins Handelsregister / relativ gering</td>
            <td style="border:1px solid #b3adad; padding:5px;">ja</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag nicht zwingend, aber zu empfehlen</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>KG</b><br>
                Kommanditgesellschaft (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>kein festes Kapital</li>
                    <li>keine Mindesteinlage vorgeschrieben, jedoch Kommanditeinlagen für Kommanditisten (Höhe beliebig)</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 2</td>
            <td style="border:1px solid #b3adad; padding:5px;">Komplementäre (persönlich haftende Gesellschafter) unbeschränkt, Kommanditisten in Höhe der Einlage</td>
            <td style="border:1px solid #b3adad; padding:5px;">Grds. pers. haftende Gesellschafter; in besonderen Fällen Beteiligung der Kommanditisten erforderlich</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung und Eintragung ins Handelsregister / relativ gering</td>
            <td style="border:1px solid #b3adad; padding:5px;">ja</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag nicht zwingend, aber zu empfehlen</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>GmbH</b><br>
                Gesellschaft mit beschränkter Haftung (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Mindestkapital: 25.000 Euro</li>
                    <li>mindestens 12.500 Euro müssen in bar oder in Sachwerten bei Gründung eingezahlt werden</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 1</td>
            <td style="border:1px solid #b3adad; padding:5px;">nur mit Gesellschaftsvermögen (Haftungsbeschränkung tritt erst nach Eintragung in das Handelsregister ein), ggf. persöhnliche Haftung des Geschäftsführers</td>
            <td style="border:1px solid #b3adad; padding:5px;">Geschäftsführer, Geschäftspolitik: Gesellschafterversammlung, sofern vorhanden Aufsichtsrat, Bestellung von Prokurist möglich</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung und Anmeldung zur Eintragungin das Handelsregister, insgesamt umfangreiche Formalitäten / hohe Gründungskosten (Erleichterung bei Verwendung des notariellen Musterprotokolls)</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>UG (haftungsbeschränkt)</b><br>
                Unternehmergesellschaft (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Mindestkapital 1 Euro pro Gesellschafter</li>
                    <li>vollständige Einzahlung bei Gründung erforderlich</li>
                    <li>nur Bargründung möglich</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 1</td>
            <td style="border:1px solid #b3adad; padding:5px;">wie GmbH</td>
            <td style="border:1px solid #b3adad; padding:5px;">wie GmbH</td>
            <td style="border:1px solid #b3adad; padding:5px;">wie GmbH</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>AG</b><br>
                Aktiengesellschaft (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Mindestgrundkapital: 50.000 Euro</li>
                    <li>mind. 1/4 des Nennbetrages der Aktien müssen bar oder in Sachwerten bei Gründung eingezahlt werden</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 1</td>
            <td style="border:1px solid #b3adad; padding:5px;">nur mit Gesellschaftsvermögen; ggf. persöhnliche Haftung des Vorstandes</td>
            <td style="border:1px solid #b3adad; padding:5px;">Vorstand; Geschäftspolitik: Aufsichtsrat, Hauptversammlung</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung und Eintragung ins Handelsregister, sehr umfangreiche Formalitäten / hohe Gründungskosten</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ja</td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">
                <b>eG</b><br>
                eingetragene Genossenschaft (Kaufmann)
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>kein festes Kapital</li>
                    <li>keine Mindesteinlage vorgeschrieben</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">mind. 3</td>
            <td style="border:1px solid #b3adad; padding:5px;">nur mit Gesellschaftsvermögen</td>
            <td style="border:1px solid #b3adad; padding:5px;">Vorstand; Geschäftspolitik: Aufsichtsrat, Generalversammlung</td>
            <td style="border:1px solid #b3adad; padding:5px;">Gewerbeanmeldung und Eintragung ins Genossenschaftregister</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">schriftlicher Gesellschaftsvertrag</td>
        </tr>
    </tbody>
</table>

## Einzelunternehmung

Die Einzelunternehmung ist mit über zwei Millionen Gründungen die beliebteste Rechtsform in Deutschland.
Typische Einzelunternehmungen sind z.B. kleinere Handwerksbetriebe, landwirtschaftliche Betriebe oder Einzelhandelsbetriebe.  
Einzelunternehmungen lassen sich grob in zwei Arten unterteilen:

* Einzelne natürliche Personen, die einer selbständigen Tätigkeit nachgehen, z.B. Freiberufler, Kleingewerbetreibende sowie Land- und Forstwirte
* Unternehmen eines eingetragenes Kauf-mannes/-frau im Sinne des Handelsgesetzbuches

Wie es der Name schon sagt, steh bei der Einzelunternehmung **ein einzelner** Unternehmer im Vordergrund, der das Unternehmen alleine leitet.

### Gründung und Startkapital

Die Gründung unterscheidet sich hinsichtlich den Freiberuflern, Kleingewerbetreibenden und den gewerbetreibenden Kauf-männern/-frauen. Grundsätzlich ist aber ein Inhaber von nöten, der das Unternehmen leitet.  
Start- bzw. Mindestkapital ist nicht notwendig, für die Eintragung ins Handelsregister bei den Kaufleuten fallen alledings gebühren an, die von Bundesland zu Bundesland verschieden sind.

### Namensgebung

Grundsätzlich wird der Name einer Einzelunternehmung nach dem Vor- und Nachnamen der Person gebildet.
Zusätzlich kann noch ein Branchenname hinzugefügt werden (z.B. Blumenhandel Max Mustermann).
Eingetragene Kaufleute dürfen auch einen Phantasienamen nutzen.
Zudem erhalten sie einen Rechtsformzusatz, nämlich "eingetragener Kaufmann" (e.K.).

### Haftung, Gewinn und Verlust

Die Haftung ist der größte Nachteil einer Einzelunternehmung.
Der Inhaber der Unternehmung haftet nämlich alleine mit seinem Geschäfts- und Privatvermögen.
Allerdings erhält er auch den gesamten Gewinn seines Unternehmens und trägt die Verluste des Unternhemens.

### Pro & Kontra

| Vorteile | Nachteile |
| -------- | --------- |
| Gewinne gehören uneingeschränkt dem Inhaber | uneingeschränkte Haftung |
| Inhaber kann alleine Entscheidungen treffen | Buchführungspflichten (nur als Kaufmann) |
| Kein Startkapital notwendig | "Einzelkämpfer" |
| Hohe Kreditwürdigkeit | |
