# Arbeitsvertrag

![Zustandekommen des Arbeitsvertrages](../img/wi/arbeitsvertrag.png)

## Beendigung

![Beendigung eines Arbeitsvertrages](../img/wi/beendigung_arbeitsvertrag.jpg)

![Allgemeiner Kündigungsschutz](../img/wi/kuendigung.jpg)

![Besonderer Kündigungsschutz](../img/wi/besondere_kuendigung.jpg)