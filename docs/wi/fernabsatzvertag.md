# Fernabsatzverträge

## Definition

Verträge über

* die Lieferung von Waren oder Erbringung von Dienstleistungen,
* zwischen einem Unternehmen und einem Verbraucher,
* die **ausschließlich** durch Fernkommunikationmittel (Katalog, Telefon, E-Mails, etc.) geschlossen werden.
* Ausnahmen sind unter anderem Verträge über Fernunterricht, Versicherungsverträge, Anrufe bei einem "Pizzaservice", Hotel- oder Konzertkartenreservierungen und Taxibestellungen (§ 312b BGB)

## Informationspflicht durch den Unternehmer

Unternehmer muss **vor Vertragsschluss bzw. Lieferung** klar und verständlich insbesondere informieren über:

* Wesentliche Merkmale der Ware bzw. Leistung
* Gewährleistung- und Garantiebedingungen
* Vertragslaufzeit und Kündigungsbedingungen
* Preis, einschließlich aller Bestandteile wie Steuern und Liefer- und Versandkosten
* Zahlungs- und Lieferungsbedingungen
* Seine Identität und Kontaktdaten
* 14-tägiges Widerrufsrecht
* die Wiederrufsbedingungen, insbesondere darüber, wer die Kosten der Rücksendung trägt.

Das Wiederrufsrecht **gilt unter anderem nicht** bei individuellen Kundenanfertigungen, Audio-, Videoaufzeichnungen oder Software, wenn sie entsiegelt wurden und bei Waren aus Versteigerungen. "Internet-Auktionen", z.B. bei ebay, gelten laut Bundesgerichtshofes (Az.:VII ZR 375/03) in der Regel nicht als Versteigerungen im obigen Sinn mit der Folge, dass auch hier ein Wiederrufsrecht des Verbrauchers besteht, sofern der Verkäufer gewerblich tätig ist.

## Weiter Anforderungen

* Bestell-Buttons müssen an Worten wie "kostenpflichtig kaufen" klar erkennbar sein.
* Die Anbieter müssen ein gängiges und zumutbares kostenfreies Bezahlverfahren anbieten. Bei mit Zusatzkosten verbunden Bezahlverfahren (z.B. Abbuchungen über die Kreditkarte) dürfen nur die dem Anbieter tatsächlich entstehenden Kosten verlangt werden
* Für Servive-Hotlines, die zur Klärung von Fragen aus einem geschlossenen Vertrag eingerichtet sind, dürfen keine zusätzlcihen Entgelte verlangt werden, die über die normalen Telefongebühren hinausgehen.
* Bestellungen mit voreingetragenen Häckchen oder Kreuzen über Internet sind unwirksam. Kunden müssen jede Leistung ausdrücklich bestellen.

## Widerrufsrecht

Grundsätzlich ist jeder abgeschlossene Vertrag einzuhalten, es gilt der Grundsatz "pacta sunt servanda".
Danach sind Verträge für beide Seiten verbindlich.
Aus Gründen des Verbraucherschutzes können bestimmte Willenserklärungen jedoch widerrufen werden.
Zunächst schwebend wirksam, werden die Verträge nach dem Widerruf nichtig.
Die Frist zum Widerruf für den Verbraucher beträgt üblicherweise 14 Tage.

### Widerrufbare Willenserklärungen

#### Außerhalb von Geschäftsräumen geschlossene Verträge (§ 312b BGB)

Erfasst werden Verträge, die

* an einem Ort geschlossen werden, der kein Geschäftsraum des Unternehmers ist, (z.B. in der Wohnung, am Arbeitsplatz)
* im Geschäftsraum des Unternehmers geschlossen werden oder per Fernkommunikation zustande kommen, wobei Verbraucher und Unternehmer sich aber vorher außerhalb der Geschäftsräume persöhnlich und individuell getroffen und miteinander gesprochen haben, z.B. auf öffentlichen Wegen und Plätzen, in Verkehrsmitteln oder Restaurants
* auf Verkaufs- und Werbefahrten ("Kaffeefahrten") geschlossen werden, sofern dieser Ausflug vom Unternehmer (mit-)organisiert wurde

Ein Widerrufsrecht besteht nicht, wenn der Kaufgegenstand nicht mehr als 40€ kostet und direkt bezahlt und mitgenommen wird (§ 312 Abs. 2 Nr. 12)

#### Fernabsatzvertag (§312c BGB)

Geschäft zwischen Unternehmer und Verbraucher, wenn es durch Fernkommunikationsmittel unter gleichzeitiger körperlicher Abwesenheit der Vertragspartner zustande gekommen ist.
Gleichgestellt sind Geschäfte, die über Online-Auktionshäusern (bspw. eBay) zustande kommen.  
Keine Fernabsatzgeschäfte sind unter anderem Verträge über die Lieferung von Lebensmitteln an den Wohnsitz eines Verbrauchers im Rahmen von regelmäßigen Fahrten, die Erbringung von Dienstleistungen in den Bereichen Unterbringung, Beförderung, Lieferung von Speisen und Getränken sowie der Freizeitgestaltung (§ 312 BGB)

#### Verbraucherdarlehensvertrag (§ 491 i.V.m § 495 BGB)

Gewährung von Darlehen - nicht Überziehungskredite - an private Verbraucher über mehr als 200,00€.
Weitere Ausnahmen:
Arbeitgeberdarlehen, Pfandkredit, Vertragslaufzeit bis drei Monate und geringe Kosten

#### Verbundene Verträge (§ 358 BGB)

Kaufvertrag, der mit einer Finanzierung verbunden ist, wobei beide Verträge eine wirtschaftliche Einheit bilden.
Dies ist der Fall, wenn der Unternehmer selbst die Finanzierung übernimmt oder bei der Finanzierung durch einen Dritten mitwirkt.

#### Ratenlieferungsvertrag (§ 510 BGB)

* Lieferung zusammengehörender Sachen in Teillieferungen und Zahlung ratenweise (Bände eines Lexikons)
* regelmäßige Lieferung gleichartiger Sachen (Abonnements)
* Vertrag über die regelmäßige Abnahme von Waren (Buchclubs)

#### Versicherungsverträge (§ 8 nd § 152 Abs. 1 VVG)

