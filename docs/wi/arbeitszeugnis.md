# Arbeitszeugnis

Nach § 109 Gewerbeordnung muss ein Zeugnis mindestens Art und Dauer der Tätigkeit also ohne Angaben zur Leistung und Führung bzw. Verhalten enthalten (= einfaches Zeugnis).

Arbeitnehmer haben nach Beendigung eines Arbeitsverhältnisses Anspruch auf ein qualifiziertes Arbeitszeugnis, in dem neben dem Inhalt des einfachen Zeugnisses ihre Tätigkeiten umfassend wiedergegeben und ihre Leistung und Führung gerecht beurteilt wird.  
Win Zeugnis muss zudem klar und verständlich formuliert sein.

| Schulnoten | Arbeitszeugnis |
| ---------- | -------------- |
| sehr gut | stets / immer / jederzeit zur vollsten Zufriedenheit | 
| gut | stets / immer | jederzeit zur vollen Zufriedenheit; zur vollsten Zufriedenheit |
| befriedigend | zur vollen Zufriedenheit |
| ausreichend | zu unserer Zufriedenheit |
| mangelhaft | im Allgemeinen zu unserer Zufriedenheit |
| ungenügend | hat sich bemüht... |