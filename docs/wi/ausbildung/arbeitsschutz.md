# Arbeitsschutz

* **Arbeitszeitgesetz** (max. 10 h/Tag, wenn in 6 Monaten im Durchschnitt 8 h werktäglich nicht überschritten; Pause: 6-9 h = 30 min. ab 9h: 45 min.)
* **Bundesurlaubsgesetz** ( mind. 24 Werktage = 20 Arbeitstage, AG bestimmt Urlaub, aber Wünsche vom AN berücksichtigen)
* **Entgeltfortzahlungsgesetz** (Weiterzahlung für 6 Wochen, danach übernimmt Krankenkasse 70% vom Brutto)
* **Kündigungsschutzgesetz** ( Gründe: Personen-, Verhaltens-, Betriebsbedingt, sonst „sozial ungerechtfertigt.  Sozialauswahl beachten)
* **Mutterschutzgesetz** (Beschäftigungsverbot: 6 Wochen vor und 8 Wochen nach Entbindung, Kündigungsverbot: Während der Schwangerschaft und 4 Monate nach Entbindung)
* **Elternzeitgesetz** (Möglichkeit zur Kind Betreuung, Erhalt von Elterngeld und währenddessen besonderer Kündigungsschutz)
* **Jugendarbeitsschutzgesetz:**
    * **Arbeitszeit:** Höchstens 8 Std. am Tag (Ausnahme: 8 1/2 Std. bei Verkürzung an einzelnen Werktagen), 40 Std. in der Woche, nur an 5 Tagen pro Woche soll gearbeitet werden (§ 8 JArbSchG)
    * **Ruhepausen:** Bei einer Arbeitszeit von 4 1/2 bis 6 Std.: 30 Min., bei mehr als 6 Std. Arbeitszeit 60 Min. (§ 11 JArbSchG)
    * **Schichtzeit:** Arbeitszeit + Pausen: höchstens 10 Std. (§ 12 JArbSchG)
    * **Freizeit:** Mindestens 12 Std. zwischen 2 Arbeitstagen (§ 13 JArbSchG)
    * **Arbeitsbeginn:** Keine Beschäftigung vor 6 Uhr morgens. Ausnahmen: In Bäckereien, Konditoreien und in der Landwirschaft dürfen 16-Jährige bereits ab 5 Uhr arbeiten, 17-Jährige in Bäckereien ab 4 Uhr (§ 14 JArbSchG)
    * **Arbeitsende:** Keine Beschäftigung nach 20 Uhr. Ausnahmen: 16-Jährige dürfen in Gaststätten bis 22 Uhr und in mehrschichtigen Betrieben bis 23 Uhr arbeiten (§ 14 JArbSchG)
    * **Urlaub:** Jugendliche, die zu Beginn des Kalenderjahres noch nicht 16 Jahre alt sind: 30 Werktage; noch nicht 17 Jahre alt sind: 27 Werktage; noch nicht 18 Jahre alt sind: 25 Werktage (§ 19 JArbSchG)
    * **Berufsschulzeit:** Der Jugendliche muss hierzu freigestellt werden. Mehr als 5 Unterrichtsstunden entsprechen 1 Arbeitstag (§ 9 JArbSchG)
    * **Verbotene Arbeiten:** Arbeiten, welche die Leistungsfähigkeit übersteigen, z.B. Akkordarbeit, Fließbandarbeit, gefährliche Arbeiten (§§ 22 - 23 JArbSchG)
    * **Ärztliche Untersuchungen:** Ohne Erstuntersuchung vor Beschäftigunsaufnahme dürfen Jugendliche nicht beschäftigt werden. In den letzten 3 Monaten des 1. Arbeitsjahres muss eine Nachuntersuchung durchgeführt werden, wenn das 18. Lebensjahr noch nicht vollendet ist (§§ 32 - 33 JArbSchG)