# Ausbildungsvertrag

## Übersicht

![Übersicht über die Ausbildung](../../img/wi/uebersicht_ausbildungsvertrag.png)

## Mindestangaben

1. Art, sachliche und zeitliche Gliederung sowie Ziel der Berufsausbildung
2. Beginn und Dauer der Berufsausbildung - mindestens 2, in der Regel 3 Jahre
3. Ausbildungsmaßnahmen außerhalb der Ausbildungsstätte
4. Dauer der regelmäßigen täglichen Ausbildungszeit
5. Dauer der Probezeit - mindestens 1 Monat, höchstens 4 Monate
6. Zahlungstermine und Höhe der Ausbildungsvergütung
7. Dauer des Urlaubs
8. Kündigungsvoraussetzungen
9. Hinweis auf Tarifverträge, Betriebs- oder Dienstleistungsvereinbarungen

## Rechte und Pflichten

### Pflichten des Ausbildenden

* **Ausbildungspflicht** - d.h. Vermittlung von Kenntnissen, Fertigkeiten und beruflicher Handlungsfähigkeit
* kostenlose **Bereitstellung der Ausbildungsmittel**
* Freistellung zum **Berufsschulbesuch**
* Zahlung der **Ausbildungsvergütung** (2020 beträgt die **Mindestvergütung** 515€)
* **Fürsorgepflicht** - d.h. Zahlung der Beiträge zur gesetzlichen Sozialversicherung, Einhaltung des Jugendarbeitsschutgesetzes, Beachtung der Unfallschutzbestimmungen
* Ausbilder darf nur **Arbeit anordnen**, die zum Ausbildungsberuf gehören
* Pflicht zur **Zeugnisausstellung**

!!! note "Merke"

    Die Pflichten des Ausbildenden sind gleichzeitig die Rechte des Auszubildenden

!!! warning ""

    Die Nichteinhaltung kann zur außerordentlichen Kündigung (=fristlos) berechtigen und zur Schadenersatzpflicht führen.

### Pflichten des Auszubildenden

* **Lernpflicht** - d.h. der Auszubildende soll bemüht sein, sich die nötigen Kenntnisse und Fähigkeiten anzueignen
* **Sorgfaltspflicht** - d.h. ihm übertragene Arbeiten muss der Auszubildende sorgfältig ausführen
* **Gehorsamspflicht** - d.h. Weisungen des Ausbilders sind zu befolgen
* Pflicht zum **Berufsschulbesuch**
* Pflicht, **schriftlichen Ausbildungsnachweis** zu führen
* **Schweigepflicht**
* Pflicht zur Einhaltung des **Wettbewerbsverbots, d.h. dem Ausbildenden darf keine Konkurrenz gemacht werden

!!! note "Merke"

    Die Pflichten des Auszubildenden sind gleichzeitig die Rechte des Ausbildenden


!!! warning ""

    Die Nichteinhaltung kann zur außerordentlichen Kündigung (=fristlos) berechtigen und zur Schadenersatzpflicht führen.

## Kündigung

* schriflich
* 4-Wochenfrist bei **Berufsaufgabe** oder **Berufswechsel**
* fristlos **aus wichtigem Grund** (z.B. Diebstahl, Beleidung)
* frislos und ohne Angabe von Gründen **während der Probezeit** (1 bis 4 Monte)

## Vergleich Arbeitsverhältnis

| Merkmal | Ausbildungsverhältnis | Arbeitsverhältnis | 
| ------- | --------------------- | ----------------- |
| **Vertragsgegenstand** | Berufsausbildung in anerkanntem Ausbildungsberuf | Arbeitsleistung |
| **Beteiligte** | Ausbildender (Betrieb) = Praxis; Auszubildender (falls U18 mit gesetzlichem Vertreter); Berufsschule = Theorie | Arbeitgeber und Arbeitnehmer |
| **Vertragsdauer** | (je nach Ausbildungsberuf mindestens 2, höchstens 4 Jahre) | Liegt im Ermessen der Vertragspartner (Befristet / Unbefristet)
| **Entgelt** | **Ausbildungsmindestvergütung** (Steigt jedes Jahr) | Lohn und Gehalt (**Mindestlohn**) |
| **Kündigung** | Während der Probezeit jederzeit fristlos. Danach: 1. Aus einem wichtigen Grund; 2. vom Auszubildenden mit einer Frist von 4 Wochen | In Probzeit (2 Wochen Frist); fristgerecht (ordentlich) mit Kündigungsfrist oder fristlos (außerordentliche) ohne Kündigungsfrist |
| **Vertragsform** | Schriftlich | Mündlich, schriflich |
| **Probezeit Dauer** | 1 - 4 Monate | 0 - 6 Monate |
| **Dokumentation** | Pflicht Berichtsheft zu schreiben | Arbeitsprojekte dokumentieren |
| **Rechtsgrundlage** | BBiG (Berufsbildungsgesetz) | Jeweiligen Gesetze des Arbeitsrechts |