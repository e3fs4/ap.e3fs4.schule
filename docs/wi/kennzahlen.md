---
tags:
- FISI
- FIAE
---

# Kennzahlen

## Verbraucherpreisindex

Der Verbraucherpreisindex (oder auch Lebenshaltungsindex) misst die durchschnittliche Preisentwicklung aller Waren und Dienstleistungen, die private Haushalte für Konsumzwecke kaufen und ist der wichtigste Maßstab zur Beurteilung der Geldwertentwicklung in Deutschland.

Bei der Berechnung des Verbraucherpreisindexes geht man von einem Warenkorb aus, der sämtliche von privaten Haushalten gekaufte Waren und Dienstleistungen widerspiegelt.
Derzeit umfasst der Warenkorb ca. 600 Sachgüter und Dienstleistungen (z.B. Nahrungsmittel, Bekleidung, Reparaturen, Strom, Gas, Möbel, Mieten usw.).
Der Warenkorb wird laufend aktualisiert, damit immer diejenigen Güter in die Preisbeobachtung eingehen, welche von den privaten Haushalten aktuell gekauft werden.
Der Verbraucherpreisindex wird vom Statistischen Bundesamt erstellt.

Ausgangspunkt für die Ermittlung ist der Warenkorb.
Da nicht alle Güter das gleiche Gewicht besitzen, erstellt das Statistische Bundesamt ein „Wägungsschema“.
Das Wägungsschema zeigt auf, welche prozentualen Anteile bestimmte Güter und Dienstleistungen an den Gesamtausgaben der privaten Haushalte haben.
Das Wägungsschema wird alle fünf Jahre den veränderten Verbrauchsgewohnheiten der Haushalte angepasst.

![Gewichtung des Verbraucherpreisindex 2020](../img/wi/gewichtung-vpi.png)

Das Verfahren der Index-Ermittlung steht in der Kritik. Denn es kann nicht zwingend auf jeden Haushalt angewendet werden. Der Index stellt lediglich eine grobe statistische Orientierung dar. Der Grund dafür liegt darin, dass beispielsweise auch die in den letzten Jahren eher rückläufige Preisentwicklung elektronischer Geräte berücksichtigt wird. Diese mindert statistisch die im Allgemeinen eher steigenden Kosten für Lebensmittel und Ähnliches. Die eigentlich wichtigen Lebenshaltungskosten steigen demnach unter Umständen stärker als es der Lebenshaltungsindex vermuten lässt.

Vereinfachtes Beispiel:

<table>
    <thead>
        <tr>
            <th rowspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Warenkorb</th>
            <th rowspan="2" colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Währungsschema Jahr 00</th>
            <th colspan="4" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Preise</th>
        </tr>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">01</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">02</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">1. Nahrungsmittel</td>
            <td style="border:1px solid #b3adad; padding:5px;">615,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">41%</td>
            <td>615,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">615,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">2. Kleidung</td>
            <td style="border:1px solid #b3adad; padding:5px;">600,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">40%</td>
            <td style="border:1px solid #b3adad; padding:5px;">600,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">600,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">3. Wohnung</td>
            <td style="border:1px solid #b3adad; padding:5px;">150,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">10%</td>
            <td style="border:1px solid #b3adad; padding:5px;">200,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">200,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">4. Brennstoffe</td>
            <td style="border:1px solid #b3adad; padding:5px;">60,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">4%</td>
            <td style="border:1px solid #b3adad; padding:5px;">60,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">60,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">5. Dienstleistungen</td>
            <td style="border:1px solid #b3adad; padding:5px;">75,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">5%</td>
            <td style="border:1px solid #b3adad; padding:5px;">75,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
            <td style="border:1px solid #b3adad; padding:5px;">75,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;"></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Gesamtausgaben</td>
            <td style="border:1px solid #b3adad; padding:5px;">1500,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">100%</td>
            <td style="border:1px solid #b3adad; padding:5px;">1550,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">103,3%</td>
            <td style="border:1px solid #b3adad; padding:5px;">1610,00 GE</td>
            <td style="border:1px solid #b3adad; padding:5px;">107,3%</td>
        </tr>
    </tbody>
</table>

**Erläuterungen:** Berechnung des Verbraucherpreisindex im Jahr 01 und 02  
Das Basisjahr 00 wird mit 100 Punkten angesetzt.
Die angenommene Verteuerung der Wohnungsausgaben um 50,00 GE im Jahr 01 (=33,3%) bewirkt bei Konstanz aller anderen Preise eine Erhöhung der Lebenshaltungskosten um 3,3 auf 103,3 Punkte.
Steigen im Jahr z.B. die Preise für Kleidung um 10%, erhöhen sich die Lebenshaltungskosten bezogen auf das Jahr 00 um 4 auf 107,3 Punkte.

## Inflation, Deflation, Kaufkraft, Reallohn

Die Preise einzelner Güter ändern sich ständig und damit auch die Kaufkraft des Geldes.
Die Kaufkraft des Geldes ist die Gütermenge, die mit einer Geldeinheit erworben werden kann.
Zur Messung der Kaufkraft wird der Verbraucherpreisindex herangezogen, der die Preisentwicklung der typischen Güter der Lebenshaltung, d.h. des Warenkorbs, widerspiegelt.

* Sinkt der Verbraucherpreisindex, hat sich die Kaufkraft des Geldes erhöht. Für eine Geldeinheit können mehr Güter als zu einem früheren Zeitpunkt gekauft werden.
* Steigt der Verbraucherpreisindex, hat sich die Kaufkraft des Geldes verringert. Für eine Geldeinheit können weniger Güter als zu einem früheren Zeitpunkt gekauft werden.

Wenn die Kaufkraft bei gleich hohem Einkommen sinkt, wird von Inflation (Geldentwertung) gesprochen.
Inflation ist ein ständiger allgemeiner Anstieg der Preise, der zu einer Minderung der Kaufkraft des Geldes führt.
Maßstab zur Messung der Inflationsrate ist der Verbraucherpreisindex.
Ist der Wertverlust zu groß, nimmt das Vertrauen in die entsprechende Währung ab.
Die Folge ist die „Flucht“ in Sachwerte (z.B. Häuser, Wohnungen), Ersatzwährung (z.B. Gold) oder Tauschhandel.

Sinkendes Preisniveau bewirkt, dass Unternehmen und private Haushalte mit ihren Käufen eine abwartende Haltung einnehmen, weil sie weiter sinkende Preise erwarten:  
sie sparen.
Durch die schlechte Auftragslage gehen Arbeitsplätze verloren, die Einkommen sinken.
Dies führt zu einer weiteren Verminderung der Nachfrage und steigende Arbeitslosigkeit.
Ein anhaltendes Sinken des Preisniveaus, verbunden mit zunehmender Arbeitslosigkeit nennt sich Deflation.
Ursachen der Deflation sind vor allem Kürzungen der Staatsausgaben und pessimistische Zukunftserwartungen.

### Nominallohn, Reallohn und Geldillusion

Was der Arbeitnehmer am Monatsende ausbezahlt erhält (Nettolohn), nennt man **Nominallohn**.
Setzt man den Nominallohn ins Verhältnis zum Preisniveau, erhält man den **Reallohn**.
Er gibt die Gütermenge an, die mit dem betreffenden Nominallohn gekauft werden kann.
Der Reallohn berücksichtigt die Kaufkraft des Einkommens.
Im Allgemeinen neigt man dazu, den Wert des Geldes nominal zu bewerten.
Dies ist besonders verheerend in Zeiten hoher Inflation, wenn der nominale Wert des Geldes immer schneller vom realen Wert abweicht.
Die Unfähigkeit zwischen dem nominalen und dem realen Wert des Geldes zu unterscheiden wird als Geldillusion bezeichnet.

![Reallöhne, Nominallöhne, Verbraucherpreise](../img/wi/loehneVergleich.png)

## Weiterführende Links

### Videos

* [Verbraucherpreisindex und Inflation kurz erklärt | Statistisches Bundesamt](https://www.destatis.de/DE/Themen/Wirtschaft/Preise/Verbraucherpreisindex/inflation.html)
* [Inflationsrate - Grundelemente der Makroökonomie 7 | Simpleclub](https://www.youtube.com/watch?v=OeBLISolZ1E)