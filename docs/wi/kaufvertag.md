# Kaufvertrag

## Zustandekommen

Wie alle Verträge entsteht auch der Kaufvertrag durch mindestens zwei übereinstimmende Willenserklärungen, den Antrag und dessen Annahme.
Antrag und Antragsannahme können sowohl durch den Käufer als auch durch den Verkäufer erfolgen.
Am häufigsten treten zwei Möglichkeiten auf:  
Der Verkäufer macht dem Käufer ein Angebot.
Dieser bestellt rechtzeitig, ohne das Angebot abzuändern.
Dadurch ist ein Kaufvertrag entstanden.
Bestellt der Käufer ohne ein vorhergehendes Angebot, entsteht ebenfalls ein Kaufvertrag, wenn der Verkäufer die Bestellung annimmt.

## Inhalte und Pflichten

Durch den Abschluss des Kaufvertrages verpflichten sich die Vertragspartner (Käufer und Verkäufer), ihre Leistungen entsprechend den getroffenen Vereinbarungen zu erbringen (**Verpflichtungsgeschäft**).
Durch Einhaltung dieser Pflichten erfüllen die Vertragspartner den Kaufvertrag (**Erfüllungsgeschäft**).

Das Verpflichtungsgeschäft und das Erfüllungsgeschäft können zeitlich auseinander liegen.
Oft fallen sie jedoch zeitlich zusammen (z.B. Kauf an einem Kiosk oder im Einzelhandelsgeschäft).

Die Erfüllungsvereinbarungen müssen eindeutig getroffen werden.
Deshalb sollte der Kaufvertrag folgende Fragen klären:

* **Wie** ist zu erfüllen?
* **Wann** ist zu erfüllen?
* **Wo** ist zu erfüllen?

### Inhalte

Der Kaufvertrag enthält im Normalfall Vereinbarungen über:

* Art, Beschaffenheit und Güte (Qualität) der Sachgüter
* Menge der Sachgüter
* Preis und Preisnachlässe
* Kosten der Versandverpackung
* Lieferungsbedingungen
* Zahlungsbedingungen
* Erfüllungsort und Gerichtsstand
* Allgemeine Geschäftsbedingungen

Fehlen solche Vereinbarungen, so treten an ihre Stelle die jeweiligen gesetzlichen Bestimmungen des BGB bzw. des HGB.

### Pflichten

Der Verkäufer ist verpflichtet:

* dem Käufer den Kaufgegenstand rechtzeitig und mangelfrei zu übergeben,
* dem Käufer das Eigentum daran zu übertragen,
* den Kaufpreis anzunehmen.

Der Käufer ist verpflichtet:

* den vereinbarten Kaufpreis rechtzeitig (fristgemäß) zu zahlen,
* den ordnungsgemäßen Kaufgegenstand abzunehmen.

## Form

Der Kaufvertrag ist in der Regel formfrei.
Er kann also sowohl mündlich oder schriftlich als auch durch konkludentes Handeln abgeschlossen werden.
Nur bei bestimmten Kaufverträgen schreibt der Gesetzgeber eine besondere Form vor.
Notarielle Beurkundung ist beim Kauf von Immobilien (Grundstücke, Wohnungseigentum) erforderlich, beim Kauf eines GmbH-Anteils und beim Erbschaftskauf.
Große und teure Sachen werden in der Praxis jedoch fast immer mit einem schriftlichen Vertrag verkauft (z. B. Autos).

## Bindung eines Angebotes

Grundsätzlich ist jeder an sein Angebot gebunden.
Diese Bindung wird aufgehoben:

- Durch Freizeichnungsklauseln, d.h. der Absender macht seinen Antrag unverbindlich mit Formulierungen wie z.B. „Angebot freibleibend“, „solange der Vorrat reicht“, „unverbindlich“.
- Durch eine verspätete oder abgeänderte Annahme. Unter Anwesenden muss ein Antrag sofort angenommen werden, schriftliche Angebote schnellstmöglich. Unter Berücksichtigung der Beförderungsdauer muss bei einem schriftlichen Angebot ca. nach einer Woche die Annahme vorliegen. Eine verspätete oder abgeänderte Annahme verpflichtet den Antragsteller nicht mehr. Nimmt er dennoch an, entsteht ein Vertrag (eine verspätete bzw. abgeänderte Annahme ist demnach rechtlich ein neuer Antrag).

Schaufensterauslagen, Kataloge, Zeitungsanzeigen oder Postwurfsendungen sollen den Kunden nur auffordern, selbst ein Angebot abzugeben.
Ein Angebot ist immer an eine bestimmte Person gerichtet.