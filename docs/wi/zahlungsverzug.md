# Zahlungsverzug

![Zahlungsverzug Übersicht](../img/wi/zv.png)

## Folgen

![Gerichtliches Mahn- und Klageverfahren](../img/wi/folgen_zv.png)

### Ablauf Lohnabtretung

Die Lohnabtretung dient der Kreditsicherung.  
-> Kreditgeber (Bank) können sich eine Abtretung des Lohns von betreffenden Schuldner unterschreiben lassen.  
-> Durch die Unterschrift des Kreditnehmers (Schuldners) sichert dieser dem Gläubiger den pfändbaren Teil seines monatlichen Arbeitsentgeltes zu.  
-> Der Kreditnehmer zahlt seinen Kredit  nicht pünktlich zurück.  
-> Die Bank als Gläubiger wendet sich an den Arbeitgeber.  
-> Die Bank zieht den pfändungsfähigen Betrag des Lohns ein.  