# Betriebsrat

## Voraussetzungen

Fünf (drei davon wählbar) ständig wahlberechtigte Arbeitnehmer

## Wahlberechtigt sind:

alle Arbeitnehmer eines Betriebes über 16 Jahre alt

## Wählbar sind:

alle Arbeitnehmer über 18 Jahre und die mindestens 6 Monate im Betrieb sind

## Wahl

Alle 4 Jahre

## Größe

| in Unterhnehmen mit so vielen Mitarbeitern | beträgt die Zahl der Betriebsratmitglieder | 
| ------------------------------------------ | ------------------------------------------ |
| 5 - 20 | 1 |
| 21 - 50 | 3 |
| 51 - 100 | 5 | 
| 101 - 200 | 7 |
| 201 - 400 | 9 |
| 401 - 700 | 11 |
| 701 - 1000 | 13 |
| 1001 - 1500 | 15 |
| 1501 - 2000 | 17 |
| 2001 - 2500 | 19 |
| 2501 - 3000 | 21 |
| 3001 - 3500 | 23 |
| 3501 - 4000 | 25 |
| 4001 - 4500 | 27 |
| 4501 - 5000 | 29 |
| 5001 - 6000 | 31 |
| 6001 - 7000 | 33 |
| 7001 - 9000 | 35 |
| je angefangene weiter 3000 | +2 |

## Stufen der Mitbestimmung

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">sozialer Bereich</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">personeller Bereich</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">wirtschaftlicher Bereich</th>
        </tr>
        <tr>
            <th rowspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Mitbestimmungsrecht</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Mitwirkungsrecht</th>
        </tr>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Wiederspruchsrecht</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Informations- und Beratungsrecht</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">soziale Angelegenheiten (Betriebsordnung, Urlaubsplan, Beginn / Ende der Arbeitszeit, Zeit / Ort / Art der Entgeltzahlung, Entlohnungsgrundsätze, Akkord- / Prämiensätze, Überstunden, Pausen, soziale Einrichtungen, Kantine, sanitäre Anlagen, Unfallverhütung)</td>
            <td style="border:1px solid #b3adad; padding:5px;">personelle Einzelmaßnahmen (Versetzung, Eingruppierung, Umgruppierung, Kurzarbeit, Einstellung)</td>
            <td style="border:1px solid #b3adad; padding:5px;">wirtsch. Angelegenheiten (wirt. und finaz. Lage, Produktion und Absatz, Investitionen, neue Arbeits- und Rationalisierungsmethoden)</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Sozialplan bei Betriebsveränderungen</td>
            <td style="border:1px solid #b3adad; padding:5px;">betriebliche Bildungsmaßnahmen</td>
            <td style="border:1px solid #b3adad; padding:5px;">Arbeitsplatzgestaltung (Baumaßnahmen, techn. Anlagen, Arbeitsablauf)</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">betriebliche Berufsausbildung</td>
            <td rowspan="2" style="border:1px solid #b3adad; padding:5px;">Kündigung</td>
            <td style="border:1px solid #b3adad; padding:5px;">Personalplanung, Förderung, betriebliche Bildung</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Personalfragebogen, Beurteilungsgrundsätze</td>
            <td style="border:1px solid #b3adad; padding:5px;">Betriebsveränderungen, Stilllegung</td>
        </tr>
    </tbody>
</table>