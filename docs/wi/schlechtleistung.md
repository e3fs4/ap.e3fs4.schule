# Schlechtleistung

Der Käufer hat einen Anspruch darauf, dass die Ware in einwandfreiem Zustand geliefert wird.
Hat sie nicht die vertraglich **vereinbarte Beschaffenheit**, liegt ein Sachmangel vor.
Wurde nichts festgelegt, ist die gewöhnliche Verwendung maßgebend, d.h. eine Beschaffenheit, wie sie bei gleichartigen Sachen üblich ist.
Eine **mangelhafte Lieferung** kann demnach folgende Mängel aufweisen:

## Sachmängel

<table>
    <thead>
        <tr>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Beschaffenheitsmängel</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Montagemängel</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Falschlieferung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Minderlieferung</th>
        </tr>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Fehlerhafte Ware</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Ware entspricht nicht der Werbeaussage</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Montagefehler</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Mangelhafte Montageanleitung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">(Mangel in der Art)</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">(Mangel in der Menge)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">entspricht nicht der vertraglich vereinbarten Beschaffenheit</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:5px;">es fehlen der Ware Eigenschaften, die in einer Werbeaussage oder Kennzeichnung versprochen wurden</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:5px;">unsachgemäß durchgeführte Montage des Verkäufers</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:5px;">mit der Folge falscher Montage durch Käufer (sog. "IKEA-Klausel")</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:5px;">andere Sache wird geliefert</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:5px;">zu geringe Menge wird geliefert</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">eignet sich nicht für die nach dem Vertrag vorausgesetzte Verwendung</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">eignet sich nicht für eine gewöhnliche Verwendung</td>
        </tr>
    </tbody>
</table>

## Rechtsmängel

z.B.

* Verkäufer ist nicht Eigentümer
* Ware ist mit Pfandrecht belastet

## Haftung

Der Umfang der Sachmangelhaftung hängt davon ab, ob ein Privatmann oder ein Unternehmer einkauft.

| Privatmann als Käufer | Unternehmer als Käufer |
| --------------------- | ---------------------- |
| Bei neuen Produkten haftet der Verkäufer 2 Jahre. Bei gebrauchten Sachen kann die Frist auf ein Jahr herabgesetzt werden. | Durch Vertrag kann der Verkäufer seine Haftung beliebig verkürzen oder ausschließen. In Allgemeinen Geschäftsbedingungen kann die Haftung bei neuen Sachen auf ein Jahr herabgesetzt werden. |

## Rechte des Käufers

![Rechte des Käufers bei Schlechtleistung](../img/wi/rechteKaeufer_schlechtleistung.jpg)

## Pflichten des Käufers

* Prüfungspflicht
    * **bürgerlicher Kauf** und **einseitiger Handelskauf** (Verbrauchsgüterkauf; § 474 BGB): innerhalb der Gewährleistungsfrist
    * **zweiseitiger Handelskauf** und  **einseitiger Handelskauf**, wenn Unternhemer etwas von Privat kauft: unverzüglich
* Rügepflicht
    <table>
        <thead>
            <tr>
                <th style="border: none; padding:5px; text-align:center; vertical-align: middle;">Rügefristen</th>
                <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Offene Mängel</th>
                <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Versteckte Mängel</th>
                <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Arglistig verschwiegene Mängel</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border:1px solid #b3adad; padding:5px;">Zweiseitiger Handelskauf und  einseitiger Handelskauf, wenn Unternhemer etwas von Privat kauft</td>
                <td style="border:1px solid #b3adad; padding:5px;">unverzüglich</td>
                <td style="border:1px solid #b3adad; padding:5px;">unverzüglich nach Entdeckung, spätestens innerhalb der Gewährleistungsfrist</td>
                <td style="border:1px solid #b3adad; padding:5px;">unverzüglich nach Entdeckung, innerhalb von 3 Jahren</td>
            </tr>
            <tr>
                <td style="border:1px solid #b3adad; padding:5px;">Einseitiger Handelskauf (Verbrauchsgüterkauf) und bürgerlicher Kauf</td>
                <td colspan="2" style="border:1px solid #b3adad; padding:5px;">innerhalb der Gewährleistungspflicht</td>
                <td style="border:1px solid #b3adad; padding:5px;">innerhalb von 3 Jahren</td>
            </tr>
        </tbody>
    </table>
  -> Mängel müssen in der Rüge genau bezeichnet werden