# Gehalt

## Brutto Netto

Bruttolohn / -gehalt - gesetzliche Abzüge (Lohnsteuer, Kirchensteuer, Solidaritätszuschlag, Sozialversicherungsbeiträge) = Nettolohn / -gehalt

Nettolohn / -gehalt - sonstige Abzüge (z.B. vermögenswirksames Sparen, Mietzahlungen für Betriebswohnung, Lohnpfändung) = Auszahlungsbetrag

## Gehaltsabrechnung

![Beispiel Gehaltsabrechnung](../img/wi/gehaltsabrechnung.png)