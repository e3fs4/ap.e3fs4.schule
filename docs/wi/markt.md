---
tags:
- FISI
- FIAE
---

# Markt

!!! note "Definition"

    Der Markt ist der Ort, an dem Angebot und Nachfrage nach bestimmten Gütern aufeinandertreffen und derern Preis ermittelt wird.
    ![Markt Schaubild](../img/wi/marktSchaubild.png)

## Marktformen

### Nach Art der Preisbildung

#### Volkommene Märkte

* Einheitlicher Preis für ein bestimmtes Gut

#### Unvolkommene Märkte

* Unterschiedliche Preise für ein bestimmtes Gut

| | Milchkaffe (Beispiel unvollkommener Markt) | Börse (Beispiel volkommener Markt) |
| - | ---------------------------------------- | ---------------------------------- |
| **Merkmal:** | Kein gleichartiges (heterogenes) Gut | Gleichartiges Gut |
| Begründung: | viel/wenig Milch; große/kleine Tasse; unterschiedliche Kaffesorten | keine Unterschiede hinsichtlich Wert, Form, Farbe, etc. |
| **Merkmal:** | keine Marktübersicht (Markttransparenz) | Marktübersicht (Markttransparenz) |
| Begründung: | schwierige Informationsbeschaffung, da viele unterschiedliche Cafés | gesamte Marktsituation an der Börse zu erfahren |
| **Merkmal:** | sachliche Vorzüge (Präferenzen) | keine sachlichen Vorzüge (Präferenzen) |
| Begründung: | Qualität des Kaffees | Die Aktie 1242 der Firma Daimler AG ist nicht besser als die mit der Nummmer 4563 |
| **Merkmal:** | persöhnliche Vorzüge (Präferenzen) | keine persöhnlichen Vorzüge (Präferenzen) |
| Begründung | Man wählt das Café, in dem man am freundlichsten bedient wird | Es wird nicht ein bestimmter Verkäufer bevorzugt, weil dieser z.B. sympathischer ist |
| **Merkmal:** | räumliche Vorzüge (Präferenzen) | keine räumlichen Vorzüge (Präferenzen) |
| Begründung: | Man wählt das Café welches am nächsten/günstigsten ist | Punktmarkt, an dem Angebot und Nachfrage gleichzeitig aufeinander treffen | 
| **Merkmale** | zeitliche Vorzüge (Präferenzen) | keine zeitlichen Vorzüge (Präferenzen) |
| Begründung: | Man geht in das Café, das geöffnet hat. | Keine Unterscheidung der Aktien, da keine Lieferzeit |
| | **-> Preise unterscheiden sich** | **-> Preise unterscheiden sich nicht**

!!! note "Merke"

    Ist ein Merkmal des volkommenen Marktes nicht erfüllt, spricht man von einem unvolkommenen Markt.

### Nach Anzahl der Anbieter und Nachfrager

| Marktform | Polypol | Oligopol | Monopol |
| --------- | ------- | -------- | ------- |
| Marktteilnehmerzahl bzgl. Anbieter | Viele Anbieter | Wenige Anbieter | Ein Anbieter |
| Preisgestaltung | Bestmöglicher Preis aus Kundensicht | Preiskampf oder abgesprochen hoch | Bestmöglicher Preis aus Anbietersicht |
| Konkurrenten | Viele | Wenige | Keine |
| Gewinn | Kaum / Wenig | Akzeptabler Gewinn | Hoch | 
| Beispiele | Lebensmittel, Bekleidung, Arbeitsmarkt, Wohnungsmarkt, Gebrauchtwagenmarkt | Auto, Benzin, Tabak, Kino, Pay-TV, Mobilfunkanbieter, Stromanbieter | Post, Bahn, Wasserversorgung, Teile der Rüstungsindustrie, Pharmakonzern für ein geschützes Medikament |

#### Monopol 

Bei einem Monopol (griechisch "monos" = einer allein) existiert nur ein einziger Anbieter für ein Produkt in einem Markt.
Da es keinerlei Konkurrenten für diesen Anbieter gibt, kann er quasi den Preis setzen und die Menge anbieten, die für ihn und seinen Gewinn ideal sind.
Den interessierten Käufern bleibt nichts anderes übrig, als das Gut für den Preis beim Monopolisten zu kaufen.
Durch den fehlenden Wettbewerb hat der Monopolist keinen Anreiz, sein Gut zu verbessern oder bessere Wege zu finden, es herzustellen.
Allerdings muss er darauf achten, dass er die Preise nicht zu hoch ansetzt, sonst geht die Kaufbereitschaft der Nachfrager zurück.

#### Oligopol

Bei einem Oligopol (griechisch „oligoi“ = wenige) gibt es nur wenige Anbieter eines Produktes, hat jeder dieser Anbieter eine gewisse Marktmacht und kann durch seine Preisentscheidung das Marktgeschehen beeinflussen.
Senkt er den Verkaufspreis seines Produktes, läuft die Nachfrage vermehrt zu ihm.
Alle anderen Mitanbieter müssen nun irgendwie darauf reagieren.
Jede Aktion eines Anbieters führt somit zu einer Gegenreaktion der anderen Anbieter.
Daraus kann sich ein scharfer Wettbewerb und Preiskampf unter den Oligopolisten entwickeln.
Andererseits besteht die Gefahr, dass sich die wenigen Anbieter absprechen und gemeinsam einen Preis festlegen, an den sie sich alle halten und der ihnen hohe Gewinne einbringt.
Eine Gruppe von Anbietern, die solche Preisabsprachen treffen, nennt man Kartell.

#### Polypol

Bei der polypolen (griechisch „polys“ = viele) Marktform gibt es sehr viele Anbieter, die alle dasselbe bzw. ein sehr ähnliches und austauschbares Produkt verkaufen.
Anbieter, die höhere Preise verlangen als die Konkurrenz werden aufgrund des ähnlichen Produktes nicht mehr verkaufen können, da die Kunden sofort zu einem der zahlreichen Konkurrenten gehen würden, um das gleiche Produkt dort zu einem günstigeren Preis zu kaufen.
Aufgrund des geringen Marktanteils kann kein Anbieter direkt Einfluss auf den Marktpreis nehmen.
Der rege Wettbewerb bietet also den bestmöglichen Preis für den Nachfrager.
Das Polypol setzt grundsätzlich einen vollkommenen Markt voraus.

## Preisbildung

### Nachfrage
