# Allgemeine Geschäftsbedingungen

Um Verträge wie Kauf, Miet-, Leasing- oder Kreditverträge möglichst effektiv abwickeln zu können, formulieren viele Unternehmen allgemeine Geschäftsbedingungen (AGB), der Bestandteil ihrer Verträge werden sollen.
Die AGB können Regelungen enthalten, die den Kunden benachteiligen.
Um den Verbraucher vor den „Kleingedruckten“ zu schützen, ist im BGB in den §§ 305-310 ein rechtlicher Rahmen für die AGB vorgegeben.

![Übersicht AGB's](../img/wi/agb_uebersicht-0.jpg)