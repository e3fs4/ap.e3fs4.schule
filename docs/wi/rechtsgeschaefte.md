# Rechtsgeschäfte

![Übersicht Arten von Rechtsgeschäften](../img/wi/rechtsgeschaefte_uebersicht.png)

## Willenserklärung

!!! note

    Wenn Rechtsubjekte **Willenserklärungen** abgeben, die sich auf bestimmte Rechtsobjekte beziehen, kommt ein **Rechtsgeschäft** zustande.

Durch Willenserklärungen werden Rechte

* begründet (z.B. Eigentumserwerb)
* verändert (z.B. Testament)
* aufgehoben (z.B. Kündigung)

Eine wirksame Willenserklärung liegt vor, wenn folgende Voraussetzungen gegeben sind:

* Handlungsbewusstsein (= bewusstes Tätig werden)
* Rechtsbindungswille bzw. Erklärungsbewusstsein (=rechtliche Bindung ist gewollt)
* bestimmter Geschäftswille (= Inhalt des Rechtsgeschäftes ist bestimmt)

Eine Willenserklärung kann auf verschiedene Arten abgegben werden:

* Ausdrückliche Erklärung
    * mündlich
    * schriftlich z.B. durch Brief, Telefax, E-Mail
* Schlüssiges, konkludentes Handeln z.B.
    * durch Liefern der bestellten Ware
    * Geld in Automaten stecken
    * Heranwinken eines Taxis
    * Kopfnicken, Handzeichen
* Schweigen - Keine Reaktion auf vorhergehende Willenserklärung
    * (Selten! Nur unter Kaufleuten §362 HGB)

Bindung an eine Willenserklärung §147 BGB:

* §147 (1) BGB: Unter Anwesenden kann eine Willenserklärung nur sofort, d.h. solange die Unterredung dauert, angenommen werden.
* §147 (2) BGB: Unter Anwesenden kann eine Willenserklärung nur bis zu dem Zeitpunkt angenommen werden, zu dem unter regelmäßigen Umständen eine Antwort erwartet werden kann. Bei einer Bestellung per Brief geht man i.d.R. davon aus, dass man innerhalb von 5 bis 6 Tagen eine Antwort erhält. (2 Tage Hinweg, 2 Tage Überlegungszeit, 2 Tage Rückweg)

Keine Willenserklärungen sind:

* Realakte (= reine Tathandlung, bei denen es auf den Willen des Handelnden nicht ankommt, z.B. Verarbeitung von Rohstoffen im Produktionsprozess mit Folgen für das Eigentum an den Stoffen)
* Geschäftliche Handlungen (= die Rechtsfolge tritt kraft rechtlicher Anordnung ein, z.B. Mahnungen beim Schuldnerverzug)
* Erklärungen im gesellschaftlichen Bereich (= Rechtsbindungswillen fehlt)
* Aufforderung / Einladung ein Angebot zu machen (= Rechtsbindungswillen gegenüber einer bestimmten Person fehlt)

## Form der Rechstgeschäfte

Generell gilt der **Grundsatz der Formfreiheit**, d.h. Rechtsgeschäfte können in jeder möglichen Form abgeschlossen werden.

Aber: Abweichend vom Grundsatz der Formfreiheit gibt es Rechtsgeschäfte, für die das Gesetz bestimmte Formen voschreibt.

Gründe: Die Erklärenden auf mögliche negative Folgen ihrer Erklärung hinzuweisen (**Warnfunktion**) oder die Beratung der Erklärung durch Rechtskundige sicherzustellen (**Beratungsfunktion**), beispielsweise durch einen Notar.

<table>
    <thead>
        <tr>
            <th></th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Schriftform</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">§§</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">elektronische Form</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">§§</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Textform</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">§§</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">öffentliche Beglaubigung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">§§</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">notarielle Beurkundung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">§§</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Erläuterung</td>
            <td style="border:1px solid #b3adad; padding:5px;">Schriftlich abgefasste Urkunde mit <b>eigenhändiger Unterschrift</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">126 (1) BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">elektronisches Dokument mit Namen und Unterschrift des Ausstellers <b>(= elektronische Signatur)</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">126a BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Urkunde oder andere dauerhafte Wiedergabe; Nennung der Person und <b>Nachbildung der Unterschrift</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">126b BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Notar beglaubigt Unterschrift der/des Erklärenden</td>
            <td style="border:1px solid #b3adad; padding:5px;">129 (1) BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Notar protokolliert Verhandlung. Unterzeichnung von allen Beteiligten. Notar bestätigt Inhalt und Echtheit der Unterschriften.</td>
            <td style="border:1px solid #b3adad; padding:5px;">128 BGB</td>
        </tr>
        <tr>
            <td rowspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Anwendungsbeispiele</td>
            <td style="border:1px solid #b3adad; padding:5px;">Kündigung</td>
            <td style="border:1px solid #b3adad; padding:5px;">623 BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Ersatz für Schriftform, soweit Gesetz nicht anders bestimmt</td>
            <td style="border:1px solid #b3adad; padding:5px;">126 (3)</td>
            <td style="border:1px solid #b3adad; padding:5px;">Wiederrufsrecht</td>
            <td style="border:1px solid #b3adad; padding:5px;">355 (1) BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Antrag zur Eintragung ins Handelsregister</td>
            <td style="border:1px solid #b3adad; padding:5px;">12 HGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Grundstückskaufvertrag</td>
            <td style="border:1px solid #b3adad; padding:5px;">311b (1) BGB</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Ausbildungsvertrag</td>
            <td style="border:1px solid #b3adad; padding:5px;">11 BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Neuer Personalausweis (verschiedene Funktionen, Postident Verfahren)</td>
            <td style="border:1px solid #b3adad; padding:5px;">126 (3)</td>
            <td style="border:1px solid #b3adad; padding:5px;">Rückgaberecht</td>
            <td style="border:1px solid #b3adad; padding:5px;">356 (1) BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Anmeldung zum Vereinsregister</td>
            <td style="border:1px solid #b3adad; padding:5px;">77 BGB</td>
            <td style="border:1px solid #b3adad; padding:5px;">Schenkungsversprechen</td>
            <td style="border:1px solid #b3adad; padding:5px;">518 (1) BGB</td>
        </tr>
    </tbody>
</table>