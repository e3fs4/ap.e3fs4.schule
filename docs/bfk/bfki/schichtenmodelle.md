# Schichtenmodelle

## OSI


<table>
    <thead>
        <tr>
            <th colspan="2" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">OSI-Schicht</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">TCP / IP-Referenzmodell</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Protokollbeispiele</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Einheiten</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Kopplungselemente</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#anwendungsschicht">7</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#anwendungsschicht">Anwendung (Application)</a></td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Anwendung</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">DHCP<br>DNS<br>FTP<br>HTTPS<br>LDAP<br>RTP<br>SMTP<br>XMPP</td>
            <td rowspan="3" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Daten</td>
            <td rowspan="4" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Gateway<br>Content-Switch<br>Proxy<br>Layer-4-7-Switch</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#darstellungsschicht">6</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#darstellungsschicht">Darstellung (Presentation)</a></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#sitzungsschicht">5</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#sitzungsschicht">Sitzung (Session)</a></td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#transportschicht">4</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#transportschicht">Transport (Transport)</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Transport</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">TCP<br>UDP</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">TCP = Segmente<br>UDP = Datagramme</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#vermittlungsschicht">3</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#vermittlungsschicht">Vermittlung- / Paket (Network)</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Internet</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">IP<br>IPsec</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Pakete</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Router<br>Layer-3-Switch</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#sicherungsschicht">2</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#sicherungsschicht">Sicherung (Data Link)</a></td>
            <td rowspan="2" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Netzzugriff</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">MAC<br>IEEE 802.3 Ethernet<br>IEEE 802.11 WLAN<br>Token Ring</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Rahmen (Frames)</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Bridge<br>Layer-2-Switch<br>Wireless Access Point</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#bitubertragungsschicht">1</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"><a href="#bitubertragungsschicht">Bitübertragung (Phyical)</a></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Token Ring</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Bits, Symbole</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Netzwerkkabel<br>Repeater<br>Hub</td>
        </tr>
    </tbody>
</table>

### Bitübertragungsschicht

Die Bit-Übertragungsschicht oder auch physikalische Schicht beschreibt nur, wie die reine Übertragung der Daten elektrisch beziehungsweise allgemein physikalisch erfolgt.
OSI-basierte Netzwerkstandards beschreiben in dieser untersten Schicht die Struktur der Signale.
Dazu gehören unter anderem die folgenden Aspekte:

* zulässiger Amplitudenbereich
* Versand- und Empfangsmethoden für Bit-Folgen
* Operationen zur Umwandlung dieser Bit-Folgen in Daten für die nächsthöhere Schicht (und umgekehrt)
* Verarbeitungsgeschwindigkeit der Bit-Folgen
* Start- und Stoppsignale
* Erkennung beziehungsweise Unterscheidung der Signale bei gemeinsam genutzten Medien
* Übertragungseigenschaften der Medien (Kabel, Lichtwellenleiter, Funk oder Ähnliches)

Die Medien selbst sowie Netzwerkkarten oder Onboard-Netzwerkchips sind kein Bestandteil der Definitionen auf der ersten Schicht.
Die Hersteller müssen selbst dafür Sorge tragen, dass ihre Produkte den Spezifikationen genügen.

### Sicherungsschicht

Die Sicherungsschicht beschreibt alle Vorkehrungen, die dafür sorgen, dass aus den einzelnen zu übertragenden Bits, also dem reinen physikalischen Stromfluss, ein verlässlicher Datenfluss wird.
Dazu gehören die beiden Teilaufgaben Media Access Control (MAC) – die Regelung des Datenverkehrs, wenn mehrere Geräte den gleichen Kanal verwenden – sowie Logical Link Control (LLC), wobei es um die Herstellung und Aufrechterhaltung von Verbindungen zwischen den Geräten geht.
Viele Protokolle dieser Schicht implementieren eine Fehlerkontrolle, bei Ethernet wird zum Beispiel CRC verwendet.
In manchen Fällen wird auch Quality of Service (QoS), eine Art Prioritätsinformation, genutzt.
In der Sicherungsschicht werden die Bit-Folgen in Einheiten einer bestimmten Größe unterteilt und mit einem Header aus Metainformationen versehen.
Je nach Standard werden auf dieser Ebene unterschiedliche Namen für diese Datenpakete verwendet.
Bei Ethernet und Token Ring ist beispielsweise von Frames die Rede, bei ATM von Zellen.
Der Payload (Nutzdateninhalt) eines Frames beziehungsweise einer Zelle beginnt in aller Regel mit dem Header eines hineinverschachtelten Pakets der nächsthöheren Schicht.
Es kann aber auch vorkommen, dass Pakete verschiedener Protokolle der zweiten Schicht ineinander verschachtelt werden.
Dies ist zum Beispiel bei PPP over Ethernet, PPP over ATM oder ATM over SDH der Fall.

### Vermittlungsschicht

Die Netzwerkschicht oder Vermittlungsschicht definiert diejenigen Komponenten und Protokolle des Netzwerks, die an der indirekten Verbindung von Computern beteiligt sind.
Hier ist sogenanntes Routing erforderlich, das Weiterleiten von Daten in andere logische oder auch physikalisch inkompatible Netzwerke.
So gehören zum Beispiel alle diejenigen Protokolle zur Netzwerkschicht, die die logischen Computeradressen der höheren Schichten in die physikalischen Adressen umsetzen, bei Ethernet zum Beispiel ARP.
Auch auf der Netzwerkschicht werden die Daten in Pakete unterteilt, deren Namen sich je nach konkretem Protokoll unterscheiden.
Das mit Abstand verbreitetste Protokoll dieser Ebene, das im weiteren Verlauf des Kapitels noch ausführlich vorgestellte IP-Protokoll, bezeichnet sie als IP-Pakete.

### Transportschicht

Die Protokolle der Transportschicht lassen sich in verbindungsorientierte Protokolle wie TCP und verbindungslose Protokolle wie etwa UDP unterteilen.
Auf dieser Schicht werden vielfältige Aufgaben erledigt.
Ein wichtiger Aspekt sind Multiplexmechanismen, die die Anbindung der Datenpakete an konkrete Prozesse auf den kommunizierenden Rechnern ermöglichen, bei TCP und UDP beispielsweise über Portnummern, bei SPX über Connection-IDs.
Verbindungsorientierte Transportprotokolle wie TCP oder SPX sind zudem meist mit einer Fluss- und Fehlerkontrolle ausgestattet, um zu gewährleisten, dass Pakete vollständig am Ziel ankommen und dort in der richtigen Reihenfolge verarbeitet werden.
Auch auf der vierten Schicht verwenden verschiedene Protokolle jeweils eigene Bezeichnungen für die Datenpakete; so ist etwa von UDP-Datagrammen, TCP-Sequenzen und SPX-Paketen die Rede.

### Sitzungsschicht

Die Kommunikationssteuerungsschicht oder Sitzungsschicht stellt die Kommunikation zwischen kooperierenden Anwendungen oder Prozessen auf verschiedenen Rechnern sicher.

### Darstellungsschicht

Die Darstellungs- oder Präsentationsschicht dient der Konvertierung und Übertragung von Datenformaten, Zeichensätzen, grafischen Anweisungen und Dateidiensten.

### Anwendungsschicht

Die Anwendungsschicht schließlich definiert die unmittelbare Kommunikation zwischen den Benutzeroberflächen der Anwendungsprogramme, kümmert sich also um die Verwendung derjenigen Dienste über das Netzwerk, die Benutzer unmittelbar zu Gesicht bekommen.

### Horizontale und Vertikale Verbindung

Das OSI-Modell wird auf der Senderseite von oben nach unten abgearbeitet und auf der Empfängerseite von unten nach oben.
Das heißt die jeweilige Schicht gibt ihre Daten samt ihrem Header an die nächste Schicht weiter.
Diese packt ihren Header mit ihren jeweils relevanten Daten ebenfalls dazu und gibt diese dann wieder an die nächste Schicht weiter.
Man spricht von einer **vertikalen Verbindung**.

Die jeweils über oder unterliegenden Schichten können aber nichts mit den Daten der jeweiligen anderen Schicht anfangen.
Im Gegensatz dazu können die selben Schichten der Sender und Empfänger serwohl die Daten der jeweiligen Schicht verarbeiten.
Hierbei spricht man von der **horizontalen Verbindung**

![Kommunikation im OSI-Modell](../../img/bfki/osi_horizontal.svg)

### Datenkapselung

![Datenkapselung im OSI-Modell](../../img/bfki/encapsulation.png)

## TCP / IP-Refernzmodell

| TCP / IP-Schicht | Beispiel |
| ---------------- | -------- |
| Anwendungen | HTTP, FTP, DHCP |
| Transport | TCP, UDP |
| Internet | IP |
| Netzzugang | Ethernet |