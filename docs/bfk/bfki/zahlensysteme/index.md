---
tags:
- FISI
- FIAE
---

# Zahlensysteme

Zahlensysteme sind elementare Grundlage für Computer.
Computer werden nicht grundlos Rechenmaschinen genannt.
Im folgenden werden die wichtigsten Zahlensysteme für Computer behandelt.