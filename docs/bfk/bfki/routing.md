---
tags:
- FISI
- FIAE
---

# Routing

Geräte können nur in ihrem eigenen Netzt kommunizieren. 
Damit sie auch mit Geräten außerhalb ihres Netztes kommunizieren können benötigen sie ein Gerät, welches Routing beherrscht (normalerweise ein Router).
Routing ist also die Wegfindung für ein Datenpaket über Netzwerkgrenzen hinweg.

## Router

Ein Router ist letztendlich ein Gerät mit mindestens zwei Netzwerkkarten (Interfaces).
Der Router ist dann ein Vermittler zwischen diesen beiden Interfaces.
Kommt ein Paket auf einem Interface von einem Router an, welches nicht für den Router selbst bestimmt ist, dann leitet er das Datenpaket anhand von Routingtabbellen weiter.



## Statisches Routing



### Routingtabellen


## Dynamisches Routing

In großen Netzverbünden kann es sehr viel Arbeit werden die Routen einer Routingtabelle von Hand einzutragen.
Daher wurden dynamische Routingprotokolle entwickelt.
Die Idee dabei ist simple, die Router teilen sich untereinander ihre angeschlossenen Netze mit.
Folgende Ziele sollen mit dynamischen Routing gelöst werden:

* Entfernte Netzwerke finden
* Routingingormationen aktuell halten
* Den besten Pfad finden
* Routen bewerten

Die Route wird anhand der Metrik bewertet.
Die Metrik ist letzendlich nur eine Zahl in der Routingtabelle. 
Je niedriger die Metrik ist, desto besser ist die Route.
Welche Werte in die Metrik einfließen hängt vom Routingprotokoll ab.

* **Hop-Count:** Anzahl der Router auf dem Weg zum Ziel
* **Bandbreite:** Übertragungsgeschwindigkeiten auf den Routen
* **Kosten:** Wert zur Priorisierung von Routen
* **Auslastung**
* **Delay**
* **Zuverlässigkeit**

### Distance-Vektor Protokolle

Distance-Vektor Protokolle bestimmen die Routen anhand von zwei Werten:  
Zum einen an der **Distance**, also der Entfernung zum Ziel. Diese wird in der Metrik festgelegt. 
Und zum anderen an der Richtung (**Vektor**) der Route.
Das heißt der Router speichert den Next-Hop oder zumindest das Interface an dem er ein Paket weiterleiten muss (Exit-Interface)..

#### RIP

RIP steht für **Routing Information Protocol** und arbeitet nach dem Distance-Vektor Verfahren.
Als Entfernung zum Ziel (Distance) wird bei RIP der Hop-Count genutzt.
Ab 16 Hops gilt ein Netz als unerreichbar.
Um Änderungen im Netz zu erkennen werden in regelmäßigen Intervallen (beispielsweise alle 30 Sekunden) die kompletten Routingtabellen zwischen den Routern ausgetauscht.
Diese Updates werden bei RIP v1 per [Broadcast]() und bei RIP v2 per [Multicast]() verschickt.
Die Informationen werden also immer nur an den nächsten Router weitergegeben.
Sobald die Routingtabellenupdates gleich bleiben, also keine Änderungen vorgenommen werden müssen, spricht man von einem **konvergenten** Netz.
Bleiben die Updates von einem Router allerdings mehrfach aus, müssen die entsprechenden Einträge aus der Routingtabelle entfernt werden.

##### Nachteile von RIP

* Topologie des Netztes ist komplett unbekannt
    * Kann zu Count to infinity führen -> Lösung: Split Horizon
* Updates sind zeitgesteuert. Bis die Updates im gesamten Netzt bekannt sind kann es dauern.

### Link-State-Routing Protokolle

Link-State-Routing Protokolle arbeiten nach dem Shortest-Path-First Algorithmus von Dijkstra.
Routing-Updates werden nur am Anfang und bei Bedarf, also bei Änderungen ausgetauscht.
Der "Link-State", als der Verbindungszustand" wird mittels periodischen Hello-Paketen ermittelt.
Der Link-State umfasst das IP-Netzt und die IP-Adresse eines Interfaces, die Kosten für den Link und gibt es weitere Router (Nachbarn), die an diesem Netz angeschlossen sind.
Diese Link-State-Pakete werden anfangs ausgetauscht, sodass alle Router über alle Link-States bescheid wissen.

#### OSPF


## Weiterführende Links

* [Wikipedia](https://de.wikipedia.org/wiki/Routing)
* [Routingtabelle - Wikipedia](https://de.wikipedia.org/wiki/Routingtabelle)
* [RIP - Wikipedia](https://de.wikipedia.org/wiki/Routing_Information_Protocol)
* [OSPF](https://de.wikipedia.org/wiki/Open_Shortest_Path_First)

### Videos

* [Statisches Routing Reihe - Sebastian Philippi](https://www.youtube.com/playlist?list=PLCb8EhYsrW_tsRzos77LjP2DCNcNr00r2)
* [Dynamisches Routing Reihe - Sebastian Philippi](https://www.youtube.com/playlist?list=PLCb8EhYsrW_tjvQxo-Yn1l0L_-ETXPnqQ)