# Klassifizierung

## Reichweite des Netzes

### Local Area Netzwerk (LAN)

= lokales Netzwerk

beschreibt ein Netzwerk, das an ein einzelnes zusammenhängendes Areal gebunden ist, also etwa einen Raum, ein Gebäude oder maximal ein zusammenhängendes (Firmen-)Gelände.
LANs sind heutzutage in Wirtschaftsunternehmen, Schulen und Universitäten sowie anderen Organisationen und Instituten weit verbreitet.

### Metropolitan Area Network (MAN)

= Stadtgebietsnetzwerk

bezeichnet ein Netz, das eine Stadt, eine Gemeinde oder auch eine Region umfasst.
Ein Beispiel wären die verschiedenen eigenen Netze von NetCologne in Köln.
Die Ausdehnung für ein MAN liegt bei 100 km und mehr.

### Wide Area Network (WAN)

= Fernnetzwerk

ist ein Netz, das mehrere Städte, eine ganze Region oder sogar ein ganzes Land umfasst.
In Deutschland gibt es beispielsweise das Deutsche Forschungsnetz (DFN).

### Global Area Network (GAN)

= weltweites Netzwerk

ist über mehrere Länder, einen ganzen Kontinent oder sogar die ganze Welt verbreitet.
Das bei Weitem größte GAN ist heutzutage natürlich das Internet – im engeren Sinne ist ein GAN allerdings ein homogenes Netzwerk, während das Internet aus zahllosen Einzelnetzen mit unterschiedlichen Architekturen zusammengesetzt ist.

MAN, WAN und GAN werden meist unter dem Sammelnamen WAN zusammengefasst.

## Netzwerktopologie

Die Topologie eines Netzwerks beschreibt, in welcher physikalischen Grundform die einzelnen Geräte organisiert sind.
Manche Arten von Netzwerkhardware setzen eine bestimmte Topologie voraus, andere überlassen dem Einrichtenden die Entscheidung zwischen mehreren Möglichkeiten.
Topologie ist normalerweise eine Eigenschaft lokaler Netzwerke oder gar einzelner Netzsegmente.
Die meisten Fernnetze verbinden ohnehin nicht einzelne Rechner, sondern ganze Netzwerke an unterschiedlichen Orten miteinander.

### Bustopologie

![Bustopologie](../../../img/bfki/topologie_bus.png)

Die Bustopologie beschreibt ein Netzwerk, bei dem die einzelnen Knoten (Anschlüsse) hintereinander an einem einzelnen Kabelstrang angeschlossen sind, dessen Enden nicht miteinander verbunden werden dürfen (sonst würde es sich um eine Ringtopologie handeln!).
Häufig werden die beiden Enden des Kabelstrangs durch Abschlusswiderstände (Terminatoren) abgeschlossen.
Ein Beispiel für echte busförmige Netzwerke ist Ethernet über Koaxialkabel.

### Sterntopologie

![Sterntopologie](../../../img/bfki/topologie_stern.png)

Die Sterntopologie ist die Form eines Netzes, bei dem alle Knoten mit jeweils eigenem Kabel an einem zentralen Gerät miteinander verbunden werden.
Dieses zentrale Bindeglied heißt, je nach seiner genauen Funktionsweise, Hub oder Switch.
Die Sterntopologie wird zum Beispiel von Ethernet über Twisted-Pair-Kabel verwendet.

### Ringtopologie

![Ringtopologie](../../../img/bfki/topologie_ring.png)

Die Ringtopologie ähnelt der Bustopologie insofern, als auch hier alle Knoten an einem zentralen Strang aufgereiht sind.
Dieser zentrale Kabelstrang bildet jedoch einen geschlossenen Ring.
Daraus ergibt sich automatisch eine Datenstromrichtung, in die die Datenpakete grundsätzlich weitergereicht werden.
Bekanntestes Beispiel der ringförmigen Vernetzung ist Token Ring.

### Baumtopologie

![Baumtopologie](../../../img/bfki/topologie_baum.png)

Die Baumtopologie schließlich ist eher ein Standard für den Zusammenschluss verschiedener Netzsegmente.
Von einem zentralen Kabelstrang, gewissermaßen dem »Stamm« des Baums, gehen in beliebige Richtungen einzelne Verästelungen ab, an denen entweder eine einzelne Station oder ein ganzes Netz hängt.

### Maschentopologie

![Maschentopologie](../../../img/bfki/topologie_mesh.png)

Bei der Maschen-Topologie bzw. vermaschten Topologie handelt es sich um ein dezentrales Netzwerk, das keinen verbindlichen Strukturen unterliegen muss und in dem alle Netzwerkknoten irgendwie miteinander verbunden sind.
Durch ein Mesh-Netzwerk erhöht sich die Reichweite des Netzwerks insbesondere für die am Rand liegender Knoten.
Beim Ausfall einer Verbindung existiert im Regelfall immer eine alternative Strecke, um den Datenverkehr unterbrechungsfrei fortzuführen.
Dazu müssen aktive Netzwerk-Komponenten die Datenpakete innerhalb des Netzwerks vermitteln. Zum Beispiel durch Routing.

## Zentralisierungsgrad

### Client-Server-Netzwerk

Das Client-Server-Netzwerk unterscheidet generell zwei Arten von beteiligten Rechnern:
Der Server (Dienstleister) ist ein Computer, der den Arbeitsstationen der einzelnen Anwender an zentraler Stelle Ressourcen und Funktionen zur Verfügung stellt; der Client (Kunde) nimmt diese Dienstleistungen in Anspruch.
Die Dienste, die von Servern angeboten werden, sind sehr vielfältig:
Sie reichen vom einfachen Dateiserver, der Dateien im Netzwerk verteilt oder Festplattenplatz für andere freigibt, über Druckserver, Mail- und andere Kommunikationsserver bis hin zu ganz speziellen Diensten wie Datenbank- oder Anwendungsservern.

### Peer-to-Peer-Netzwerk

Das Peer-to-Peer-Netzwerk besteht aus prinzipiell gleichberechtigten Arbeitsplatzrechnern (peer bedeutet etwa »Kollege«).
Jeder Anwender ist in der Lage, Ressourcen seines eigenen Rechners an andere im Netzwerk freizugeben.
Das heißt, dass alle Rechner im Netz bis zu einem gewissen Grad Serverdienste wahrnehmen.

In der Praxis sind allerdings Mischformen häufiger anzutreffen als reine Client-Server- oder absolute Peer-to-Peer-Netze.