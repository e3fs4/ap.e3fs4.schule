# Kabel

## Kupferkabel

![Kabel](../../img/bfki/kabel.png)

**XX steht für die Gesamtschirmung:**

* U = unshielded, ohne Schirm (ungeschirmt)
* F = foiled, Folienschirm (beschichtete Kunststofffolie)
* S = shielded, Geflechtschirm (Drahtgeflecht)
* SF = shielded-foiled, Geflecht- und Folienschirm

**Y steht für die Aderpaarschirmung:**

* U = unscreened, ohne Schirm (ungeschirmt)
* F = foiled, Folienschirm (beschichtete Kunststofffolie)
* S = screened, Geflechtschirm (Drahtgeflecht)

**ZZ steht für die Verseilungsart:**

* TP = Twisted Pair (in der Regel)

![Kabelarten](../../img/bfki/kabel_arten.jpg)

### Dämpfung

Dämpfung beschreibt die Verringerung der Stärke eines Signals.
Sie ist die natürliche Folge der Signalübertragung über große Entfernungen.
Das Ausmaß der Dämpfung wird normalerweise in der Einheit Dezibel (dB) ausgedrückt.

Pein: die Signal-Leistung auf der Senderseite (Quelle)
Paus: die Signal-Leistung auf der Empfängerseite (Ziel)

![Dämpfung berechnen](../../img/bfki/daempfung.png)

#### Beispiel

Bestimmen Sie die Dämpfung ap für einen Dämpfungsfaktor Dp = 100.000:

```
  ap = 10 * log(100.000)
  ap = 10 * 5
  ap = 50dB
```

Bestimmen Sie den Dämpfungsfaktor Dp bei einer Dämpfung ap = 35dB:

```
  35dB = 10 * log(Dp)      | :10
  3,5dB = log(Dp)
  Dp = 10^3,5
```

| EIA / TIA 568 | ISO IEC 11801 | EN 50173 | Bandbreite | Anwendung |
| ------------- | ------------- | -------- | ---------- | --------- |
| Cat. 1 | - | - | 0,4 kHz | Telefon- und Modemleitungen |
| - | - | Class A | 100 kHz | Telefon- und Modemleitungen |
| Cat. 2 | - | Class B | 4 MHz | Terminal-Systeme, ISDN |
| Cat. 3 | - | Class C | 12,5 - 16 MHz | 10Base-T, 100Base-T4, ISDN, analoges Telefon |
| Cat. 4 | - | - | 20 MHz | Token Ring (16 Mbt)
| Cat. 5 | Cat. 5 | Class D | 100 MHz | 100Base-TX, SONET, sOH |
| Cat. 5e | Cat. 5e | Class D | 100 MHz | 1000Base-T |
| Cat. 6 | Cat. 6 | Class E | 250 MHz | 1000Base-T, 155-Mbit-ATM, 622-MBit-ATM |
| Cat. 6A | Cat. 6A | Class EA | 500 MHz | 10GBase-T (bis 55 Meter) |
| - | Cat. 7 | Class F | 600 MHz | 10GBase-T (bis 100 Meter) |
| - | Cat. 7A | Class FA | 1.000 MHz | 10Gbase-T |
| - | Cat. 8 | Class G | 1.600 - 2.000 MHz | 40GBase-T und 100 GBase-T |

## Lichtwellenleiter

Lichtwellenleiter (LWL), sind dünne Kunststofffasern, die optische Signale in Form von Licht bzw. Lichtsignalen über weite Strecken übertragen können. 
Elektrische Signale in Kupferleitungen nutzen Elektronen um von einem zum anderen Ende zuwandern, bei LWLs übernehmen Photonen (Lichtteilchen) diese Aufgabe.

| Vorteile | Nachteile |
| -------- | --------- |
| Überbrückung von Langstrecken möglich | Teuer in der Anschaffung |
| Extrem hohe Bandbreite | Aufwendige Produktion |
| Verlegung mit anderen Kabeln ohne Störung | Signale müssen immer umgewandelt werden |

![LWL Aufbau](../../img/bfki/lwl_aufbau.jpg)

### Multimodefaser mit Stufenindex

![Multimodefaser mit Stufenindex](../../img/bfki/lwl_mmf_stufenindex.jpg)

Multimodefasern mit Stufenprofil haben einen Gesamtdurchmesser von 200 bis 500 µm.
Durch sie werden mehrere Lichtwellen gleichzeitig geschickt.
An den Wänden der Faser wird das Signal hart reflektiert.
Die Brechzahl fällt zwischen Kern und Mantel scharf ab.
Das Ausgangssignal wird dadurch schlechter.


### Multimodefaser mit Gradientenindexprofil

![Multimodefaser mit Gradientenindexprofil](../../img/bfki/lwl_mmf_gradient.jpg)

Multimodefasern mit Gradientenprofil haben einen Gesamtdurchmesser von 125 µm.
Durch sie werden mehrere Lichtwellen gleichzeitig geschickt.
An den Wänden der Faser wird das Signal weich reflektiert.
Die Brechzahl des Kerns nimmt meist parabelförmig zum Mantel ab.
Das Ausgangssignal ist noch sehr gut.

### Single-Mode-Faser

![Single-Mode-Faser](../../img/bfki/lwl_smf.jpg)

## Strukturierte Verkabelung

![Strukturierte Verkabelung](../../img/bfki/sv.png)

### Bereiche

#### Primärbereich

Die Primäre Verkabelung ist die Verkabelung vom Standortverteiler zu den Gebäuden und zwischen den Gebäuden.
Da große Kabelstrecken/Kabelmengen gebraucht werden empfiehlt sich hier eine Glasfaserleitung wegen der geringen Dämpfung und der geringen Störanfälligkeit.

Maximallänge: 1500m – 2000m

Kabel Art: Glasfaserkabel (Für kleinere Strecken auch mal Kupfer (100m))

#### Sekundärbereich

Die Sekundäre Verkabelung ist die Verkabelung im Haus und den Stockwerken.
Es Verkabelt die Stockwerke untereinander innerhalb des Gebäudes.

Maximallänge: 500m

Kamelart: Vorzugsweise Glasfaser, Twisted-Pair (bis 100m)

#### Tertiärbereich

Dies ist die horizontale Stockwerkverkabelung also die Verkabelung auf dem jeweiligen Stockwerk. 
Diese Verkabelung kann man auch Etagenverkabelung nennen.
Die Kabel laufen vom Stockwerkverteiler zur Anschlussdose.

Maximale Länge: 100m

Kabel Art: Twisted-Pair (Kupfer)

![Bereiche Strukturierte Verkabelung](../../img/bfki/sv2.gif)