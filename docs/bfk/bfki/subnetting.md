---
tags:
- FISI
- FIAE
---

# Subnetting

## Subnetzrechner

```
<div id="tableone" style="padding-top: 40px;">
    <table class="inputTable" >
    <tr><th>Enter IP address:</th>
        <td></td>
    </tr>
    <tr>
        <td>IP Address :</td>
        <td><input id="IP" onchange="uploadvalues();"></td>
    </tr>
    <tr>
        <td>CIDR Notaion (eg: /24) :</td>
        <td><input id="CIDR" onchange="uploadvalues();"></td>
    </tr>
    <tr>
        <td></td>
        <td style="padding-top: 5px;"><button onclick="calculate();">Calculate</button></td>
    </tr>
    </table>
</div>

<div >
    <table class="results">
    <!-- ************** OUTPUT OF THE CALCULATION ********* -->
    <tr>
        <td>Subnet Adress </td>
        <td class="output" id="subnet"></td>
    </tr>
    <tr>
        <td>Network Address </td>
        <td class="output" id="networkaddress"></td>
    </tr>
    <tr>
        <td>Broadcast Address </td>
        <td class="output" id="broadcast_address"></td>
    </tr>
    <tr>
        <td>Usable Host Range </td>
        <td class="output" id="host_range"></td>
    </tr>
    <tr>
        <td>Total Hosts </td>
        <td class="output" id="total_hosts"></td>
    </tr>
    <tr>
        <td>Total Usable Hosts </td>
        <td class="output" id="usable_hosts"></td>
    </tr>
    </table>
</div>

<script src="../subnetcalculator.js"></script>
```