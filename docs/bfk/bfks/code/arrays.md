---
tags:
- FISI
- FIAE
---

# Arrays

Arrays, die manchmal auch als Datenfelder bezeichnet werden, ermöglichen es, eine nahezu beliebig große Anzahl von Variablen gleichen Namens und gleichen Datentyps zu definieren.
Durch einen Index wird ein wahlfreier Zugriff auf die einzelnen Daten (Array – Elemente) möglich. 
Arrays kommen insbesondere dann zum Einsatz, wenn in Programmschleifen dieselben Operationen auf alle oder einen Teil der Elemente ausgeführt werden sollen.

## Deklaration und Initialisierung

```csharp
int[] myArray;
```

Mit dieser Anweisung wird das Array myArr deklariert, das Integerzahlen beschreibt. 
Um wie viele es sich handelt, ist noch nicht festgelegt.
Die Kennzeichnung als Array erfolgt durch die eckigen Klammern, die hinter dem Datentyp angegeben werden müssen.
Danach folgt der Bezeichner des Arrays.

Um das Array nun auch zu initialisiern müssen wir folgendes tun:

```csharp
int[] myArray;
myArray = new int[3];
```

Das Schlüsselwort new kennzeichnet die Erzeugung eines [Objekts](./oop.md), dahinter wird der Datentyp genannt.
Die Anzahl der Array-Elemente – man spricht auch von der Größe des Arrays – geht aus der Zahlenangabe in den eckigen Klammern hervor:  
In unserem Fall verwaltet das Array `myArray` genau drei Integer.
Die Angabe in den eckigen Klammern der Initialisierung ist immer eine Zahl vom Typ int.

Deklaration und Intialisierung können anstatt in einzelenen Schritten auch in einen Schritt gepackt werden:

```csharp
int[] myArray = new int[3];
```

Zudem ist es möglich auch direkt Daten in das Array bei der Initialisierung zu übergeben:

```csharp
int[] myArray = new int[3]{5, 3, 18};
int[] myArray = new int[]{5, 18}; //Alternative
int[] myArray = {5, 3, 18}; //Kurzform
```

!!! warning "Achtung"

    Folgende Intitialisierung ist, falsch:
    ```csharp
    int[] myArray = new int[3]{23};
    ```

## Zugriff auf Array-Elemente

Bei der Initialisierung eines Arrays werden die einzelnen Elemente durchnummeriert.
Dabei hat das erste Element den Index 0, das letzte Element den Index Anzahl der Elemente - 1.

Ein Array, das mit
```csharp
int[] myArray = new int[3];
```
deklariert und initialisiert worden ist, enthält somit drei Elemente:
```csharp
myArray[0]
myArray[1]
myArray[2]
```

Beabsichtigen wir, dem ersten Element des Arrays die Zahl 55 zuzuweisen, müsste die Anweisung wie folgt lauten:
```csharp
myArray[0] = 55;
```

Die Auswertung des Elementinhalts läuft analog:

```csharp
Console.WriteLine(myArray[0]);
```

## Weiterführende Links

* [Arrays (C#-Programmierhandbuch) | Microsoft](https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/arrays/)
* [Datenfelder | Visual C# 2012 - das umfassende Handbuch](https://openbook.rheinwerk-verlag.de/visual_csharp_2012/1997_02_005.html#dodtp0b8cce7d-3fbf-4a06-95aa-3492d6f3c9c7)

### Videos

* [Arrays | Programmieren Starten](https://www.youtube.com/watch?v=MXsFBBN4OXk&list=PL_pqkvxZ6ho18awjThtUMZio-yvc79TGi&index=8)