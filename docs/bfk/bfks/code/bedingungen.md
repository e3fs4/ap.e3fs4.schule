---
tags:
- FISI
- FIAE
---

# Bedingungen

## Weiterführende Links

* [Verzweigungen und Schleifen | Microsoft](https://learn.microsoft.com/de-de/dotnet/csharp/tour-of-csharp/tutorials/branches-and-loops-local)
* [Kontrollstrukturen | Visual C# 2012 - das umfassende Handbuch](https://openbook.rheinwerk-verlag.de/visual_csharp_2012/1997_02_006.html#dodtp40c3cf1f-8f10-4cd5-b66d-ece497b81c35)

### Videos

* [If-Abfragen | Programmieren Starten](https://www.youtube.com/watch?v=NljE9JeBI1M&list=PL_pqkvxZ6ho18awjThtUMZio-yvc79TGi&index=4)
* [Switch | Programmieren Starten](https://www.youtube.com/watch?v=US4VDVnudD8&list=PL_pqkvxZ6ho18awjThtUMZio-yvc79TGi&index=5)