---
tags:
- FISI
- FIAE
---

# Schleifen

Schleifen dienen dazu, Anweisungsfolgen wiederholt auszuführen.
Dabei wird zwischen bestimmten und unbestimmten Schleifentypen unterschieden.  
Bei bestimmten Schleifen ist beim Schleifeneintritt bekannt, wie oft die Anweisungsfolge durchlaufen werden muss.
Ergibt sich erst während des Schleifendurchlaufs, wann die Bearbeitung abgebrochen wird, spricht man von unbestimmten Schleifen.

## while-Schleife

Die while-Schleife bietet sich an, wenn erst zur Laufzeit der Anwendung festgelegt wird, wie oft der Schleifenkörper durchlaufen werden muss.  
In der while-Schleife werden die Anweisungen wiederholt, solange die Bedingung „true“ ist. 
Ist die Bedingung von Anfang an „false“, wird der Inhalt der while-Schleife nie ausgeführt. 
Die while-Schleife wird auch als kopfgesteuerte Schleife bezeichnet, da vor dem Schleifeneintritt die Bedingung überprüft wird!

```csharp
while(Bedingung)
{
    // Anweisung1
    // Anweisung2
    // ...
}
```

!!! warning "Wichtig"

    Die Bedingung muss durch Anweisungen innerhalb der Schleife irgendwann „false“ werden, da sonst eine endlose Schleife entsteht.

### Struktogramm

## do-while-Schleife

Die do-while-Schleife bietet sich an, wenn erst zur Laufzeit der Anwendung festgelegt wird, wie oft  der Schleifenkörper durchlaufen werden muss. 
Die do-while-Schleife ist also eine unbestimmte Schleife.  
In der do-while-Schleife werden die Anweisungen wiederholt, solange die Bedingung „true“ ist.
Ist die Bedingung von Anfang an „false“, wird der Inhalt der do-while-Schleife **einmal** ausgeführt.
Die do-while-Schleife wird auch als fußgesteuerte Schleife bezeichnet, da die Bedingung erst am Schleifenende überprüft wird!

```csharp
do
{
    // Anweisung1
    // Anweisung2
    // ...
} while(Bedingung)
```

!!! warning "Wichtig"

    Die Bedingung muss durch Anweisungen innerhalb der Schleife irgendwann „false“ werden, da sonst eine endlose Schleife entsteht.

### Struktogramm

## For-Schleife

### Struktogramm

## Weiterführende Links

* [Verzweigungen und Schleifen | Microsoft](https://learn.microsoft.com/de-de/dotnet/csharp/tour-of-csharp/tutorials/branches-and-loops-local)
* [Programmschleifen | Visual C# 2012 - das umfassende Handbuch](https://openbook.rheinwerk-verlag.de/visual_csharp_2012/1997_02_007.html#dodtp75533eec-0149-4966-892c-3af285f78ef8)

### Videos

* [While und Do-While Schleifen | Programmieren Starten](https://www.youtube.com/watch?v=-O9R80Nrj_4&list=PL_pqkvxZ6ho18awjThtUMZio-yvc79TGi&index=6)
* [For-Schleifen | Programmieren Starten](https://www.youtube.com/watch?v=poOdKu9qBz4&list=PL_pqkvxZ6ho18awjThtUMZio-yvc79TGi&index=7)