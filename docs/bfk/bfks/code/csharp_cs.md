---
tags:
- FISI
- FIAE
---

# C# Cheatsheet

## Variablen

### Syntax 

``` csharp
DataType variableName = value;
```

### Namensregeln

- Variablen müssen mit einem Buchstaben oder Unterstrich starten
- Variablennamen dürfen keinen Leerzeichen enthalten
- Variablennamen dürfen Zahlen enthalten
- Variablennamen dürfen keine Sonderzeichen enthalten (Außnahme: Unterstrich)

### Beispiele

``` csharp
string name = "IT-Schule";
int count = 5;
```

## Datentypen

| Datentyp | Größe | Beschreibung |
| -------- | ----- | ------------ |
| `int` | 4 bytes | Speichert ganze Zahlen von -2,147,483,648 bis 2,147,483,647 |
| `long` | 8 bytes | Speichert ganze Zahlen von -9,223,372,036,854,775,808 bis 9,223,372,036,854,775,807 |
| `float` | 4 bytes | Speichert Bruchzahlen (Kommazahlen). Ausreichend für die Speicherung von 6 bis 7 Dezimalziffern | 
| `double` | 8 bytes | Speichert Bruchzahlen (Kommazahlen). Ausreichend für die Speicherung von 15 Dezimalziffern |
| `bool` | 1 bit | Speichert Wahrheitswerte (True oder false) |
| `char` | 2 bytes | Speichert ein einzelnes Zeichen/Buchstabe, umgeben von Anführungszeichen | 
| `string`| 2 bytes per character | Speichert eine Zeichenfolge, umgeben von Anführungszeichen |

## Strings

### Konkatenation / Zusammenführung

``` csharp
Console.WriteLine("Hello" + "World");
```

### Neue Zeile

``` csharp
Console.WriteLine("Hello \n" + "World")
```
## Arrays

### Syntax

``` csharp
DataType[] arrayName = { Comma Separated Values }; //Keine fixe Größe
DataType[] arrayName = new DataType[3] {Comma Separated Values}; //Array mit der Größe 3
```

### Beispiele

``` csharp
int[] noten = {1, 2, 3, 4, 5, 6};
string[] faecher = new string[5] {"GK", "Deutsch", "Englisch", "Wi", "BfK"};
```

## Bedingte Anweisungen / Conditional Statements

### If Bedingung

#### Syntax

```csharp
if (true)
{
    Anweisung
}
```

#### Beispiel

``` csharp
if (count == 5)
{
    Console.WriteLine("Hello World!");
}
```

### If-Else Bedingung

``` csharp
if (count == 5)
{
    Console.WriteLine("Hello World");
}
else
{
    Console.WriteLine(count);
}
```

### Switch Statement

#### Syntax

```csharp
switch (switch_on)
{
    case firstCase :
        code;
        break;
    default :
        code;
        break;
}
```

#### Beispiel

``` csharp
switch (name)
{
    case "It-Schule" :
        Console.WriteLine("It-Schule");
        break;
    case "Institut Dr. Flad" :
        Console.WriteLine("Institut Dr. Flad");
        break;
    default :
        Console.WriteLine(name);
        break;
}
```

## Schleifen

### While Schleife

#### Syntax

``` csharp
while (true)
{
    code;
}
```

#### Beispiel

``` csharp
while (count < 5)
{
    Console.WriteLine("Hello World");
}
```

### For Schleife

#### Syntax

``` csharp
for (int i = 0; i < length; i++)
{
    code;
}
```

#### Beispiel

``` csharp
for (int i = 0; i <= 100; i++)
{
    Console.WriteLine(i);
}
```

### Foreach Schleife

#### Syntax

``` csharp
foreach (var item in collection)
{
    code;
}
```

#### Beispiel

``` csharp
foreach (string fach in faecher)
{
    Console.WriteLine(fach);
}
```

## Methoden

### Syntax

```csharp
public void MethodenName()
{
    //Methode ohne Rückgabewert
}
```

```csharp
public void MethodenName(DataType var)
{
    //Methode mit Übergabeparameter
}
```

```csharp
public DataType MethodenName()
{
    //Methode mit Rückgabewert
}
```

### Beispiele

```csharp
public void WelcomeUser(string name)
{
    Console.WriteLine("Hello " + name + "!");
}
```

```csharp
public int Counter()
{
    count++;
    return count;
}
```

## Klassen

### Syntax

```csharp
Class MyClassName
{

}
```

