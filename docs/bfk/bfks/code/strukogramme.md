---
tags:
- FISI
- FIAE
---

# Struktogramme

Struktogramme sind eine Möglichkeit, Algorithmen unabhängig von einer 
Programmiersprache aufzuschreiben. Sie werden nach ihren Entwicklern auch 
Nassi-Shneiderman-Diagramme genannt.

Struktogramme sind Veranschaulichungen von Algorithmen mittels einfacher 
geometrischer Formen. 
Jedes Rechteck ist mit einer elementaren Anweisung beschriftet oder es 
stellt eine Kontrollstruktur wie z.B. eine Schleife oder eine Verzweigung dar. 
Rechtecke können aufeinander gestapelt und ineinander geschachtelt werden.

## Sequenz
Anweisungen welche aufeinander gestapelt werden.  
Sie werden von oben nach unten abgearbeitet.

![Sequenz](../../../img/bfks/strukogramm_sequenz.jpg)

## Schleife
Schleife mit Anweisungen, die ausgeführt werden bis eine 
Abbruchbedingung erfüllt ist oder sie läuft unendlich weiter.
- Wiederhole fortlaufend
- Wiederhole 10-mal
- Wiederhole bis Rand erreicht

![Schleife](../../../img/bfks/strukogramm_schleife.jpg)

## Verzweigung (If Anweisung)
Wenn Anweisung, dann dies oder jenes.  
Wenn keine alternative, dann Alternativblock leer lassen.

![Verzweigung](../../../img/bfks/strukogramm_if.jpg)