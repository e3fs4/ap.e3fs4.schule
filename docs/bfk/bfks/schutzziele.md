---
tags:
- FISI
- FIAE
---

# IT-Schutzziele CIA Model

![CIA Model](../../img/bfks/cia.png)

## Vertraulichkeit (Confidentiality):
Schutz vor unbefugter Preisgabe von Informationen.  
Vertrauliche Daten und Informationen dürfen ausschließlich befugten in der 
zulässigen Weise zugänglich sein.

## Integrität (Integrity):
Die Integrität bezieht sich auf die Richtigkeit und Vollständigkeit von Informationen.  
Es ist wichtig sicherzustellen, dass Daten nicht manipuliert, 
beschädigt oder gestohlen werden können.

## Verfügbarkeit (Availability):
Jederzeit verfügbar und zugänglich sind, wenn sie benötigt werden.
