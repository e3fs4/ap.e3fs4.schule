---
tags:
- FIAE
---

# SOLID Prinzipien
Das SOLID-Prinzip ist ein Konzept der Softwareentwicklung, das aus fünf Grundsätzen besteht, 
die helfen sollen, eine gute Codequalität zu erreichen.

![SOLID IMG](../../img/bfks/solid_principles.png)


## Single Responsibility Principle (SRP):
Eine Klasse sollte nur eine einzige Verantwortlichkeit haben, um Änderungen an einer Verantwortlichkeit 
nicht die Funktionalität einer anderen zu beeinträchtigen. 

### Beispiel:

---

#### Negativ:

`````java
class DbAccess {
    public writeToDb() {};
    public validateInput() {};
}
`````

Die Klasse DbAccess sollte sich nicht um Datenbank zugriff und die validierung des Inputs kümmern.

#### Positiv:
`````java
class DbAccess {
    public writeToDb() {};
}
class InputValidators {
    public validateInput() {};
}
`````

## Open/Closed Principle (OCP):
Eine Klasse sollte offen für Erweiterungen, aber geschlossen für Änderungen sein, 
sodass neue Funktionalitäten hinzugefügt werden können, ohne den bestehenden Code zu ändern.

### Beispiel:

---

#### Negativ:

`````java
class Shape {
    public getArea() {
        switch (shape) {
            case "rect":
                return;
            case "circle":
                return;
        }
    }
}
`````

Der Code kann zu Fehlern führen, da ein Rechteck und ein Kreis unterschiedliche Eigenschaften haben.  
Die Klasse Shape kann ein Interface sein, welches die Methode enthält.

#### Positiv:
`````java
interface Shape {
    public getArea();
}

class Circle implements Shape {
    public getArea() {};
}
`````

## Liskov Substitution Principle (LSP):
Eine Instanz einer abgeleiteten Klasse sollte anstelle ihrer Basisklasse verwendet werden können, 
ohne die Funktionalität des Programms zu beeinträchtigen. 

### Beispiel:

---

#### Negativ:

`````java
class Door {
    public void open() {}
};

class Rectangle extends Door { };

class sys {
    void main() {
        Rectangle rect = new Rectangle();
        rect.open();
    };
}
`````

Ein rechteck kann nicht geöffnet werden, die abstrakte Klasse sollte nicht benutzt werden!

#### Positiv:
`````java
class Door {
    public void open() {}
};

class AutomaticDoor extends Door { };

class sys {
    void main() {
        AutomaticDoor door = new AutomaticDoor();
        door.open();
    };
}
`````

## Interface Segregation Principle (ISP):
Klienten sollten nicht gezwungen sein, Methoden zu implementieren, die sie nicht benötigen, 
und Schnittstellen sollten aufgeteilt werden, um dies zu ermöglichen. 

### Beispiel:

---

#### Negativ:

`````java
interface Printer {
    void print();
}

class Scanner implements Printer {
    @java.lang.Override
    public void print() {
        return null;
    }
}
`````

Ein Scanner kann nicht drucken. Deshalb würde die Methode print() nicht benutzt werden.

#### Positiv:
`````java
interface Machine {
    void work();
}

class Scanner implements Printer {
    @java.lang.Override
    public void work() {
        System.out.println("Working...");
    }
}
`````

## Dependency Inversion Principle (DIP):
Abhängigkeiten sollten auf abstrakten Klassen oder Schnittstellen beruhen, anstatt auf konkreten Klassen, 
um die Flexibilität und Wiederverwendbarkeit des Codes zu verbessern.

### Beispiel:

---

#### Negativ:

`````java
public class MySQLConnection {
    public void connect() {}
}

public class OracleConnection {
    public void connect() {}
}

public class DatabaseUser {
    private MySQLConnection mySQLConnection;
    private OracleConnection oracleConnection;

    public MySQLDatabaseUser(MySQLConnection connection) {
        this.mySQLConnection = mySQLConnection;
    }

    public OracleDatabaseUser(OracleConnection connection) {
        this.oracleConnection = oracleConnection;
    }
}
`````

Ein Scanner kann nicht drucken. Deshalb würde die Methode print() nicht benutzt werden.

#### Positiv:
`````java
public interface DatabaseConnection {
    void connect();
}

public class MySQLConnection implements DatabaseConnection {
    public void connect() { }
}

public class OracleConnection implements DatabaseConnection {
    public void connect() {}
}

public class DatabaseUser {
    private DatabaseConnection connection;

    public DatabaseUser(DatabaseConnection connection) {
        this.connection = connection;
    }
}
`````

## Vorteile bei Anwendung:
- **Wartbarkeit**: SOLID-Prinzipien können die Wartbarkeit von Code verbessern, indem sie sicherstellen, 
dass Änderungen an einer Stelle den Rest des Codes nicht beeinträchtigen.
- **Erweiterbarkeit**: Die Einhaltung von SOLID-Prinzipien kann die Erweiterbarkeit von Code verbessern, 
indem sie sicherstellen, dass neue Funktionalitäten einfach hinzugefügt werden können, 
ohne den vorhandenen Code zu beeinträchtigen. 
- **Testbarkeit**: SOLID-Prinzipien können die Testbarkeit von Code verbessern, indem sie sicherstellen, 
dass jeder Teil des Codes unabhängig von anderen Teilen getestet werden kann. 
- **Lesbarkeit**: SOLID-Prinzipien können die Lesbarkeit von Code verbessern, indem sie sicherstellen, 
dass Code leichter verständlich und nachvollziehbar ist. 
- **Wiederverwendbarkeit**: SOLID-Prinzipien können die Wiederverwendbarkeit von Code verbessern, 
indem sie sicherstellen, dass Code in verschiedenen Kontexten verwendet werden kann.