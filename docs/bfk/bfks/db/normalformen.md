---
tags:
- FISI
- FIAE
---

# Normalformen

## 1. Normalform
Eine Tabelle darf keine mehrwertigen Attribute enthalten.

### Beispiel:

#### Negativ:

| Mitarbeiter-ID | E-mail          | Tel-Nummer             |
|----------------|-----------------|------------------------|
| 001            | max@example.com | 0123456789, 9876543210 |

In dieser Tabelle gibt es ein mehrwertiges Attribut, 
da Telefonnummern in einer Zelle gespeichert sind.  
Dies ist ein Verstoß gegen die 1NF.

#### Positiv:

| Mitarbeiter-ID | E-mail          | Tel-Nummer1 | Tel-Nummer2 |
|----------------|-----------------|-------------|-------------|
| 001            | max@example.com | 0123456789  | 9876543210  |

## 2. Normalform
Erste Normalform + Nichtschlüsselattribut funktional vom gesamten Schlüssel abhängt.

### Beispiel:

#### Negativ:

| Rechnungsnummer | Produkt-ID | Produktname | Preis | Menge |
|-----------------|------------|-------------|-------|-------|
| 001             | 100        | T-Shirt     | 19.99 | 2     |

In dieser Tabelle ist das Attribut "Einzelpreis" funktional abhängig 
von der "Produkt-ID", aber nicht vom Primärschlüssel "Rechnungsnummer". 
Dies ist ein Verstoß gegen die 2NF.

#### Positiv:

| Rechnungsnummer | Produkt-ID | Menge |
|-----------------|------------|-------|
| 001             | 100        | 2     |

| Produkt-ID | Produktname | Preis |
|------------|-------------|-------|
| 100        | T-Shirt     | 19.99 |

## 3. Normalform
Zweite Normalform + Nichtschlüssel darf nicht von anderem Nichtschlüssel abhängen.

### Beispiel:

#### Negativ:

| ID  | Name  | Straße     | PLZ   | Ort         |
|-----|-------|------------|-------|-------------|
| 001 | Heinz | Sackstraße | 70794 | Filderstadt |

PLZ und Ort gehören zusammen.

#### Positiv:

| ID  | Name  | Straße    | PLZ   |
|-----|-------|-----------|-------|
| 001 | Heinz | Sackgasse | 70794 |

| PLZ   | Ort         |
|-------|-------------|
| 70794 | Filderstadt |