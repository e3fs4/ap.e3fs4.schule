---
tags:
- FISI
---

# Berufsfachliche Kompetenz

## Lehrplan

### Lernfelder

#### Übersicht über die Lernfelder für die Ausbildungsberufe Fachinformatiker/-in und IT-System-Elektroniker/-in

* [Das Unternehmen und die eigene Rolle im Betrieb beschreiben](index.md#lernfeld-1-das-unternehmen-und-die-eigene-rolle-im-betrieb-beschreiben)
* [Arbeitsplätze nach Kundenwunsch ausstatten](index.md#lernfeld-2-arbeitsplatze-nach-kundenwunsch-ausstatten)
* [Clients in Netzwerke einbinden](index.md#lernfeld-3-clients-in-netzwerke-einbinden)
* [Schutzbedarfsanalyse im eigenen Arbeitsbereich durchführen](index.md#lernfeld-4-schutzbedarfsanalyse-im-eigenen-arbeitsbereich-durchfuhren)
* [Software zur Verwaltung von Daten anpassen](index.md#lernfeld-5-software-zur-verwaltung-von-daten-anpassen)
* [Serviceanfragen bearbeiten](index.md#lernfeld-6-serviceanfragen-bearbeiten)
* [Cyber-physische Systeme ergänzen](index.md#lernfeld-7-cyber-physische-systeme-erganzen) 
* [Daten systemübergreifend bereitstellen](index.md#lernfeld-8-daten-systemubergreifend-bereitstellen)
* [Netzwerke und Dienste bereitstellen](index.md#lernfeld-9-netzwerke-und-dienste-bereitstellen)

#### Fachinformatiker/-in der Fachrichtung Systemintegration

* [Serverdienste bereitstellen und Administrationsaufgaben automatisieren](index.md#lernfeld-10b-serverdienste-bereitstellen-und-administrationsaufgaben-automatisieren)
* [Betrieb und Sicherheit vernetzter Systeme gewährleisten](index.md#lernfeld-11b-betrieb-und-sicherheit-vernetzter-systeme-gewahrleisten)
* [Kundenspezifische Systemintegration durchführen](index.md#lernfeld-12b-kundenspezifische-systemintegration-durchfuhren)

#### Lernfeld 1: Das Unternehmen und die eigene Rolle im Betrieb beschreiben

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, ihr Unternehmen hinsichtlich seiner Wertschöpfungskette zu präsentieren und ihre eigene Rolle im Betrieb zu beschreiben.**
* Die Schülerinnen und Schüler **informieren** sich, auch anhand des Unternehmensleitbildes, über die ökonomischen, ökologischen und sozialen Zielsetzungen des Unternehmens.
* Sie **analysieren** die Marktstruktur in ihrer Branche und ordnen das Unternehmen als komplexes System mit seinen Markt- und Kundenbeziehungen ein. Sie beschreiben die Wertschöpfungskette und ihre eigene Rolle im Betrieb.
* Dabei erkunden sie die Leistungsschwerpunkte sowie Besonderheiten ihres Unternehmens und setzen sich mit der Organisationsstruktur (Aufbauorganisation) und Rechtsform auseinander. Sie informieren sich über den eigenen Handlungs- und Entscheidungsspielraum im Unternehmen (Vollmachten) sowie über Fort- und Weiterbildungsmaßnahmen.
* Sie planen und **erstellen**, auch im Team, adressatengerecht multimediale Darstellungen zu ihrem Unternehmen.
* Die Schülerinnen und Schüler **präsentieren** ihre Ergebnisse.
* Sie **überprüfen** kriteriengeleitet die Qualität ihres Handlungsproduktes und entwickeln gemein-
sam Verbesserungsmöglichkeiten.
* Sie **reflektieren** die eigene Rolle und das eigene Handeln im Betrieb.

#### Lernfeld 2: Arbeitsplätze nach Kundenwunsch ausstatten

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, die Ausstattung eines Arbeitsplatzes nach Kundenwunsch zu dimensionieren, anzubieten, zu beschaffen und den Arbeitsplatz an die Kunden zu übergeben.**
* Die Schülerinnen und Schüler nehmen den Kundenwunsch für die Ausstattung eines Arbeitsplatzes von internen und externen Kunden entgegen und **ermitteln** die sich daraus ergebenden Anforderungen an Soft- und Hardware. Aus den dokumentierten Anforderungen leiten sie Auswahlkriterien für die Beschaffung ab. Sie berücksichtigen dabei die Einhaltung von Normen und Vorschriften (Zertifikate, Kennzeichnung) für den Betrieb und die Sicherheit von elektrischen Geräten und Komponenten.
* Sie **vergleichen** die technischen Merkmale relevanter Produkte anhand von Datenblättern und Produktbeschreibungen zur Vorbereitung einer Auswahlentscheidung (Nutzwertanalyse). Dabei beachten sie insbesondere informationstechnische und energietechnische Kenngrößen sowie Aspekte der Ergonomie und der Nachhaltigkeit (Umweltschutz, Recycling). Sie wenden Recherchemethoden an und werten auch fremdsprachliche Quellen aus.
* Sie ermitteln die Energieeffizienz unterschiedlicher Arbeitsplatzvarianten und dokumentieren
diese.
* Sie vergleichen mögliche Bezugsquellen (quantitativer und qualitativer Angebotsvergleich) und **bestimmen** den Lieferanten.
* Auf Basis der ausgewählten Produkte und Lieferanten **erstellen** sie mit vorgegebenen Zuschlagssätzen ein Angebot für die Kunden.
* Sie schließen den Kaufvertrag ab und organisieren den Beschaffungsprozess unter Berücksichtigung von Lieferzeiten. Sie nehmen die bestellten Komponenten in Empfang und dokumentie ren dabei festgestellte Mängel.
* Sie bereiten die Übergabe der beschafften Produkte vor, integrieren IT-Komponenten, konfigurieren diese und nehmen sie unter Berücksichtigung der Arbeitssicherheit in Betrieb. Sie übergeben den Arbeitsplatz an die Kunden und erstellen ein Übergabeprotokoll.
* Sie **bewerten** die Durchführung des Kundenauftrags und **reflektieren** ihr Vorgehen. Dabei berücksichtigen sie die Kundenzufriedenheit und formulieren Verbesserungsvorschläge.

#### Lernfeld 3: Clients in Netzwerke einbinden

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, eine Netzwerkinfrastruktur zu analysieren sowie Clients zu integrieren.**
* Die Schülerinnen und Schüler **erfassen** im Kundengespräch die Anforderungen an die Integration von Clients (Soft- und Hardware) in eine bestehende Netzwerkinfrastruktur und leiten Leistungskriterien ab.
* Sie **informieren** sich über Strukturen und Komponenten des Netzwerkes und erfassen deren Eigenschaften und Standards. Dazu verwenden sie technische Dokumente, auch in fremder Sprache. Sie nutzen physische sowie logische Netzwerkpläne und beachten betriebliche Sicherheitsvorgaben.
* Sie **planen** die Integration in die bestehende Netzwerkinfrastruktur indem sie ein anforderungsgerechtes Konzept auch unter ökologischen und wirtschaftlichen Gesichtspunkten (Energieeffizienz) erstellen.
* Sie **führen** auf der Basis der Leistungskriterien die Auswahl von Komponenten **durch**. Sie konfigurieren Clients und binden diese in das Netzwerk ein.
* Sie **prüfen** systematisch die Funktion der konfigurierten Clients im Netzwerk und protokollieren das Ergebnis.
* Sie **reflektieren** den Arbeitsprozess hinsichtlich möglicher Optimierungen und diskutieren das Ergebnis in Bezug auf Wirtschaftlichkeit und Ökologie.

#### Lernfeld 4: Schutzbedarfsanalyse im eigenen Arbeitsbereich durchführen

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, mit Hilfe einer bestehenden Sicherheitsleitlinie eine Schutzbedarfsanalyse zur Ermittlung der Informationssicherheit auf Grundschutzniveau in ihrem Arbeitsbereich durchzuführen.**
* Die Schülerinnen und Schüler **informieren** sich über Informationssicherheit (Schutzziele) und rechtliche Regelungen sowie die Einhaltung von betrieblichen Vorgaben zur Bestimmung des Schutzniveaus für den eigenen Arbeitsbereich.
* Sie **planen** eine Schutzbedarfsanalyse, indem sie gemäß der IT-Sicherheitsleitlinie des Unternehmens Schutzziele des Grundschutzes (Vertraulichkeit, Integrität, Verfügbarkeit) in ihrem Arbeitsbereich ermitteln und eine Klassifikation von Schadensszenarien vornehmen.
* Sie **entscheiden** über die Gewichtung möglicher Bedrohungen unter Berücksichtigung der Schadenszenarien.
* Dazu **führen** sie eine Schutzbedarfsanalyse in ihrem Arbeitsbereich **durch**, nehmen Bedrohungsfaktoren auf und dokumentieren diese.
* Die Schülerinnen und Schüler **bewerten** die Ergebnisse der Schutzbedarfsanalyse und gleichen diese mit der IT-Sicherheitsleitlinie des Unternehmens ab. Sie empfehlen Maßnahmen und setzen diese im eigenen Verantwortungsbereich um.
* Sie **reflektieren** den Arbeitsablauf und übernehmen Verantwortung im IT-Sicherheitsprozess.

#### Lernfeld 5: Software zur Verwaltung von Daten anpassen

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, Informationen mittels Daten abzubilden, diese Daten zu verwalten und dazu Software anzupassen.**
* Die Schülerinnen und Schüler informieren sich innerhalb eines Projektes über die Abbildung von Informationen mittels Daten. Dabei **analysieren** sie Daten hinsichtlich Herkunft, Art, Verfügbarkeit, Datenschutz, Datensicherheit und Speicheranforderung und berücksichtigen Datenformate und Speicherlösungen.
* Sie **planen** die Anpassung einer Anwendung zur Verwaltung der Datenbestände und entwickeln Testfälle. Dabei **entscheiden** sie sich für ein Vorgehen.
* Die Schülerinnen und Schüler **implementieren** die Anpassung der Anwendung, auch im Team und erstellen eine Softwaredokumentation.
* Sie testen die Funktion der Anwendung und **beurteilen** deren Eignung zur Bewältigung der gestellten Anforderungen.
* Sie **evaluieren** den Prozess der Softwareentwicklung.

#### Lernfeld 6: Serviceanfragen bearbeiten

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, Serviceanfragen einzuordnen, Fehlerursachen zu ermitteln und zu beheben**
* Die Schülerinnen und Schüler nehmen Serviceanfragen entgegen (direkter und indirekter Kundenkontakt). Sie **analysieren** Serviceanfragen und prüfen deren vertragliche Grundlage (Service-Level-Agreement). Sie ermitteln die Reaktionszeit und dokumentieren den Status der Anfragen im zugrundeliegenden Service-Management-System.
* Durch systematisches Fragen **ordnen** die Schülerinnen und Schüler Serviceanfragen unter Berücksichtigung des Support-Levels und fachlicher Standards **ein**.
* Sie **ermitteln** Lösungsmöglichkeiten im Rahmen des Support-Levels. Auf dieser Basis bearbeiten sie das Problem und dokumentieren den Bearbeitungsstatus. Sie kommunizieren mit den Prozessbeteiligten situationsgerecht, auch in einer Fremdsprache, und passen sich den unterschiedlichen Kommunikationsanforderungen an (Kommunikationsmodelle, Deeskalationsstrategien).
* Sie **reflektieren** den Bearbeitungsprozess der Serviceanfragen und ihr Verhalten in Gesprächssituationen. Die Schülerinnen und Schüler diskutieren die Servicefälle und schlagen Maßnahmen zur Qualitätssteigerung vor.

#### Lernfeld 7: Cyber-physische Systeme ergänzen

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, die physische Welt und IT-Systeme funktional zu einem cyber-physischen System zusammenzuführen.**
* Die Schülerinnen und Schüler analysieren ein cyber-physisches System bezüglich eines Kundenauftrags zur Ergänzung und Inbetriebnahme weiterer Komponenten.
* Sie **informieren** sich über den Datenfluss an der Schnittstelle zwischen physischer Welt und IT-System sowie über die Kommunikation in einem bestehenden Netzwerk. Sie verschaffen sich einen Überblick über die Energie-, Stoff- und Informationsflüsse aller am System beteiligten Geräte und Betriebsmittel.
* Die Schülerinnen und Schüler **planen** die Umsetzung des Kundenwunsches, indem sie Kriterien für die Auswahl von Energieversorgung, Hardware und Software (Bibliotheken, Protokolle) aufstellen. Dazu nutzen sie Unterlagen der technischen Kommunikation und passen diese an.
* Sie **führen** Komponenten mit dem cyber-physischen System funktional **zusammen**.
* Sie **prüfen** systematisch die Funktion, messen physikalische Betriebswerte, validieren den Energiebedarf und protokollieren die Ergebnisse.
* Die Schülerinnen und Schüler **reflektieren** den Arbeitsprozess hinsichtlich möglicher Optimierungen und diskutieren das Ergebnis in Bezug auf Betriebssicherheit und Datensicherheit.

#### Lernfeld 8: Daten systemübergreifend bereitstellen

* **Die Schülerinnen und Schüler besitzen die Kompetenz, Daten aus dezentralen Quellen zusammenzuführen, aufzubereiten und zur weiteren Nutzung zur Verfügung zu stellen.**
* Die Schülerinnen und Schüler ermitteln für einen Kundenauftrag Datenquellen und **analysieren** diese hinsichtlich ihrer Struktur, rechtlicher Rahmenbedingungen, Zugriffsmöglichkeiten und -mechanismen.
* Sie **wählen** die Datenquellen (heterogen) für den Kundenauftrag **aus**.
* Sie **entwickeln** Konzepte zur Bereitstellung der gewählten Datenquellen für die weitere Verarbeitung unter Beachtung der Informationssicherheit.
* Die Schülerinnen und Schüler **implementieren** arbeitsteilig, auch ortsunabhängig, ihr Konzept mit vorhandenen sowie dazu passenden Entwicklungswerkzeugen und Produkten.
* Sie übergeben ihr Endprodukt mit Dokumentation zur Handhabung, auch in fremder Sprache, an die Kunden.
* Sie **reflektieren** die Eignung der eingesetzten Entwicklungswerkzeuge hinsichtlich des arbeitsteiligen Entwicklungsprozesses und die Qualität der Dokumentation.

#### Lernfeld 9: Netzwerke und Dienste bereitstellen

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, Netzwerke und Dienste zu planen, zu konfigurieren und zu erweitern.**
* Die Schülerinnen und Schüler ermitteln die Anforderungen an ein Netzwerk in Kommunikation mit den Kunden. Sie **informieren** sich über Eigenschaften, Funktionen und Leistungsmerkmale der Netzwerkkomponenten und Dienste nach Kundenanforderung, auch unter Berücksichtigung sicherheitsrelevanter Merkmale. Dabei wenden sie Recherchemethoden an und werten auch fremdsprachliche Quellen aus.
* Sie **planen** die erforderlichen Dienste und dafür notwendige Netzwerke sowie deren Infrastruktur unter Berücksichtigung interner und externer Ressourcen.
* Dazu **vergleichen** sie Konzepte hinsichtlich ihrer Nachhaltigkeit sowie der technischen und wirtschaftlichen Eignung.
* Sie **installieren** und konfigurieren Netzwerke sowie deren Infrastruktur und implementieren Dienste. Sie gewährleisten die Einhaltung von Standards, führen Funktionsprüfungen sowie Messungen durch und erstellen eine Dokumentation.
* Die Schülerinnen und Schüler **beurteilen** die Netzwerke sowie deren Infrastruktur und die Dienste hinsichtlich der gestellten Anforderungen, Datensicherheit und Datenschutz.
* Sie **reflektieren** ihre Lösung unter Berücksichtigung der Kundenzufriedenheit, Zukunftsfähigkeit und Vorgehensweise.

#### Lernfeld 10b: Serverdienste bereitstellen und Administrationsaufgaben automatisieren

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, Serverdienste bereitzustellen, zu administrieren und zu überwachen.**
* Die Schülerinnen und Schüler **informieren** sich über Serverdienste sowie Plattformen.
* Sie **wählen** diese gemäß den Kundenanforderungen aus. Dabei berücksichtigen sie auch Verfügbarkeit, Skalierbarkeit, Administrierbarkeit, Wirtschaftlichkeit und Sicherheit.
* Sie planen die Konfiguration der ausgewählten Dienste und erstellen Konzepte zur Einrichtung, Aktualisierung, Datensicherung und Überwachung.
* Sie implementieren die Dienste unter Berücksichtigung betrieblicher Vorgaben und Lizenzierungen. Sie wenden Testverfahren an, überwachen die Dienste und empfehlen den Kunden Maßnahmen bei kritischen Zuständen. Sie dokumentieren ihre Ergebnisse.
* Sie automatisieren Administrationsprozesse in Abhängigkeit kundenspezifischer Rahmenbedingungen, **testen** und optimieren die Automatisierung.
* Die Schülerinnen und Schüler **reflektieren** ihre Lösung und beurteilen sie hinsichtlich der Kundenanforderungen.

#### Lernfeld 11b: Betrieb und Sicherheit vernetzter Systeme gewährleisten

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, mit Hilfe einer Risikoanalyse den Schutzbedarf eines vernetzten Systems zu ermitteln und Schutzmaßnahmen zu planen, umzusetzen und zu dokumentieren.**
* Die Schülerinnen und Schüler bereiten sich auf ein Kundengespräch zur Identifizierung eines Schutzbedarfes vor. Hierzu informieren sie sich über Informationssicherheit in vernetzten Systemen.
* Sie ermitteln im Kundengespräch die Schutzziele, **analysieren** die Systeme hinsichtlich der Anforderungen an die Informationssicherheit und benennen Risiken.
* Die Schülerinnen und Schüler **planen** unter Beachtung betrieblicher IT-Sicherheitsleitlinien und rechtlicher Regelungen die Vorkehrungen und Maßnahmen zur Minimierung der Wahrscheinlichkeit eines Schadenseintritts.
* Sie **implementieren** die Maßnahmen unter Berücksichtigung technischer und organisatorischer Rahmenbedingungen.
* Sie **prüfen** die Sicherheit des vernetzten Systems und **bewerten** das erreichte Sicherheitsniveau in Bezug auf die Kundenanforderungen, eingesetzter Maßnahmen und Wirtschaftlichkeit. Sie erstellen eine Dokumentation und informieren die Kunden über die Ergebnisse der Risikoanalyse.
* Die Schülerinnen und Schüler **reflektieren** den Arbeitsprozess hinsichtlich möglicher Optimierungen und diskutieren das Ergebnis in Bezug auf den Begriff der relativen Sicherheit des vernetzten Systems.

#### Lernfeld 12b: Kundenspezifische Systemintegration durchführen

* **Die Schülerinnen und Schüler verfügen über die Kompetenz, einen Kundenauftrag zur Systemintegration vollständig durchzuführen und zu bewerten.**
* Die Schülerinnen und Schüler **führen** in Zusammenarbeit mit den Kunden eine Anforderungsanalyse **durch** und leiten daraus Projektziele, Anforderungen, gewünschte Ergebnisse, Schulungsbedarfe und Rahmenbedingungen ab.
* Auf dieser Basis **planen** und kalkulieren sie ein Projekt mit den dazugehörigen personellen und technischen Ressourcen.
* Die Schülerinnen und Schüler entwickeln Lösungsvarianten, vergleichen diese anhand festgelegter Kriterien sowie unter Berücksichtigung von Datenschutz und Datensicherheit. Sie **wählen** mit den Kunden die beste Lösung **aus**. Für den vereinbarten Auftrag erstellen sie ein Dokument über die zu erbringenden Leistungen und ein Angebot.
* Die Schülerinnen und Schüler **implementieren** die gewünschte Lösung. Dabei nutzen sie Maßnahmen zur Qualitätssicherung. Sie präsentieren den Kunden das Projektergebnis und führen eine Schulung durch. Sie übergeben den Kunden das Produkt sowie die Dokumentation.
* Die Schülerinnen und Schüler **bewerten** das Projektergebnis auch hinsichtlich Zielerreichung, Wirtschaftlichkeit, Skalierbarkeit und Verlässlichkeit.
* Sie **reflektieren** die Projektdurchführung und das Projektergebnis auch unter Berücksichtigung der kritisch-konstruktiven Kundenrückmeldungen.

### Lehrplan Download

Hier der direkte Link zum Download des Lehrplans als PDF:
[Lehrplan](./lehrplan-berufsschule.pdf)