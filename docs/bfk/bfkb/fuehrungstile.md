# Führungsstile

## Kooperativer Führungsstil

Andere Bezeichnungen für diesen Führungsstil sind demokratischer, kollegialer, sozial- integrativer oder partizipativer Führungsstil.

### Merkmale

* Der Vorgesetzte berücksichtigt alle relevanten Kenntnisse und Informationen der Mitarbeiter bei der Entscheidungsfindung. Aufgaben und Entscheidungsbefugnisse werden an die Mitarbeiter delegiert; sie übernehmen auch die Verantwortung für den Delegierten Aufgabenbereich.
* Ein hierarchischer Abstand zwischen Vorgesetzten und Mitarbeitern wird eher als hinderlich angesehen, eine persönliche Autorität des Vorgesetzten wird jedoch vorausgesetzt.
* Der Vorgesetzte setzt einen hohen Sachverstand bei seinen Mitarbeitern voraus und erwartet, dass diese im Rahmen ihrer festgelegten Aufgabenbereiche selbständig denken, handeln und entscheiden.
* Informationen, die der Mitarbeiter benötigt, werden leicht zugänglich gemacht.
* Die Mitarbeiter kontrollieren sich selbst oder der Vorgesetzte kontrolliert in Form von Ergebniskontrollen.
* Soziale Bedürfnisse, Statusstreben und der Wunsch nach Selbstverwirklichung werden befriedigt. Somit sind die Mitarbeiter zufrieden, freundlich und vertrauensvoll untereinander.

| Vorteile | Nachteile |
| -------- | --------- |
| Eigeninitiative und Kreativitätsförderung | Längere Entscheidungsprozesse |
| Ausfälle werden abgefedert | Starke Mitarbeiterkonkurrenz |
| Selbständige Mitarbeiter | | 

## Laissez-fair Führungsstil

## Merkmale

* Der Vorgesetzte bemüht sich nicht, bei den Mitarbeitern Interesse und Aktivität zu wecken und diese auf die Unternehmensziele auszurichten.
* Er kontrolliert die Mitarbeiter selten und lässt sie gewähren.
* Er entwickelt ein distanziertes Verhältnis zur Person des Mitarbeiters. Ihn interessiert nur die Leistung, um die persönlichen Probleme des Mitarbeiters kümmert er sich nicht.
* Er neigt zu einem weichen Kurs und übersieht unerwünschtes Verhalten von Mitarbeitern.
* Die Informationen fließen mehr oder weniger zufällig.

| Vorteile | Nachteile |
| -------- | --------- |
| Mitarbeiter können sich voll entfalten | Planlosigkeit und mangelnde Kontrolle |
| | Chaos, Rivalitäten & Kompetenzrangelei |
| | Mögliche Ausgrenzung Einzelner |

## Autoritärer Führungsstil

## Merkmale

* Entscheidungen werden vom Vorgesetzten allein ohne Anhören der Mitarbeiter getroffen.
* Aufgaben werden befehlsmäßig den „Untergebenen“ angeordnet, ohne sie zu begründen. Die Arbeitsanweisungen werden bis ins Detail festgelegt und die Mitarbeiter führen die Anordnungen nur aus, ohne Verantwortung und Kompetenz zu besitzen.
* Informationen werden nur auf dem Dienstweg weitergegeben und erhalten nur das zur Aufgabenerfüllung Notwendige.
* Der Vorgesetzte geht auf Distanz zu seinen Mitarbeitern und pocht auf seine Amtsautorität. Er erwartet von ihnen in erster Linie Pünktlichkeit, Ordnung, Aufrechterhaltung der Disziplin und Anerkennung der gegebenen Zustände.
* Er geht davon aus, dass er gegenüber seinen Mitarbeitern den größten Sachverstand besitzt, und glaubt, dass ohne sein Eigreifen und ständige Kontrolle keine Leistung zustande kommt.
* Kritik ist nicht konstruktiv, die Mitarbeiter erhalten kein persönliches Lob und keine Anerkennung.

| Vorteile | Nachteile |
| -------- | --------- |
| Schnelle Entscheidungen | Verantwortung bei einer Person |
| Klar geregelte Kompetenzen | Fehlentscheidungen bei Überforderung |
| Arbeitsprozess unter Kontrolle | Bei Ausfall Chaos |
