# Anfrage

Da für die Beschaffung der Ware häufig mehrere Bezugsquellen zur Auswahl stehen, ist es zweckmäßig, durch gezielte **Anfragen** alle Informationen zu erfragen, die für eine begründete Kaufentscheidung erforderlich sind.

## Zweck

Einholung von **Informationen** über das **Warenangebot** der Lieferanten (Artikel, Preise, Mengen, Lieferbedingungen)

## Inhalt

* Allgemeine Anfrage
    * Anforderung von Prospekten, Katalogen, Preislisten u.ä.
    * Bitte um persönliches Beratungsgespräch durch Außendienstmitarbeiter
* Bestimmte Anfrage
    * Informationsbedarf richtet sich auf eine konkrete Ware bzw. Warengruppe

## Rechtliche Wirkung

**Anfragen** sind rechtlich immer **unverbindlich!**

## Aufbau

![Aufbau Anfrage](../../img/bfkb/anfrage_Aufbau.jpg)