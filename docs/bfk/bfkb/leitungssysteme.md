# Leitungssysteme

## Einliniensystem

![Einliniensystem](../../img/bfkb/einliniensystem.png)

## Mehrliniensystem

![Mehrliniensystem](../../img/bfkb/mehrliniensystem.png)

### Vorteile

* direkte Kommunikationswege
* bessere Mitarbeiterkontrolle (durch mehr Vorgesetzte)
* Fachwissen der Vorgesetzten
* Entlastung ranghoher Instanzen

### Nachteile

* Widersprüchliche Arbeitsanweisungen können zu Missverständnissen führen
* Kompetenzüberschneidungen mit Konfliktpotenzial
* Probleme bei Abgrenzung der Zuständigkeit und Verantwortung

## Stab-Linien-System

Das Stabliniensystem ist ein Versuch, die Vorteile aus Einlinien- und Mehrliniensystem miteinander zu verknüpfen.  
Die einzelnen Stellen einer Linie werden den sogenannten Stäben zugeordnet.
Diese Stäbe stellen Wissensträger dar, welche in Form von beratenden Tätigkeiten dem potenziellen Wissensmangel bei den Linieninstanzen Abhilfe verschaffen sollen.
Der Stab hat dabei keine direkte Weisungsbefugnis.  
Die Verantwortung für die Erreichung der Unternehmensziele obliegt den Linien, der Stab dient lediglich als Unterstützung und Entlastung.

![Stabliniensystem](../../img/bfkb/stabliniensystem.jpg)

### Vorteile

* Entlastung der einzelnen Instanzen
* Entscheidungen können sorgfältiger getroffen werden
* hohes Fachwissen der Stäbe
* Teamarbeit (Ausgleich zwischen Fachwissen der Stäbe und Überblick der Linieninstanzen)

### Nachteile

* evtl. Konflikte zwischen Linie und Stab
* Entscheidungsprozesse werden unübersichtlicher
* fehlerhafte Entscheidungen können leicht dem Stab zugeschoben werden

## Matrixorganisation

![Marixorganisation](../../img/bfkb/matrixorganisation.webp)

### Vorteile

* Übersichtliche Führungsstruktur
* Keine starren Hierarchien
* Teamarbeit
* Spezialisiertes Führungspersonal
* Permanente Ansprechpartner
* Kürzere Kommunikationswege

### Nachteile

* Komplexere Entscheidungsprozesse
* Zunahme innerbetrieblicher Konkurrenzsituationen und Konflikte
* Zurechnungsprobleme von Erfolg und Misserfolg
* Möglichkeit überforderter Mitarbeiter
* Gesteigerte Nachfrage nach Fachkräften