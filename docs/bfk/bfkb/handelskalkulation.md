# Handelskalkulation

## Vorwärtskalkulation

Listeneinkaufspreis gegeben, gesucht ist der Listenverkaufspreis.
Es wird von oben nach unten gerechnet.

Bei der Vorwärtskalkulation liegt eine oligopole bzw. monopole Marktsituation vor, da der Listeneinkaufspreis gegeben ist, der Listenverkaufspreis aber nicht.

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">%</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">€</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">Listeneinkaufspreis</b></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">385,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">- Lieferantenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">40</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">154,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zieleinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">231,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">- Lieferantenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">3</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">6,93</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Bareinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">224,07</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Bezugskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">4,93</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Bezugspreis (Einstandspreis)</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">229</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Handlungskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">25</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">57,25</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Selbstkosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">286,25</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Gewinn</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">14,31</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Barverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">300,56</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Kundenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">2</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">6,13</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zielverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">306,70</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Kundenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">25</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">102,23</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zielverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: green;">408,93</td>
        </tr>
    </tbody>
</table>

Berechnung bis Barverkaufspreis: Preis * %  
Ab Barverkaufspreis: Preis / (1 - %) * %

## Rückwärtskalkulation

Listenverkaufspreis gegeben, gesucht ist der Listeneinkaufspreis.
Es wird von unten nach oben gerechnet.

Bei der Rückwärtskalkulation liegt eine polypole Marktsituation vor, da der Listenverkaufspreis durch die Marktituation gegeben ist.

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">%</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">€</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">Listeneinkaufspreis</b></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: green;">398,32</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">- Lieferantenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">10</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">39,83</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zieleinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">358,49</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">- Lieferantenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">4</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">14,34</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Bareinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">344,15</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Bezugskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">8,50</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Bezugspreis (Einstandspreis)</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">352,65</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Handlungskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">20</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">70,53</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Selbstkosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">423,18</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Gewinn</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">10</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">42,32</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Barverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">465,50</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Kundenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">2</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">9,50</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zielverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">475,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Kundenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">25,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zielverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">500,00</td>
        </tr>
    </tbody>
</table>

Berechnung bis Barverkaufspreis: Preis * %  
Ab Barverkaufspreis: Preis / (1 + %) * %

## Differenzkalkulation

Listeneinkaufspreis als auch Listenverkaufspreis ist gegeben.
Gesucht ist der Gewinn
Von Listeneinkaufspreis wird nach unten gerechnet. Von Listenverkaufspreis wird nach Gewinn hochgerechnet.

Bei der Differenzkalkulation liegt eine polypole Marktsituation vor, da der Listenverkaufspreis durch die Marktituation gegeben ist.
Allerdings ist hier von den Lieferanten auch der Listeneinkaufspreis fest, d.h. es besteht nur spielraum im Gewinn.

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">%</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">€</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">Listeneinkaufspreis</b></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">385,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">- Lieferantenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">40</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">154,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zieleinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">231,00</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">- Lieferantenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">3</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">6,93</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Bareinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">224,07</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Bezugskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">4,93</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Bezugspreis (Einstandspreis)</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">229</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Handlungskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">25</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">57,25</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Selbstkosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">286,25</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Gewinn</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: green;">2,45</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: green;">42,32</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Barverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">293,27</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Kundenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">2</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">5,99</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zielverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">299,25</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">+ Kundenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">25</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">99,75</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">= Zielverkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle; background-color: yellow;">399,00</td>
        </tr>
    </tbody>
</table>

Berechnung: Preis * %  
Gewinn = Barverkaufspreis - Selbstkosten
Gewinn in %: Gewinn / Selbstkosten

!!! warning "Achtung"

    Gewinn kann auch negativ sein!!!

**Kalkulationszuschlag** = (Listenverkaufspreis - Bezugspreis) * 100 / Bezugspreis

**Handelsspanne** = (Listenverkaufspreis - Bezugspreis) * 100 / Listenverkaufspreis

**Kalkulationsfaktor** = Listenverkaufspreis / Bezugspreis