# Ereignisgesteuerte Prozessketten

!!! note "Definition Geschäftsprozess"

    Ein Prozess ist eine Folge von Vorgängen, die der Umformung bzw. dem Transport von Material, Energie und Informationen von einem Anfangszustand in einen Endzustand dienen und nach festgelegten Regeln ablaufen. (vgl. DIN 66201)  
    ein Geschäftsprozess besteht aus einer abgeschlossenen Folge von Tätigkeiten, die zur Erfüllung einer betrieblichen Aufgabe notwendig sind.  
    ein Geschäftsprozess ist ein Bündel von Aktivitäten, für das ein oder mehrere Inputs benötigt werden und das für einen Kunden ein Ergebnis mit Wert erzielt (Hammer/Champy)

Geschäftsprozesse werden mit Hilfe von Modellen dargestellt. Geschäftsprozessmodelle helfen,

- den Ist-Zustand zu verdeutlichen und gewährleisten, dass alle Beteiligte über das Gleiche reden (Transparenz über Prozesse)  
- beim ganzheitlichen Prozessmanagement (z.B. Analyse der Schwachstellen, Suche nach Lösungen und Verbesserung der Prozesse)  
- bei der Entwicklung/Einführung geeigneter Soft- und Hardware  
- bei der Entwicklung/Einführung eines Informations- und Kommunikationssystems.

## Symbole

| Bezeichnung | Sinnbild | Bedeutung | Beispiel |
| ----------- | -------- | --------- | -------- |
| Ereignis | ![Symbol Ereignis](../../../img/bfkb/ereignis_epk.png) | - Ein Zustand, der eine Folge auslöst, ist eingetreten. | Ware ist eingegangen, Bestellung ist eingegangen, .... |
| Funktion | ![Symbol Funktion](../../../img/bfkb/funktion_epk.png) | - Verarbeitungsaktivität, die eine Umwandlung hin zum Zielzustand bewirkt | Ware wird geprüft, Bestellung wird erfasst, ... |
| | | - führt zu einem/mehreren Ereignissen | |
| Objekt | ![Symbol Objekt](../../../img/bfkb/objekt_epk.png) | - Objekt, mit dem gearbeitet wird | Ware, Bestellscheine, Kundendaten, ... |
| Organisationseinheit | ![Symbol Organisationseinheit](../../../img/bfkb/ou_epk.png) | - betroffene Abteilung, Stelle im Organigramm | Beschaffung, Vertrieb |
| UND-Operator | ![Symbol UND-Operator](../../../img/bfkb/und_epk.png) | UND-Verknüpfung | Ware wird eingelagert und Wareneingang wird verbucht |
| ODER-Operator | ![Symbol ODER-Operator](../../../img/bfkb/oder_epk.png) | ODER-Verknüpfung | Barzahlung oder Überweisung oder Baranzahlung + Restüberweisung |
| Exklusiv-Oder-Operator | ![Symbol Exklusiv-Oder-Operator](../../../img/bfkb/xor_epk.png) | Entweder-Oder-Verknüpfung | Entweder schriftliche oder telefonische Anfrage |
| Kontrollfluss | ![Symbol Kontrollfluss](../../../img/bfkb/kontrollfluss_epk.png) | - zeitlich-logische Reihenfolge | - nach Wareneingang (Ereignis) kommt die Ware zur Prüfung (Funktion) |
| Informations- und Materialfluss | ![Symbol Informations- und Materialfluss](../../../img/bfkb/informationsfluss_epk.png) | - Fluss von Informationen oder Materialien | Bestellschein und Lieferschein werden bei Prüfung verwendet |
| | | - zeigt Zu- oder Abgang von Informationen/Materialien im Prozess |
| | | - verbindet Objekte mit Prozess |
| Organisationszuordnung | ![Symbol Organisationszuordnung](../../../img/bfkb/organistaionszuordnung_epk.png) | Zuordnung von Organisationseinheiten (z.B. Abteilungen, Stellen) zu Funktionen | - Warenprüfung wird Einkaufsabteilung zugeordnet |

## Regeln

### Allgemeine Regeln EPKs

- Der zeitlich logische Prozess ist auszuweisen.  
- Eine EPK wird durch ein Ereignis begonnen und durch ein Ereignis beendet.  
- Eine EPK enthält mindestens eine Funktion.  
- Ereignisse können nicht direkt mit anderen Ereignissen verbunden werden.  
- Funktionen können nicht direkt mit anderen Funktionen verbunden werden.  
- Größere Prozessketten über zwei oder mehrere Seiten haben als Konnektoren (Verbindung) zwischen den Seiten Kreise  
- -> Das heißt Ereignisse und Funktionen wechseln sich ab.

### Regeln für Ereignisse von EPKs
	
- Ein Ereignis kann nicht Vorgänger eines anderen Ereignisses sein.  
- Ein Ereignis kann nicht Nachfolger eines anderen Ereignisses sein.  
- Ein Ereignis hat nur einen Eingangspfeil (ohne Informationsobjekte).  
- Ein Ereignis hat nur einen Ausgangspfeil.  
- Ein Ereignis folgt oder geht einer Funktion voraus.  
- Entscheidungen werden ausschließlich bei Ereignissen gefällt.  

### Regeln für Funktionen von EPKs 

- Eine Funktion kann nicht Vorgänger einer anderen Funktion sein.  
- Eine Funktion kann nicht Nachfolger einer anderen Funktion sein.  
- Eine Funktion geht mindestens einem Ereignis voraus oder folgt mindestens einem Ereignis.  
- Eine Funktion hat nur einen Eingangspfeil (ohne Informationsobjekte).  
- Eine Funktion hat nur einen Ausgangspfeil.  

### Regeln für Konnektoren von EPKs  

- ODER- bzw. XOR-Konnektoren dürfen nicht einem Ereignis folgen  
- Nur AND/UND Konnektoren dürfen einem Ereignis folgen  

### Regeln für Informationsobjekte von EPKs 

- Informationsobjekte werden ausschließlich an Funktionen geknüpft.  
- Bei der Verbindung von Funktion und Informationsobjekt beschreiben Pfeile Datenflüsse („Lesen“, „Schreiben“ bzw. die Kombination aus beiden).  

### Regeln für Organisationseinheiten von EPKs  

- Organisationseinheiten werden Funktionen zugeordnet.  
- Organisationseinheiten beschreiben Stellen, keine Mitarbeiter.  
- Organisationseinheiten basieren auf dem Organigramm der Unternehmung.  

### Formale Regeln von EPKs

- Ein Ereignis stellt einen Zustand bzw. ein Ergebnis dar. Dies sollte auch in der Formulierung zum Ausdruck kommen, z.B. Rechnung (ist) eingetroffen, Lieferung    (ist) nicht vollständig.  
- Eine Funktion beschreibt eine auszuführende Tätigkeit/Handlung. Dies sollte  in der Formulierung  zum Ausdruck kommen, z.B. Rechnung sachlich und rechnerisch prüfen, Termineinhaltung überwachen.