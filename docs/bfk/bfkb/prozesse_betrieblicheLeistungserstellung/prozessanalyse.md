# Geschäftsprozessanalyse und -optimierung

## Geschäftsprozessanalyse

### Aufgabe: 

Unternehmensindividuelle Verbesserung von Geschäftsprozessen mit Subprozessen

### Probleme im betrieblichen Leistungserstellungsprozess

Im folgenden Schaubild ist der Weg der eingekauften Waren vom Erreichen des Meldebestands bis zur Auslieferung des Produkts an den Kunden dargestellt.
In diesem Prozess durchläuft die Ware verschiedene Abteilungen:

![Leistungserstellungsprozess](../../../img/bfkb/problemeAblauforganisation.png)

**Probleme, die sich an den Schnittstellen ergeben können:**

- mangelhaften Überblick vieler Mitarbeiter über alle Tätigkeiten,  
- Informationsverluste und –verfälschungen,  
- unproduktive Wartezeiten (unterbrochene Durchlaufzeiten),  
- Schwierigkeiten bei der Lösung von Konflikten, Engpässen usw.  
- fehlende Kundenorientierung

### Ziele

- Optimierung der Zielerreichungen (-> vor allem Gewinnmaximierung / Kostenminimierung)  
- Erhöhung der Kundenzufriedenheit und Qualität  
- Verbesserung der Kernkompetenzen

### Arten

#### Schwachstellenanalyse  

##### Gegenstand:  

Untersuchung der Prozesse hinsichtlich  

- organisatorischer Brüche  
- zeitgleicher Ausführung von Vorgängen  
- unnötiger Ereignisse und Vorgänge  
- informationstechnischer Brüche  

##### Ziele:  

- Auffinden von Schwachstellen  

#### Workflowanalyse  

##### Gegenstand:  

- Dient der Automatisierung von betrieblichen Tätigkeiten auf der Basis von strukturierbaren Routineprozessen durch die Einführung von IT-Systemen  

##### Ziele:  

- Optimierung von Informationsfluss und Durchlaufzeiten  

#### Referenzanalyse  
	
##### Gegenstand:  

- Geschäftsprozesse werden mit Mustergeschäftsprozessen (Referenzprozessen) verglichen und in diese überführt.  

##### Ziele:  

- Optimale Unterstützung eines eingeführten IT-Systems (z.B. SAP)  

#### Benchmarkanalyse  

##### Gegenstand:  

- Systematischer Vergleich unternehmenseigener Prozesse, Leistungen und Produkten mit denen von Spitzenunternehmen, die diese Problemlösungen oder Verfahren am besten beherrschen.  

##### Ziele:  

- Feststellung von übermäßigen Ausschussmengen und überdurchschnittlichen Lagerkosten 

## Geschäftsprozessoptimiertung

### Eliminieren

- Sind alle Vorgänge notwendig, bzw. wertschöpfend? -> Weglassen von unnötigen Vorgängen  
  ![Eliminieren](../../../img/bfkb/eliminieren_gp.png)  
- **Beispiele:**  
  - Doppelarbeiten  
  - Mehrfacherfassungen  

### Outsourcing  

- Können einzelne Unternehmensaufgaben auf externe Dienstleister ausgelagert werden?  
  ![Outsourcing](../../../img/bfkb/outsourcing_gp.png)  
- **Beispiele:**  
  - Outsourcing der Buchhaltung auf externe Dritte  
  - IT-Outsourcing: Outsourcing der Datenverarbeitung  

### Kombination  

- Können Vorgänge zusammengefasst werden?  
  ![Kombination](../../../img/bfkb/kombination_gp.png)  
- **Beispiele:**  
  - Der Disponent übernimmt auch die Aufgaben der Beschaffung.  

### Parallelisierung  

- Können die Vorgänge bei gleicher Qualität beschleunigt werden? -> Parallelisierung von Vorgängen  
  ![Parallelisierung](../../../img/bfkb/parallelisierung_gp.png)  
- **Beispiele:**  
  - Unterschiedliche voneinander nicht abhängige Qualitätsprüfungen (z.B. Motor und Lenkung) gleichzeitig durchführen  

### Austausch  

- Können die Vorgänge qualitativ verbessert werden? -> Ändern der logischen Reihenfolge bzw. Hinzufügen  
  ![Austausch](../../../img/bfkb/austausch_gp.png)  
- **Beispiele:**  
  - Bei Auftragseingang zuerst die Bonität des Kunden, dann die Verfügbarkeit von Waren prüfen.  

### Beschleunigen  

- Wie können die Durchlaufzeiten verringert werden?  
- **Beispiele:**  
  - durch Einsatz neuer Technologien oder Parallelisierung  

### Unterstützung durch IT Systeme  

- Werden die Informationsobjekte häufig gewechselt? Werden alle Vorgänge durch IT-Systeme unterstützt? -> Einrichtung von Informationssystemen  
  ![Unterstützung durch IT-Systeme](../../../img/bfkb/itSysteme_gp.png)  
- **Beispiele:**  
  - In der Rechnungskontrolle werden genau die Daten bereitgestellt, die zur Prüfung relevant sind (Datenbank-Anwendungen).  

### Effizienz?  

- Sind alle Vorgänge effizient? -> Vermeidung von Rücksprüngen  
  ![Effizienz](../../../img/bfkb/effizienz_gp.png)  
- **Beispiele:**  
  - Feststellung nach Neuprodukteinführung: Produkt ist zu kostenintensiv und somit nicht wettbewerbsfähig