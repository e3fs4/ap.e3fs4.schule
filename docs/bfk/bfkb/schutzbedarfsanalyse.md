# Schutzbedarfsanalyse

## Schutzziele

* Vertraulichkeit = Daten sind für unberechtigte Dritte nicht zugänglich
* Integrität = Daten können nicht verfälscht werden
* Verfügbarkeit = Daten stehen zur Verfügung, wenn sie gebraucht werden

### Erweiterte Schutzziele

* Verbindlichkeit = Akteur kann seine Handlung nicht abstreiten 
* Zurechenbarkeit = Handlung kann Akteur sicher zugeordnet werden 
* Authentizität = Ist die Information echt?

## Phasen des Sicherheitsprozesses

1. Initiierung des Sicherheitsprozesses
2. Erstellung der Leitlinie zur Informationssicherheit
3. Organisation des Sicherheitsprozesses
4. Erstellung einer Sicherheitskonzeption
5. Umsetzung der Sicherheitskonzeption
6. Aufrechterhaltung & Verbessung (Wieder zurück zu 4.)

## BDSG

= Bundes Datenschutz Gesetz

* gültig in Deutschland
* regelt die Verarbeitung von personenbezogenen Daten
* dient zum Schutz der Privatsphäre und der Kontrolle über seine Daten

## DSGVO

= Datenschutz Grundverordnung

* EU-weit gültig (seit 05/2018)
* regelt Schutz personenbezogener Daten in EU-Mitgliedsstaaten, inkl. Strafen bei Verstoß
* Rechte:
    * Recht auf Auskunft
    * Recht auf Berichtigung
    * Recht auf Löschung

