# Wertschöpfungskette

Die Wertschöpfungskette ist eine, von Michael E. Porter im Jahr 1985 konzipierte, Darstellung einer geordneten Reihung von Aktivitäten in der Leistungserstellung von Produkten und Dienstleistungen.
Sie stellt die Leistungserbringung von der Entwicklung, Beschaffung, Produktion, Vertrieb bis zum „after sales service“ dar - der Weg vom Ausgangsmaterial bis zur Verwendung.

Es wird zwischen den Unterstützungs- und den Primäraktivitäten unterschieden.
Die Primäraktivitäten sind die Aktivitäten, die einen direkten wertschöpfenden Beitrag liefern, während die Unterstützungsaktivitäten die notwendigen Voraussetzungen schaffen und somit indirekt der Wertschöpfung dienen.

## Prozessorientierte Organisation

> „Ein Prozess ist eine Folge von Vorgängen, die der Umformung bzw. dem Transport von Material, Energie und Informationen von einem Anfangszustand in einen Endzustand dienen und nach festgelegten Regeln ablaufen“. (vgl. DIN 66201)

Ein Geschäftsprozess ist eine zeitliche und sachlogische Abfolge von Unternehmensaktivitäten, die festgelegte Unternehmensziele verfolgen und zur Bearbeitung auf Unternehmensressourcen (Organisationseinheiten, Betriebsmittel, Daten) zurückgreifen.

Kernprozesse (Prozesse, die der Wertschöpfung eines Unternehmens dienen):  
Beispiele in einem Industriebetrieb:

* Innovationsprozess (Forschung und Entwicklung)
* Beschaffungsprozess
* Fertigungsprozess
* Absatzprozess

Supportprozess (Unterstützung der Kernprozesse):  
Beispiele:

* Personalwesen
* Buchhaltung
* Lagerhaltung

In der klassischen Ablauforganisation bauen die Abläufe auf die vorhandene Aufbauorganisation auf. 
Diese Organisationsform stößt aufgrund veränderter Rahmenbedingungen an Ihre Grenzen.
Zunehmende Produktvielfalt, verschärfter Verdrängungswettbewerb, verkürzte Innovationszyklen, technologische Entwicklung und vor allem die steigende Kundenorientierung führen dazu, dass Abstimmungsprobleme, Koordinations- und Motivationsaufwand zunehmen.

Bei der prozessorientierten Organisation orientiert man sich an die Kundenwünsche (Kunde zu Kunde).
Ausgehend vom gewünschten Endprodukt werden die einzelnen Teilprozesse und die notwendigen Ressourcen abgeleitet.
Durch eine höhere Transparenz der Arbeitsabläufe können Kosten und Zeitaufwand gründlicher zugeordnet werden.

| Klassische Organisation | Prozessorientierte Organisation |
| ----------------------- | ------------------------------- |
| Vertikale Sichtweise (hierarchisch) | Horizontale Sichtweise |
| Fokus auf Funktionen, Abteilungen, Stellen | Kundenorientiert |
| Leistungsorientiert | In der Regel Teamarbeit |
| In der Regel Einzelarbeit und Routinetätigkeiten | Transparent |
| Intransparent | |