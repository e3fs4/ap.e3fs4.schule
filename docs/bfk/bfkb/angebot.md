# Angebot

## Qualitativer Angebotsvergleich

### Vorgehen

1. Erfassen Sie, welche Vergleichskriterien anhand der Außendienstberichte zur Beurteilung der Lieferanten zur Verfügung stehen. 
2. Gewichten Sie die Vergleichskriterien nach Ihrem Ermessen. Beachten Sie dabei: 
    a. Es müssen insgesamt 100% verteilt werden.
    b. Das Ihrer Meinung nach wichtigste Vergleichskriterium erhält die meisten Prozentpunkte, usw.
    c. Sie sollten Ihre Gewichtung der Vergleichskriterien begründen können. 
3. Bewerten Sie die Kriterien auf einer Skala von 1 bis 10. Beachten Sie dabei: 
    a. 1 bedeutet: Das Kriterium wird vom Lieferanten kaum erfüllt. 
    b. 5 bedeutet: Das Kriterium wird vom Lieferanten mittelmäßig erfüllt. 
    c. 10 bedeutet: Das Kriterium wird vom Lieferanten sehr stark erfüllt. 
4. Multiplizieren Sie die von Ihnen vergebenen Bewertungspunkte mit der jeweiligen prozentualen Gewichtung des Kriteriums. Addieren Sie anschließend die errechneten Punkte für jeden Lieferanten. 
5. Entscheidung für den Lieferanten mit der höchsten Punktzahl

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Vergleichskriterien</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Gewichtung</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Lieferant A</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Lieferant B</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Lieferant C</th>
        </tr>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Punkte</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">% x Pkt.</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Punkte</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">% x Pkt.</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Punkte</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">% x Pkt.</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Beschaffungskosten</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">150</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">75</td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Qualität der Produkte</td>
            <td style="border:1px solid #b3adad; padding:5px;">20</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">200</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">100</td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">20</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Preis</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">150</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">75</td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Lieferzeit</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">150</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">75</td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Umweltstandard eingehalten?</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">100</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">50</td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Zertifizierung</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">100</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">50</td>
            <td style="border:1px solid #b3adad; padding:5px;">1</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Service</td>
            <td style="border:1px solid #b3adad; padding:5px;">15</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">150</td>
            <td style="border:1px solid #b3adad; padding:5px;">10</td>
            <td style="border:1px solid #b3adad; padding:5px;">150</td>
            <td style="border:1px solid #b3adad; padding:5px;">5</td>
            <td style="border:1px solid #b3adad; padding:5px;">75</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;"><b>Summe</b></td>
            <td style="border:1px solid #b3adad; padding:5px;"><b>100%</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">70</td>
            <td style="border:1px solid #b3adad; padding:5px;">1000</td>
            <td style="border:1px solid #b3adad; padding:5px;">40</td>
            <td style="border:1px solid #b3adad; padding:5px;">575</td>
            <td style="border:1px solid #b3adad; padding:5px;">11</td>
            <td style="border:1px solid #b3adad; padding:5px;">160</td>
        </tr>
    </tbody>
</table>

## Quantitativer Angebotsvergleich

### Bezugspreiskalkulation

Die Bezugspreiskalkulation ist ein Instrument für den Einkäufer, Angebote vergleichbar zu machen.
Umsatzsteuer ist für das Unternehmen kostenneutral, da sie eine Endverbrauchersteuer ist.

Vom Listeneinkaufspreis des Anbieters wird im Rahmen der Bezugspreiskalkulation zunächst ein möglicher Lieferantenrabatt abgezogen, der dem Käufer beispielsweise aufgrund langjähriger Geschäftsbeziehungen gewährt wird.
Daraus ergibt sich der sog. Zieleinkaufspreis, von dem – je nachdem, zu welchem Zeitpunkt der Käufer zahlt – noch ein möglicher Lieferantenskonto abgezogen wird.
Hieraus errechnet sich der Bareinkaufspreis, den der Käufer zu bezahlen hätte, wenn er die Ware beim Lieferanten abholen würde.
Häufig wird die Ware jedoch geliefert, sodass der Verkäufer die hierdurch entstehenden Verpackungs- und Transportkosten ggf. als Bezugskosten auf den Bareinkaufspreis aufschlägt.
Daraus ergibt sich der für den Einkäufer nun vergleichbare Bezugspreis (Einstandspreis).

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Lieferant</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Lieferant A</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Lieferant B</th>
            <th colspan="2" style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Lieferant C</th>
        </tr>
        <tr>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">%</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Betrag</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">%</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Betrag</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">%</th>
            <th style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">Betrag</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">1. Listeneinkaufspreis</b></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">130</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">135</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">146</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">2. - Lieferantenrabatt</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">0</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">0</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">10</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">13,5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">7,3</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">3. = Zieleinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">130</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">121,5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">138,7</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">4. - Lieferantenskonto</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">2</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">2,6</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">0</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">9</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">2</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">2,77</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">5. = Bareinkaufspreis</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">127,4</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">121,5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">135,93</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">6. + Bezugskosten</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">4,1</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">3,5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">0</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:2px;">7. = Bezugspreis (Einstandspreis)</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">131,5</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">125</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;">135,93</td>
            <td style="border:1px solid #b3adad; padding:2px; text-align:center; vertical-align: middle;"></td>
        </tr>
    </tbody>
</table>