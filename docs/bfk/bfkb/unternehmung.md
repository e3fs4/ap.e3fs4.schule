# Unternehmung

## Unternehmensphilosophie, -leitbild, CI

### Unternehmensphilosophie

Die Unternehmensphilosophie kann als zentrale übergeordnete Konzeption für die Führung eines Unternehmens und seine Langfrist-Ausrichtung am Markt verstanden werden (Charakter eines Unternehmens). Sie umfasst Alleinstellungsmerkmale und Werte des Unternehmens. Aus ihr werden Kultur, Leitbild und Strategie des Unternehmens abgeleitet. Die Unternehmensphilosophie ist die Grundlage für das Leitbild. Ohne gemeinsame Werte kann kein Leitbild entwickelt werden.

### Unternehmensleitbild

Ist die aktive Umsetzung der Philosophie und das Leben der Werte im Unternehmensalltag. Das Leitbild ist eine schriftliche Erklärung einer Organisation über ihr Selbstverständnis und ihre Grundprinzipien (Mission und Vision einer Organisation). Es formuliert Unternehmensziele, die zu einer positiven Unternehmenskultur beitragen sollen.
- Nach **innen** soll ein Leitbild Orientierung geben und somit handlungsleitend und motivierend wirken.
- Nach **außen** (Öffentlichkeit, Kunden) soll es deutlich machen, für was die Organisation steht.
Das Leitbild ist Basis für die CI einer Organisation.

### Corporate Identity

„**CI** bezeichnet ein komplexes strategisches Konzept, welches inhaltlich und formal alle Unternehmensaktivitäten abstimmt, zur Schaffung einer unverwechselbaren Persönlichkeit, und um die Akzeptanz der Leistungen zu steigern.“
Die CI lässt sich in folgende Punkte unterteilen:
- *Corporate Behaviour:* Das Verhalten des Unternehmens gegenüber seinen Mitarbeitern sowie das Verhalten der Mitarbeiter gegenüber den Kollegen und seiner Umwelt (Kunden). 
- *Corporate Communications:* Alle kommunikativen Äußerungen (Werbung, Public Relations).
- *Corporate Design:* Die einzigartige/einheitliche Gestaltung aller Elemente, die zum Unternehmen gehören (das innere und äußere Erscheinungsbild z.B. Farben, Schriften, Logo, Briefpapier, Innenausstattung, Arbeitsplatzgestaltung...
- *Corporate Image:* Die Gestaltung von Preis-, Produkt- und Werbestrategien.

## Ziele

!!! important "Merke"

    Das oberste Ziel jedes Privatsunternehmens lautet **Gewinnmaximierung**

## Arten von Unternehmenszielen:

* **Strategische Ziele:** (mittel- bis langfristig) z.B. Marktanteil auf 25 % erhöhen, Ressourcen um 10 % besser ausnutzen, etc.
* **Operative Ziele:** Strategische Ziele in konkrete Einzelmaßnahmen umsetzen:
  * *Kundenorientierte Ziele*, z.B. Kundenbedürfnisse zu 95% erfüllen
  * *Ablauforientierte Ziele*, z.B. Produktionskosten um 5% senken
* **Primärziele:** bestimmen den momentanen wirtschaftlichen Erfolg: Kosten optimieren, Termine einhalten, Qualität optimieren
* **Sekundärziele:** bestimmen den langfristigen Unternehmenserfolg: z. B. gutes Image, hohe Innovationskraft, Kundenorientierung, Motivation, Flexibilität

###### Wird das Unternehmensziel Gewinnmaximierung erreicht, kann dies für alle am Unternehmen beteiligten Interessengruppen von Vorteil sein:

* Mitarbeiter:
  * Gute Löhne / Gehalt
  * Zusatzleistungen (Provision, Vergünstigungen/Personalrabatte, betriebliche Altersversorgung)
  * Arbeitsplatzsicherheit
  * Gewinnbeteiligung
* Kapitalgeber:
  * Steigende Aktienkurse / Dividendenausschüttung
  * Pünktlicher Geldrückfluss
* Lieferanten:
  * (Mehr) Aufträge
  * Rechnungen werden bezahlt
* Kunden:
  * Höheres Leistungsspektrum (z.B. Restaurant bietet zusätzlich einen Lieferdienst an)
  * Versorgungssicherheit (z.B. Garantieleistungen, Ersatzteile usw.)
  * Produktneuheiten
* Staat:
  * Steuereinnahmen (z.B. Umsatzsteuer)
  * „Sponsoring“
  * Abgaben an Sozialversicherung
* Eigentümer / das Unternehmen:
  * Höhere Sicherheiten
  * Höherer Marktanteil / höhere Marktpräsenz
  * Gutes Image / Ansehen
  * Geld für Investitionen, Forschung- und Entwicklung

#### Zielformulierung:

Alle Zielformulierungen müssen enthalten:

* **Zielobjekt:** Wer soll das Ziel erreichen?
* **Zielinhalt:** Was soll erreicht werden?
* **Zielmaßstab:** In welcher Größenordnung soll das Ziel erreicht werden?
* **zeitlicher Bezug:** In welchem Zeitraum soll das Ziel erreicht werden? Bsp: In der Einkaufsabteilung sollen die Kosten um 5% gesenkt werden bis Ende 2020.

> **Merke:** Der Weg wie das Ziel erreicht werden soll, gehört nicht in eine Zielformulierung!

###### Formulierung von Unternehmenszielen

Warum ist die SMART-Formel wichtig? Ganz einfach: Weil viele Ziele vorgegeben werden, die gar keine Ziele sind. Eher verkleidete Aktivitäten, die nur nach außen als Ziele dargestellt werden.\
Beispiele gefällig?

* Verbesserte Nutzeroberfläche
* Geringere Herstellungskosten
* Umsetzung so schnell wie möglich
* Hohe Qualität
* Umzug in neues Bürogebäude

Was passiert mit solchen Zielen am Ende des Projektes? Der Projektleiter ist der Meinung, das Projekt wäre abgeschlossen – der Auftraggeber ist ganz anderer Meinung. Das muss besser gehen. Und dabei hilft die SMART-Formel!

Wie ist die SMART-Formel aufgebaut? Ganz einfach: Fünf Anfangsbuchstaben – fünf Kriterien, die ein gutes Ziel erfüllen sollte. Schau sie dir mal an:

![Smart Formel](../../img/bfkb/smart.png)

Müssen immer alle Kriterien der SMART-Formel erfüllt sein?  
Wie so häufig gibt es nicht nur Schwarz und Weiß, sondern auch hier einen Graubereich: Nicht jedes Ziel muss wirklich jedes dieser Kriterien vollständig erfüllen. Akzeptierte und realistische Ziele ergeben sich häufig wie von allein. Wer wählt schon Ziele aus, die ohnehin niemand erreichen kann? (Ich sehe praktisch, wie du dein Gesicht verziehst, weil dein Chef sehr wohl der Meinung ist, unrealistische Ziele vorgehen zu müssen. Aber das ist ein anderes Thema.) Der Knackpunkt ist immer wieder die Messbarkeit: Woran kann ich messen bzw. prüfen, dass das Ziel tatsächlich erreicht wurde? Welche konkreten Zahlenwerte, Termine, Vergleichswerte kann ich nutzen?

## Organisation

**Organisation** ist ein System von dauerhaften Regelungen, welche die Aufgabenbereiche der Aufgabenträger und die Art und Weise der Aufgabenerfüllung festlegen. Organisation meint aber auch die Tätigkeit, einem Unternehmen eine Ordnung, eine dauerhafte Struktur zu geben. Mit zunehmender Betriebsgröße vertieft sich die Kluft zwischen Planung und Ausführung: Je größer die Zahl der am betrieblichen Leistungsprozess beteiligten Personen. Desto größer ist die Gefahr, dass der Einzelne nicht weiß, was er zu tun hat. Diese Lücke muss durch Koordination geschlossen werden. Der Aufbau einer Organisation orientiert sich am unternehmerischen Oberziel - Gewinnmaximierung. Die Umsetzung des ökonomischen Prinzips im Unternehmensalltag, ist das Leitmotiv zum Aufbau einer Organisation. Sie hat zwei Aufgaben:

* Schaffung einer Ordnung arbeitsteiliger Prozesse nach Maßgabe des ökonomischen Prinzips
* Entlastung der Unternehmensleitung durch generelle Regeln zur Erledigung häufig wiederholbarer Aufgaben (Routinearbeiten)

Von solchen Routinearbeiten entlastet sich die Unternehmensleitung durch **Delegation** an nachgeordneten Stellen.

###### Vorteile:

* Entlastungsfunktion für die Unternehmensleitung
* Rationalisierungsfunktion durch Arbeitsteilung
* Kostenminimierung durch Ermöglichung einer Massenproduktion

###### Nachteile:

* Erfolgseinbußen durch bürokratische Routineentscheidungen (starr und unflexibel)
* Motivationseinbußen durch eingeschränkten Entscheidungsspielraum und mangelnde Identifikation

Wenn es gelingt, ein ausgewogenes Verhältnis zwischen generellen Regelungen und fallweiser Prüfung mit Einzelfallentscheidung (Disposition) zu schaffen, spricht man von einem **Organisationsgleichgewicht**.

### Arten der Organisation

#### 1. Aufbauorganisation

Basiert auf langfristigen Entscheidungen der Unternehmensleitung zur Regelung der Beziehungen zwischen Personen, Abteilungen und Betriebsmitteln. Fragen:

* Wie sind die Aufgaben verteilt?
* Welche Stellen sollen gebildet werden?
* Welche Mitarbeiter sollen die Stellen besetzen?
* Welchen Rang nimmt die jeweilige Stelle ein?
* Wer kann wem Weisungen erteilen?

Die Schaffung einer Aufbauorganisation beginnt mit der **Aufgabenanalyse**: Die komplexe unternehmerische Gesamtaufgabe wird in Teilaufgaben zerlegt, sogenannte Arbeitspakete z.B. einkaufen, lagern, werben, Preise berechnen, verwalten. In einem zweiten Schritt werden die Teilaufgaben zu wirtschaftlich sinnvollen Aufgabenkomplexen zusammengefasst. Diesen Vorgang bezeichnet man als **Aufgabensynthese**. So entstehen **Stellen**. Die Stelle ist die kleinste organisatorische Einheit eines Unternehmens. In den Stellen sind Teilaufgaben so zusammenzufassen, dass eine vorgegebene Leistungserstellung mit geringstmöglichem Faktoreinsatz erreicht werden kann. Eine **Instanz** ist eine Stelle mit Leitungsbefugnis. Die Anzahl der untergeordneten Stellen pro Instanz bezeichnet man als *Leitungsbreite* und die Anzahl der Hierarchieebenen als *Leitungstiefe*. Eine Instanz und die ihr untergeordneten Stellen können zu einer **Abteilung** zusammengefasst werden, z.B. zur Marketing-Abteilung. Die Abteilung kann weiter untergliedert werden in Arbeitsgruppen oder Teams. Die Zerlegung der Gesamtaufgabe kann nach verschiedenen Kriterien erfolgen. Von besonderer praktischer Bedeutung ist die Gliederung nach der Verrichtung und nach Objekten.

###### 1. Funktionale Organisation (verrichtungsorientiert)

Man spricht von einer Funktionsorganisation, wenn unmittelbar unterhalb der Geschäftsleitung Abteilungen nach den Grundfunktionen des Betriebes gebildet werden.

![Funktionale Organisation Schema](../../img/bfkb/funltionaleOrganisation.png)

###### 2. Spartenorganisation (objektorientiert)

Man spricht von einer Spartenorganisation (=divisionale Organisation), wenn unmittelbar unterhalb der Geschäftsleitung Abteilungen nach Produktgruppen (Sparten), also nach Objekten, gebildet werden. Die Spartenorganisation findet bei Unternehmen mit stark differenzierter Produktionspalette Anwendung.

![Spartenorganisation Schema](../../img/bfkb/spartenorganisation.png)

Ein **Stab** ist eine Person, Gruppe oder Abteilung, die einer Instanz zugeordnet ist, sie entlastet und ihr zur Entscheidungsvorbereitung dient. Der Vorteil von Stäben ist, dass sie gerade bei einer großen Leitungsbreite die Überlastung der Instanz verhindern. Die Gefahr bei Stäben liegt darin, dass sie die Instanz in ihren Entscheidungen beeinflussen, ohne die Verantwortung zu übernehmen. Die grafische Darstellung der Struktur des Unternehmens, der Aufbauorganisation, kann mit einem **Organigramm** erfolgen, das über die hierarchische Struktur und die Aufgabenbereiche informiert.

#### 2. Ablauforganisation

Beruht auf kurz- bis mittelfristigen Entscheidungen der mittleren und unteren Führungsebene zur zeitlichen, räumlichen und personellen Strukturierung von Arbeitsabläufen. Frage:

* Wie sollen die Aufgaben bearbeitet werden?
