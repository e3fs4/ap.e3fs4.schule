# Projektmanagement

Projektmanagement ist die zielorientierte Vorbereitung, Planung, Steuerung, Dokumentation und Überwachung von Projekten mit Hilfe spezifischer Instrumente.

## Projektphasen

![Projektphasen](../../img/bfkb/projektphasen.png)

## Arbeitspakete

Alle erforderlichen Aktivitäten eines Projektes werden in einer Arbeitspaketliste zusammengefasst.
Die Arbeitspakete zerlegen das Projekt in handhabbare Portionen und können als „Projekt im Projekt" verstanden werden.
Arbeitspakete sind die kleinste ausgewiesene Einheit der Projektplanung.
Für die Arbeitspaketentwicklung gilt folgender Regelkatalog:

* Für jedes Arbeitspaket gibt es einen Verantwortlichen.
* Jedes Arbeitspaket ist in sich abgeschlossen, in sich steuer- und kontrollier­bar, in Art und Umfang übersichtlich und abgrenzbar.
* Das Ergebnis des Arbeitspakets ist präzise beschrieben. 
* Jede Tätigkeit im Projekt kann genau einem Arbeitspaket zugeordnet werden.

## Projetkstrukturplan

Der Projektstrukturplan (PSP) als Baumdiagramm hat die Aufgabe, komplexe Projektstrukturen klar und hierarchisch geordnet darzustellen.  
Die DIN 69901 regelt die Rahmenbedingungen.
Auf einen Blick lassen sich alle Arbeitspakete des Projekts erfassen und einord­nen, was die Kommunikation im Projektteam erheblich erleichtert.
Dazu werden alle ermittelten Arbeitspakete zweckmäßigen Oberbegriffen zugeordnet, mit einer Nummerierung (Codierung) versehen und in grafischer Form dargestellt.  
Projektstrukturpläne können nach verschiedenen Kriterien (objektorientiert, funktionsorientiert, ablauforientiert) sortiert werden. 

## Projektablaufplan und Terminplan

Die Aufgabe von Ablauf- und Terminplan ist es, dem Projektverlauf eine zeit­liche Komponente hinzuzufügen.
Der Ablaufplan legt Dauer und zeitliche Staffelung der einzelnen Arbeitspakete fest und der Terminplan weist dem Pro­jektverlauf konkrete Kalendertermine zu.
Ablauf- und Terminplan werden als **Balkendiagramm** und als **Netzplan** dargestellt. Beide Varianten sind Ablauf- und Terminplan zugleich.  
Die ermittelten Arbeitspakete werden dazu um zwei Aspekte ergänzt:

1. Es werden die **logischen Abhängigkeiten** zwischen den einzelnen Ar­beitspaketen ermittelt. Hierbei wird die Frage beantwortet, welche Arbeits­pakete zwingend abgeschlossen sein müssen, bevor folgende Arbeits­pakete begonnen werden können, bzw. welche Arbeitspakete parallel bearbeitet werden können.
2. Für jedes Arbeitspaket wird eine **Dauer** festgelegt, die als Grundlage für die Terminplanung dient.

Als Ergebnis dieser Überlegungen wird eine **Vorgangsliste** erstellt. 
Sie ist verbindliche Grundlage für die Erstellung sämt­licher Projektablauf- und Terminpläne.

## Balkendiagramm (= Gantt-Diagramm)

Ein Balkendiagramm besteht aus folgenden Elementen:

* einer Vorgangsliste aller Vorgänge
* einer waagrechten Zeitachse, auf der man die jeweiligen Zeiteinheiten einträgt
* einem Balken für jeden Vorgang, der in seiner Länge der jeweiligen Dauer des Vorgangs (=Arbeitspakete) entspricht
* die Pfeile, die die Balken miteinander verbinden, zeigen die logi­schen Abhängigkeiten

Liegt zwischen zwei miteinander verbundenen Vorgängen ein freier Zeitraum, so kann dieser als Puffer genutzt werden.
Ein Puffer ist der Zeitraum, um den sich ein Vorgang verschieben bzw. verzögern darf, ohne dass der geplante Anfangstermin der nachfolgenden Vorgänge beeinflusst bzw. das Gesamtprojekt verzögert wird.

Das Balkendiagramm liefert folgende Informationen:

1. Die Dauer des Gesamtprojektes
2. Die geplanten Anfangs- und Endzeitpunkte der Vorgänge
3. die Parallel verlaufenen Vorgänge

## Netzplan

## Weiterführnde Links

### Videos

* [Projektstrukturplan erstellen: Alles was du wissen musst - Projekte leicht gemacht](https://www.youtube.com/watch?v=_K4VLopVX-U)
* [Netzplan vs. Gantt: Was brauchst du wirklich? - Projekte leicht gemacht](https://www.youtube.com/watch?v=zyuqZKc98sg)
* [Netzplan einfach erklärt: Ein Beispiel mit Vorwärts und Rückwärtsterminierung - Projekte leicht gemacht ](https://www.youtube.com/watch?v=MnE1kLW3rvs)
* [Kritischer Pfad, Gesamtpuffer und freier Puffer: Die Netzplantechnik am Beispiel erklärt - Projekte leicht gemacht](https://www.youtube.com/watch?v=kjjN7MDwgf0)