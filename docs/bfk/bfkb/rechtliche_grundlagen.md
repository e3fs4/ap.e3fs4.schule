# Rechtliche Grundlagen

Für Kaufleute gilt in erster Linie das HGB (Handelsgesetzbuch).
Kaufmann ist, wer ein Handelsgewerbe betreibt (§1 HGB).
Ein Handelsgewerbe ist jeder Gewerbebetrieb, bei dem eine kaufmännische Organisation erforderlich ist.
Kleingewerbetreibende (z.B. Kiosk, kleine Gastwirtschaften) sind keine Kaufleute im Sinne des HGB (vgl. §2 HGB) und müssen sich nicht an die Bestimmungen des HGB und andere kaufmännische Vorschriften halten.

## Formen des Kaufmanns

* Istkaufmann: Gewerbebetrieb mit kaufmännischer Organisation; muss sich ins Handelsregister eintragen lassen. Eintragung ist deklaratorisch1
* Kannkaufmann: Kleinere Gewerbebetriebe mit kaufmännischer Organisation müssen sich nicht ins Handelsregister eintragen. Erst mit der Eintragung erhält man die Kaufmannseigenschaft (= konstitutiv)
* Formkaufmann: Ausschlaggebend für Kaufmannseigenschaft ist die Rechtsform (-> AG, GmbH)

![Formen des Kaufmanns](../../img/bfkb/kaufmaenner.png)

## Das Handelsregister (HR)

Das Handelsregister wird beim Amtsgericht geführt und verzeichnet alle Kaufleute des Amtsgerichtsbezirks.
Die Eintragung ins Handelsregister ist für fast alle Unternehmen Pflicht, weil durch diesen Eintrag alle rechtserheblichen Tatsachen festgehalten werden.
Zu diesen zählen Firma, Inhaber, Stammkapital, Haftung sowie Insolvenzverfahren.
Das Handelsregister schützt den Rechtsverkehr in seinem Vertrauen auf die Richtigkeit der Angaben.
Alle Interessierten haben Zugriff auf die Daten eines jeden Handelsregistereintrag.
Eintragungen, die gelöscht werden sollen, werden rot unterstrichen.

![Handelsregister](../../img/bfkb/hr.png)

## Die Firma

Name eines Vollkaufmanns unter dem er klagen oder verklagt werden kann.
Firmen müssen sich deutlich voneinander unterscheiden, sie dürfen nicht irreführend sein und die Geschäfts- und Haftungsverhältnisse müssen ersichtlich sein.

| Firmenart | Beschreibung |
| --------- | ------------ |
| Personenfirma | Die Firma besteht aus einem oder mehreren bürgerlichen Namen und der Rechtsbezeichnung |
| Sachfirma | Die Firma besteht aus dem Firmennamen, der sich aus dem Gegenstand / der Tätigkeit des Unternehmens ableitet und der Rechtsbezeichnung |
| Gemischte Firma | Die Firma besteht aus dem / den Personennamen und dem Gegenstand / der Tätigkeit des Unternehmens und der Rechtsbezeichnung |
| Phantasiefirma | Die Firma besteht aus einem frei erfundenem Phantasienamen und der Rechtsbezeichnung |

![Eintrag im HR](../../img/bfkb/firma.png)

## Rechtformzusätze / Rechtsbezeichnung

* Einzelkaufleute: Eingetragene/r Kaufmann/-frau bzw. entsprechende Abkürzung: e.K., e.Kfm., e.Kfr.
* Personengesellschaften z.B. Offene Handelsgesellschaft (OHG), Kommanditgesellschaft (KG)
* Kapitalgesellschaften: Aktiengesellschaft (AG), Gesellschaft mit beschränkter Haftung (GmbH)