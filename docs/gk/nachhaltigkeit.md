---
tags:
- FISI
- FIAE
---

# Nachhaltigkeit

!!! note "Definition"

    Eine transparente, faire und gesunde Produktion, die gleichzeitig auch einen guten Preis für das Angebotene bietet.

## Drei Säulen der Nachhaltigkeit

![Drei Säulen der Nachhaltigkeit](../img/saeulen_nachhaltigkeit.jpg)

* **Soziales:** Fair Trade, Sozialkapital stärken, Berufliche Entfaltung fördern
* **Ökologie:** Bewusster Umgang und Schutz von Ressourcen, Ökologischen Fußabdruck optimieren, Risiken für Mensch und Umwelt reduzieren
* **Ökonomie:** Nachhaltig Wirtschaften, Effizienz steigern