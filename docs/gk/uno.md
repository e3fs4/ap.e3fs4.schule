---
tags:
- FISI
- FIAE
---

# United Nations Organization

Die Vereinten Nationen (kurz VN; englisch United Nations, kurz UN; auch Organisation der Vereinten Nationen (OVN) oder UNO (von englisch United Nations Organization)) sind ein zwischenstaatlicher Zusammenschluss von 193 Staaten und als globale internationale Organisation ein uneingeschränkt anerkanntes Völkerrechtssubjekt. 

## Ziele der UNO

### Eine Welt voller Konflikte

Eine Welt ohne Konflikte - das ist ein alter Wunsch, der wohl nie in Erfüllung gehen wird.
Näher liegt die Frage: Was ist notwendig, damit Konflikte nicht gewaltsam ausgetragen werden?
Welche Institutionen und welche Politik sind notwendig, damit Konflikte innerhalb von Staaten und zwischen Staaten gewaltfrei gelöst werden können?

### Ziele der UNO

Vor dem Hintergrund der Erfahrungen des Zweiten Weltkrieges unterzeichneten 51 Staaten am 26. Juni 1945 die "Charta der Vereinten Nationen". In dieser Erklärung verpflichteten sie sich, die Grundsätze des friedlichen Zusammenlebens der Staaten zu achten und in ihren Ländern durchzusetzen. Heute gehören 193 Staaten den Vereinten Nationen an.

### Grundsätze für eine friedliche Weltordnung

Die Arbeit der UNO basiert auf den folgenden vier Prinzipien:

* Alle Staaten sind souverän und gleichberechtigt. Kein Staat kann einem anderen seine Politik aufzwingen; alle Staaten sprechen in der internationalen Politik für sich selbst.
* Konflikte werden durch Verhandlungen gelöst. Die Regierungen treffen sich regelmäßig, um die wichtigsten Aufgaben zu besprechen und gemeinsame Maßnahmen zu beschließen. Konflikte sollen bereits im Vorfeld vermieden werden.
* Staaten verzichten auf Gewalt gegen andere Staaten. Kommt es dennoch zu Konflikten, sollen diese friedlich gelöst werden - zum Beispiel durch einen neutralen Schlichter, den die Streitparteien freiwillig anrufen, oder durch ein Gerichtsurteil.
* Sicherheit gibt es nur gemeinsam. Wenn alle friedlichen Mittel versagen, kann die internationale Staatengemeinschaft die Streitparteien unter Androhung oder gar Anwendung von Gewalt zum Frieden zwingen. Gewalt gegen einen anderen Staat darf nicht von einem Staat allein, sondern nur auf Beschluss der UNO gemeinsam ausgeübt werden.