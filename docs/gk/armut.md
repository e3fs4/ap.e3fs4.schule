---
tags:
- FISI
- FIAE
---

# Armut

[#kurzerklärt: Was heißt Armut in Deutschland? - tagesschau](https://www.youtube.com/watch?v=tZx1PUU4h1w)

## Absolute Armut

!!! note "Definition"

    Absolute Armut bedeutet, dass ein Mensch aus materiellen Gründen nicht in der Lage ist, seine Grundbedürfnisse zu befriedigen. Bis September 2022 galt der Weltbank zufolge als extrem arm, wer weniger als 1,90 Dollar pro Tag und Person zur Verfügung hatte. Seit September 2022 liegt die Grenze bei 2,15 Dollar pro Tag und Person. Dieser Betrag gilt als finanzielles Minimum, das ein Mensch zum Überleben braucht.  
    Unter dieser internationalen Armutsgrenze leben weltweit 767 Millionen Menschen. Ein Leben in extremer Armut bedeutet häufig Hunger. Viele der Kinder, Frauen und Männer sind mangel- oder unterernährt. Oft können sie sich keine Medikamente leisten, weshalb sich die Armut auf ihre Gesundheit auswirkt. Auch der Zugang zu Bildung oder Wohnraum ist zahlreichen armen Menschen auf der Welt nicht möglich.

## Relative Armut

!!! note "Definition"

    Wenn das Einkommen eines Menschen unter dem durchschnittlichen Einkommen eines Landes liegt, spricht man von relativer Armut. Die relative Armut orientiert sich also am sozialen Umfeld eines Menschen. Sie bezieht sich, anders als die absolute Armut, auf soziale Ungleichheit.

## Ursachen, Folgen und Maßnahmen

### Ursachen

* Jobverlust
* Geringes Gehalt (Rassismus / Anti-Feminismus) 
* Bildungsmangel
* Krankheit
* Schulden
* Sucht

### Folgen

* Kein regelmäßiges Einkommen
* Aufgrund eines Bildungsmangel ist die Jobsuche sehr schwer
* Ausschluss aus der Gesellschaft
* Geringere Lebenserwartung
* Minderwertigkeitskomplex
* Geringe Kaufkraft

### Maßnahmen

* [Sozialhilfe](sozialstaat.md) oder ggf. Grundeinkommen
* Kostenlose Bildung
* Anti-Diskrimminierungstraining

## Teufelskreis

Unter dem Teufelskreis der Armut oder dem Armutskreislauf verstehen wir die sich gegenseitig bedingenden und verstärkenden Ursachen und Folgen von Armut.
Armut ist ein vielschichtiges Problem, das viele Ursachen haben kann und zahlreiche Folgen.
Manchmal hat Armut nur eine einzelne Ursache – beispielsweise die Arbeitslosigkeit, oder eine abgebrochene Schulbildung – aber zahlreiche Konsequenzen.
Einige Folgen dieser Armut können als sekundäre Ursachen auftreten und die Armut verstärken, oft sogar fortdauern lassen.
Wenn durch dieses gegenseitige Bedingen und Verstärken eine solche Verstetigung der Armut eintritt, kann es sein, dass man aus dieser Armutsfalle nicht mehr herausfindet. 
Wenn das eintritt, spricht man vom Teufelskreis der Armut.
In der Regel ist es so, dass der Betroffene nicht ohne Hilfe von außen den Teufelskreis verlassen kann.

![Teufelskreis der Armut](../img/teufelskreis_armut.jpg)