---
tags:
- FISI
- FIAE
---

# NATO

## Was ist die NATO?

Die NATO (North Atlantic Treaty Organization), auch Atlantisches Bündnis oder Nordatlantikpakt genannt, ist ein Verteidigungsbündnis von 30 europäischen und nordamerikanischen Staaten, das dem gemeinsamen Schutz des eigenen Territoriums dient und darüber hinaus das Ziel weltpolitischer Sicherheit und Stabilität verfolgt.

## Aufgaben

### Die ursprünglichen Aufgaben der NATO

Als die NATO 1949 gegründet wurde, war ihr Auftrag klar: 
Sie sollte den Westen gegen einen Angriff der Sowjetunion und der mit ihr verbündeten Ostblockstaaten verteidigen. 
Diese schlossen sich 1955 zum Warschauer Pakt zusammen. Es war die Zeit des Kalten Krieges, die Gefahr eines Dritten Weltkrieges war groß.
Die militärische Planung und Bewaffnung war auf diesen Konfliktfall ausgerichtet.
Fast 3 Millionen NATO-Soldaten waren 1959 in Europa und der Türkei stationiert, denen etwa ebenso viele Soldaten des [Warschauer Paktes](https://de.wikipedia.org/wiki/Warschauer_Pakt) gegenüberstanden.  

Daran änderte sich bis Ende der 1980er Jahre nichts. 
Dann endete der Ost-West-Konflikt ohne Krieg.
Die Sowjetunion gab den Wettlauf mit dem Westen um die Vorherrschaft in der Welt auf.
Die DDR und die Bundesrepublik konnten sich wiedervereinigen.
Der Warschauer Pakt löste sich auf. Die Sowjetunion zerfiel in Einzelstaaten, deren mächtigster heute Russland ist.

### Die neue NATO

Die NATO hat sich nicht aufgelöst.
Sie besteht als **Beistandspakt** über das Ende des ursprünglichen Kriegsgegners hinaus bis heute fort. 
Sie wurde sogar erweitert:
Einige der ehemaligen Staaten des Warschauer Paktes wurden zwischenzeitlich als Mitglieder aufgenommen ([]).
Es war aber auch klar, dass sich die politischen Ziele der NATO, ihre militärische Struktur und ihre Bewaffnung ändern mussten.  

Seither konzentriert sich die NATO auf **Einsätze in Spannungsgebieten und Krisenregionen**.
Dabei sind **Einsätze** auch **außerhalb des Bündnisgebietes** möglich, wenn die NATO von der [UNO](./uno.md) oder der [OSZE](https://de.wikipedia.org/wiki/Organisation_f%C3%BCr_Sicherheit_und_Zusammenarbeit_in_Europa) den Auftrag erhält.
Anders als in [Artikel 5](https://www.nato.int/cps/en/natohq/official_texts_17120.htm?selectedLocale=de) des NATO-Vertrages vorgesehen, muss zuvor kein NATO-Staat angegriffen worden sein.
Der NATO-Einsatz im Kosovo 1998/1999 auf dem Gebiet des ehemaligen Jugoslawien war der erste derartige Einsatz.  

Nach den Terroranschlägen in den USA im September 2001 rief die NATO erstmals seit ihrem Bestehen den Bündnisfall aus und beteiligte sich politisch und militärisch am Kampf gegen den internationalen Terrorismus.
In Afghanistan führt die NATO seit 2003 die dortige UN-Friedenstruppe ISAF.  

Für diese neuen Aufgaben muss die NATO entsprechend gerüstet sein. Es geht nicht mehr in erster Linie darum, einen Gegner mit Nuklearwaffen von einem großen Atomkrieg abzuschrecken und eine große Zahl von Bodentruppen in Europa in Marsch setzen zu können.
Vielmehr geht es um den Aufbau von militärischen Spezialkräften - z.B. die **Krisenreaktionskräfte** der NATO und die **Schnelle Eingreiftruppe** der Europäischen Union.

### Erweiterte Aufgaben

Sicherheit wird heute nicht nur durch die Bedrohung mit Waffen bedroht.
Moderne Informations- und Kommunikationstechnologien sind zur Grundlage der gesamten Wirtschaft geworden.
Innere und äußere Sicherheit hängen vom Funktionieren dieser Systeme ab.
Cyberwar ist längst nicht mehr nur ein Thema für Fantasy-Romane.
Auf dem **NATO-Gipfel in Lissabon im November 2010** haben die Staaten beschlossen, einen umfassenden Schutz gegen **Cyber-Angriffe und -Kriminalität** aufzubauen.
Außerdem soll in enger Abstimmung mit Russland eine Raketenabwehr in Europa aufgebaut werden, die sich vor allem gegen eine mögliche nukleare Bedrohung etwa durch den Iran richten soll.

## Weiterführende Links

* [Wikipedia](https://de.wikipedia.org/wiki/NATO)
* [What is NATO?](https://www.nato.int/nato-welcome/index.html)
* [Die Nato - bpd](https://www.bpb.de/themen/militaer/deutsche-verteidigungspolitik/293297/die-nato/)

### Videos

* [NATO einfach erklärt - explainity ® Erklärvideos](https://www.youtube.com/watch?v=LKuqQKPOduE)
* [Die NATO: Russlands ewiger Gegner? - MrWissen2go](https://www.youtube.com/watch?v=xrGqXrUdqlw)

## Aktuelles

* [Die NATO zurück auf der Weltbühne: Wie Putins Krieg die Allianz verändert - ZDF](https://www.youtube.com/watch?v=8hjTrKH_U0w)
