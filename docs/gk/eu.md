---
tags:
- FISI
- FIAE
---

# Europäische Union

![Flagge der EU](../img/Flag_of_Europe.svg.webp)

## Entstehung

1951 gründen Belgien, (West-)Deutschland, Frankreich, Italien, Luxemburg und die Niederlande die Montanunion und vereinbaren damit eine Zusammenarbeit im Bereich der Schwerindustrie. 
Hohe Wachstumsraten und die damit verbundene Schaffung von Arbeitsplätzen machen die Montanunion schnell zu einem Erfolg. 
So liegt es nahe, die Prinzipien des Gemeinsamen Marktes vom Montanbereich auf die gesamte Wirtschaft zu übertragen.

Das Ergebnis: 1957 unterzeichnen die sechs Mitgliedstaaten der Montanunion in Rom die Verträge zur Gründung der Europäischen Wirtschaftsgemeinschaft (EWG) und der Europäischen Atomgemeinschaft (Römische Verträge).

!!! quote "§ Aus dem Vertrag zur Gründung der EWG (1957)"
    **Art. 1:** Durch diesen Vertrag gründen die Hohen Vertragsparteien untereinander eine Europäische Wirtschaftsgemeinschaft.  
    **Art. 2:** Aufgabe der Gemeinschaft ist es, durch die Errichtung eines Gemeinsamen Marktes und die schrittweise Annäherung der Wirtschaftspolitik der Mitgliedstaaten eine harmonische Entwicklung des Wirtschaftslebens innerhalb der Gemeinschaft, [...] eine beschleunigte Hebung der Lebenshaltung und engere Beziehungen zwischen den in ihr zusammengeschlossenen Staaten zu fördern.  
    **Art. 8:** Der Gemeinsame Markt wird während einer Übergangszeit von zwölf Jahren schrittweise verwirklicht.

Zunächst schafften die Mitglieder der EWG nach und nach alle Binnenzölle ab und schufen so eine Zollunion - mit unübersehbaren Folgen: Zum einen profitieren die Verbraucher von einem noch nie da gewesenen Warenangebot. 
Zum anderen können Unternehmen günstiger exportieren, was insbesondere der exportorientierten deutschen Wirtschaft zugute kommt und zur Schaffung neuer Arbeitsplätze beiträgt.
Auf dem Weg von der Zollunion zum gemeinsamen Markt kamen die Staaten der seit 1967 so genannten Europäischen Gemeinschaft (EG) zunächst nur mühsam voran. 
Erst die Einheitliche Europäische Akte (1987) gibt mit dem Ziel der Vollendung des Binnenmarktes bis 1992 neuen Schwung für die weitere Vertiefung der Zusammenarbeit der inzwischen zehn Mitglieder.

Im Einzelnen will die Einheitliche Europäische Akte folgende Ziele erreichen

* **Freier Personenverkehr** durch den Wegfall der Grenzkontrollen, Niederlassungs- und Arbeitsfreiheit für alle EU-Bürger, Harmonisierung des Einreise- und Asylrechts.
* **Freier Warenverkehr** durch Wegfall der Grenzkontrollen, Harmonisierung bzw. Anerkennung nationaler Normen und Vorschriften
* **Freier Kapitalverkehr** durch mehr Freiheit im Geld- und Kapitalverkehr
* **Freier Dienstleistungsverkehr** durch Liberalisierung der Finanzdienstleistungen von Banken und Versicherungen, Harmonisierung der Banken- und Versicherungsaufsicht, Öffnung der Verkehrs- und Telekommunikationsmärkte

Die Vollendung des Binnenmarktes schafft jedoch eine Eigendynamik. 
Ein gemeinsamer Markt braucht eine gemeinsame Währung. 
Der Wegfall der Binnengrenzen erfordert eine grenzüberschreitende polizeiliche Zusammenarbeit, um Missbrauch zu verhindern. 
Die Wiedervereinigung Deutschlands verstärkt die Dynamik: Um das Misstrauen der Nachbarn vor einem zu mächtigen Deutschland abzubauen, soll es stärker in Europa eingebunden werden. 
Ergebnis dieser Bemühungen ist der **Vertrag von Maastricht** (1993), der mit der Gründung der **Europäischen Union** die Zusammenarbeit der Mitgliedstaaten auf eine neue Stufe hebt.

![Die drei Säulen der EU](../img/saeulenEuropaeischerEinigung.png)

### Zeitstrahl

| Zeit | Ereignis |
| --------- | --------------------------------- |
| 18.4.1951 | Belgien, Deutschland, Frankreich, Italien, Luxemburg und die Niederlande unterzeichnen den EGKS-Vertrag (Montanunion) in Paris. Er tritt am 23. Juli 1952 in Kraft. |
| 25.3.1957 | In Rom werden die Verträge zur Gründung der EWG und EAG (Euratom) unterzeichnet (Römische Verträge). Sie treten am 1.1.1958 in Kraft. |
| 1.7.1968 | Mit der Abschaffung aller Binnenzölle für gewerbliche Erzeugnisse und der Einführung eines gemeinsamen Außenzolls gegenüber Drittländern wird die Zollunion verwirklicht. |
| 1.1.1973 | Dänemark, Irland und Großbritannien werden Mitglieder der EG. Das Freihandelsabkommen mit Island, Österreich, Portugal, Schweden und der Schweiz tritt in Kraft. |
| 7. - 10.6.1979 | Die erste allgemeine und unmittelbare Direktwahl zum Europäischen Parlament findet statt. |
| 1.1.1981 | Griechenland wird zehntes EG-Mitglied. |
| 1.1.1986 | Mit dem Beitritt Spaniens und Portugals umfasst die EG nun zwölf Mitglieder. |
| 1.1.1993 | Der Europäische Binnenmarkt tritt in Kraft. Damit entsteht in der EG ein Wirtschaftsraum ohne Binnengrenzen, in dem der freie Verkehr von Personen, Waren, Dienstleistungen und Kapital gewährleistet ist. |
| 1.11.1993 | Der Maastrichter Vertrag über die Europäische Union tritt in Kraft. Die Europäische Union (EU) ist somit gegründet. |
| 1.1.1995 | Finnland, Österreich und Schweden treten der EU bei. Die Europäische Union umfasst jetzt 15 Mitglieder. |
| 1.1.2002 | Der EURO wird in 12 Staaten der EU als Bargeld eingeführt. |
| 1.5.2004 | Beitritt von Estland, Lettland, Litauen, Malta, Polen, Slowakei, Slowenien, Tschechien, Ungarn und Zypern. Die EU hat nun 25 Mitgliedsstaaten. |
| 1.1.2007 | Bulgarien und Rumänien treten der EU bei. Die Union hat jetzt 27 Mitgliedsländer. Slowenien führt als 13. Land den Euro ein. |
| 1.7.2013 | Kroatien wird 28. Mitglied der EU. |
| Febr/März 2022 | Die Ukraine stellt wenige Tage nach Beginn des russischen Angriffskrieges einen Antrag zum Beitritt in die EU. Auch Moldau und Georgien reichen einen Beitrittsantrag ein. |
| 23.6.2022 | Die Ukraine sowie die Republik Moldau erhalten den Status eines Beitrittskandidaten. |

## Institutionen

### Europäischen Rat

Der Europäische Rat (abgekürzt: ER) ist das oberste Entscheidungsgremium der EU. Die Staats- und Regierungschefs aller Mitgliedstaaten und der Präsident der Kommission treffen sich vierteljährlich zu sogenannten Gipfeltreffen, um auf höchster Ebene die **Leitlinien und allgemeinen Ziele der europäischen Politik** festzulegen. Im Gegensatz zum Rat der Europäischen Union (Ministerrat) entscheidet der ER nicht über Details der europäischen Gesetzgebung.  
Mit seinen Grundsatzentscheidungen, z.B. zur Einführung des Euro, hat der ER die Europäische Union immer wieder reformiert ("Vertiefung"). Die Ergebnisse seiner Tagungen werden nach dem Tagungsort benannt, z.B. "Vertrag von Lissabon". Der ER soll Impulse für die Weiterentwicklung der EU geben, was zunehmend schwieriger wird, da er seine Beschlüsse im Konsens, also einstimmig und nicht mit Mehrheit, fasst. Der Präsident wird für zweieinhalb Jahre gewählt.

### Europäisches Parlament

Das Europäische Parlament (kurz: EP) besteht aus 705 direkt gewählten Abgeordneten (Stand: Februar 2020), die sich in erster Linie als **Vertreter der über 500 Millionen EU-Bürger** verstehen. Die Sitzverteilung im EP richtet sich nach der Bevölkerungszahl der Mitgliedsländer: Größere Staaten haben mehr Abgeordnete als kleinere (Deutschland 96, Malta 6), aber kleinere Staaten haben mehr Abgeordnete pro Einwohner. So vertritt ein Abgeordneter aus Deutschland 852000 Bürger, sein Kollege aus Malta dagegen nur 69000.  
Im Gegensatz zum Deutschen Bundestag ist das EP kein "Vollparlament", es hat z.B. kein Initiativrecht, kann also selbst keine neuen Gesetze vorschlagen. Denn die EU ist kein Bundesstaat, sondern ein Staatenbündnis, dessen Mitgliedstaaten sich in wichtigen Fragen das letzte Entscheidungsrecht vorbehalten. Dennoch hat das EP heute erheblichen Einfluss auf die Gesetzgebung und den Haushalt der Europäischen Gemeinschaft. In vielen Bereichen kann es gleichberechtigt mit dem Ministerrat über europäische Gesetze entscheiden. Auch bei der Wahl der Europäischen Kommission spielt das EP eine wichtige Rolle. Außerdem ist seine Zustimmung für die Aufnahme neuer Mitglieder zwingend erforderlich. Die Arbeitsweise des EP unterscheidet sich von der der nationalen Parlamente. Die Fraktions- oder Parteizugehörigkeit der EU-Abgeordneten spielt eine untergeordnete Rolle, da es keine klare Aufteilung in Regierungsfraktion und Opposition gibt. Mehrheiten müssen jeweils mühsam gefunden werden. Bei Themen, bei denen die Interessen der Mitgliedstaaten divergieren, spielt auch die nationale Zugehörigkeit der EU-Abgeordneten eine wichtige Rolle.

### Rat der europäischen Union

Der Rat der Europäischen Union (kurz: Rat) setzt sich aus den Fachministern der einzelnen Mitgliedstaaten zusammen und wird daher oft auch als Ministerrat bezeichnet. Dabei handelt es sich eigentlich um mehrere verschiedene Gremien, denn je nach Fachgebiet treffen sich in Brüssel zum Beispiel die Landwirtschaftsminister, die Wirtschafts- und Finanzminister oder die Umweltminister. Der Rat und das Europäische Parlament entscheiden gemeinsam über neue EU-Gesetze. Die Fachminister sind an die Weisungen ihrer Regierungen gebunden und vertreten **nationale Interessen**. Im Gesetzgebungsverfahren muss daher ein Kompromiss zwischen den gemeinschaftsorientierten "europäischen" Zielen der Kommission und den unterschiedlichen Wünschen der Mitgliedstaaten ausgehandelt werden. Für die Annahme eines Vorschlags mit "qualifizierter Mehrheit" sind im Rat zwei Mehrheiten gleichzeitig erforderlich: Als qualifiziert gilt eine Mehrheit von 55 % der Mitgliedstaaten, die zugleich 65 % der Bevölkerung repräsentieren. Die Ratspräsidentschaft wechselt halbjährlich.

### Europäischer Gerichtshof

Der Europäische Gerichtshof (abgekürzt EuGH) ist für die **Auslegung des EU-Rechts** zuständig und stellt damit sicher, dass das EU-Recht in allen EU-Mitgliedstaaten in gleicher Weise angewandt wird. Außerdem kann der EuGH in Rechtsstreitigkeiten zwischen den Regierungen der EU-Mitgliedstaaten und den Organen der EU entscheiden. Auch Privatpersonen, Unternehmen und Organisationen können den EuGH anrufen, wenn sie sich durch ein EU-Organ in ihren Rechten verletzt fühlen.

### Europäische Kommission

Die Europäische Kommission besteht aus dem Präsidenten und 27 Kommissaren, die von den Mitgliedstaaten entsandt werden, aber unabhängig und nur Europa verpflichtet sind. Das Wort Kommissar bedeutet Beauftragter (von lat. committere = anvertrauen, übertragen). Nur die Europäische Kommission hat das Recht, Gesetzesvorschläge zu machen (Initiativrecht). Sie gilt daher als "Motor der europäischen Einigung". Als Exekutivorgan sorgt die Europäische Kommission mit ihren 32.000 Mitarbeitern für die **Umsetzung der EU-Beschlüsse** (Verwaltungsfunktion). Sie gilt auch als "Hüterin der Verträge", denn sie wacht darüber, dass die EU-Gesetze in allen Mitgliedsstaaten eingehalten werden (Kontrollfunktion). Jeder Kommissar ist (ähnlich wie die Minister in den nationalen Regierungen) für einen bestimmten Politikbereich zuständig, z.B. Handel, Umwelt oder Regionalpolitik. Beschlüsse werden mit Mehrheit gefasst und von allen Kommissaren als "gemeinsamer Standpunkt" nach außen vertreten (Kollegialorgan). Der Präsident wird in der Regel für fünf Jahre gewählt.

## Beitritt EU

Die Erweiterungspolitik der Europäischen Union ist eine Investition in Frieden, Sicherheit und Stabilität in Europa. Sie bietet größere Wirtschafts- und Handelschancen zum gegenseitigen Nutzen der EU und der Länder, die der EU beitreten wollen.  
Um zu prüfen, ob die Länder, die der EU beitreten wollen, gut vorbereitet sind, werden die folgenden Schlüsselbereiche bewertet: Rechtsstaatlichkeit, Grundrechte, Stärkung der demokratischen Institutionen sowie wirtschaftliche Entwicklung und Wettbewerbsfähigkeit. Dies spiegelt die Bedeutung wider, die die EU ihren zentralen Werten und vorrangigen Zielen beimisst.  Rechtsstaatlichkeit: Die Länder müssen Probleme wie die Reform des Justizwesens und die Bekämpfung von organisierter Kriminalität und Korruption von Beginn des Beitrittsprozesses an angehen. Sie müssen konkrete und nachhaltige Ergebnisse vorweisen können.  
Wirtschaftspolitische Steuerung: EU-Mitglied zu werden bedeutet nicht nur, die Regeln und Standards der EU zu erfüllen. Es geht auch darum, das Land wirtschaftlich fit für die Mitgliedschaft zu machen, damit es die Vorteile der EU-Mitgliedschaft voll ausschöpfen und gleichzeitig zu Wachstum und Wohlstand der EU-Wirtschaft beitragen kann.  
Die demokratischen Institutionen müssen weiter gestärkt werden, beispielsweise durch eine verbesserte parlamentarische Kontrolle und durch Reformen der öffentlichen Verwaltung. Die Qualität der öffentlichen Verwaltung wirkt sich unmittelbar auf die Fähigkeit der Regierung aus, effiziente öffentliche Dienstleistungen zu erbringen, Korruption zu verhindern und zu bekämpfen sowie Wettbewerbsfähigkeit und Wachstum zu fördern. Zusammen mit einer gut funktionierenden öffentlichen Verwaltung ist die Stärkung der Rolle der Zivilgesellschaft von grundlegender Bedeutung.  
Die Grundrechte bilden den Kern der Werte der EU. Länder, die der EU beitreten wollen, müssen sicherstellen, dass sie uneingeschränkt geachtet werden, insbesondere das Recht auf freie Meinungsäußerung und die Rechte von Minderheiten, einschließlich der Roma. Schutzbedürftige Gruppen müssen vor Diskriminierung geschützt werden, auch vor Diskriminierung aufgrund der sexuellen Ausrichtung.  
Regionale Zusammenarbeit und gutnachbarliche Beziehungen sind grundlegende Elemente des Stabilisierungs- und Assoziierungsprozesses, der die westlichen Balkanstaaten auf dem Weg zur EU-Mitgliedschaft voranbringt. Sie hilft der Region bei der Bewältigung gemeinsamer Herausforderungen wie Energieknappheit, Umweltverschmutzung, Verkehrsinfrastruktur und Bekämpfung der organisierten Kriminalität.

### Beitrittsprozess

1. Das Land stellt beim Rat einen Antrag, in dem es seinen Wunsch zum Ausdruck bringt, der EU beizutreten. Die Europäische Kommission gibt eine Stellungnahme zu diesem Antrag ab.
2. Die Regierungen der Mitgliedstaaten müssen einstimmig beschließen, dem Bewerberland den Kandidatenstatus zuzuerkennen. Sobald bestimmte Bedingungen erfüllt sind, werden Beitrittsverhandlungen aufgenommen, allerdings nur, wenn alle Mitgliedstaaten zustimmen.
3. Das Land muss das EU-Recht umsetzen. Alle EU-Mitgliedstaaten müssen zustimmen, dass das Land alle Bedingungen erfüllt hat. Wenn alle Verhandlungen abgeschlossen sind, muss die Europäische Kommission eine Stellungnahme abgeben, ob das Land für die Mitgliedschaft bereit ist oder nicht.
4. Die Mitgliedstaaten müssen einstimmig beschließen, den Prozess abzuschließen und den Beitrittsvertrag zu unterzeichnen. Alle bestehenden Mitgliedstaaten und der künftige Mitgliedstaat unterzeichnen den Beitrittsvertrag. Auch das Europäische Parlament muss zustimmen. Erst wenn alle bestehenden Mitgliedstaaten dem Beitrittsvertrag formell zugestimmt haben, kann das Land Mitglied der EU werden.

## EU im Alltag

!!! quote ""
    Ein neuer Tag beginnt und das Referat zum Thema „Einfluss der EU auf den Alltag der EU-Bürger/innen“ für übermorgen ist immer noch nicht angefangen. Egal, erst einmal frühstücken.
    Am Frühstückstisch sitzt dein Vater und trinkt wie jeden Morgen seinen italienischen Kaffee; dank des EU-Binnenmarktes sind Produkte aus ganz Europa überall verfügbar. Heute kann er sich beim Frühstücken Zeit lassen, denn er muss nicht zur Arbeit: Er hat Urlaub. Die EU garantiert für alle Arbeitnehmer/innen mindestens 4 Wochen Urlaub pro Jahr.
    Gedankenverloren schaust du beim Müsli-Löffeln auf die Verpackung und liest, was da alles an Informationen auf der Packung steht. Keine Nüsse, prima, denn gegen die bist du allergisch. Wie gut, dass die EU dafür sorgt, dass die Zutaten und Nährwerte eines Produktes gekennzeichnet werden müssen. So kannst du - wie auch andere Allergiker/innen - sicher sein, dass keine allergenen Inhaltsstoffe enthalten sind und du das Nahrungsmittel gefahrlos verzehren kannst.
    Da klingelt dein Handy. Dein Bruder ruft dich an. Er ist gerade in einem Erasmus-Programm in Spanien und studiert dort für ein Jahr. Prima, dass das Telefonieren mit dem Handy von dort nicht mehr so teuer ist, nachdem die Roaming-Gebühren 2017 von der EU abgeschafft wurden.
    Deinem Bruder gefällt es in Spanien sehr gut, aber er hatte letzte Woche beim Skaten einen Unfall und hat sich verletzt. Zum Glück konnte er die europaweite Notrufnummer 112 anrufen und hatte auch seine europäische Krankenversicherungskarte dabei!
    Da dein Vater ja gerade Urlaub hat, plant er einen Flug nach Spanien zu buchen, um deinen Bruder dort zu besuchen. Dank des Schengen-Abkommens wird er bei dieser Reise innerhalb der EU nicht stundenlang bei Grenzkontrollen warten müssen und kann frei im Ausland reisen. Falls er ein Problem mit dem Flug haben sollte (Verspätung, Überbuchung, usw.), verfügt er außerdem über EU-Passagierrechte, die seine Reise versichern. Ob das mit dem Flug klappt, bekommst du aber nicht mehr mit, denn du musst erst einmal zur Schule.
    Nach der Schule bist du noch mit ein paar Freunden fürs Kino verabredet. Ihr fahrt mit dem Fahrrad an einem Windpark und an der Uni vorbei. Nachhaltige Energiequellen und wissenschaftliche Forschungsprojekte sind Bereiche, die von der EU unterstützt werden. Im Kino schaut ihr euch den neuen belgischen Film an, der ein Geheimtipp sein soll. Im Abspann liest du, dass der Film vom Programm „Kreatives Europa“ unterstützt wurde. Ein EU-Programm, welches die grenzüberschreitende Verbreitung der Werke aufstrebender Künstlertalente unterstützt.
    Als du abends wieder nach Hause kommst, schaltest du deinen Computer an, um endlich dein Referat vorzubereiten. Aber vorher willst du noch schnell ein wenig im Internet surfen und das neue Computerspiel ausprobieren. Auf einer Internetseite wirst du nach persönlichen Daten gefragt. Mit der neuen Datenschutzverordnung der EU muss die Website dich darüber informieren, wie diese Daten benutzt werden. Du verfügst auch über ein europäisches „Recht auf Vergessenwerden“, wenn du diese Daten löschen lassen möchtest.
    Oh, schon so spät? Das Referat wartet immer noch und du hast keine Idee, was du zur EU und ihrem Einfluss auf den Alltag der EU-Bürger/innen sagen sollst. Oder vielleicht doch?

## Aufgabenbereiche der EU

* Förderung einer hochwertigen allgemeinen und beruflichen Bildung und des sozialen Zusammenhalts
* Gemeinsame Außen- und Sicherheitspolitik
* Ein Binnenmarkt ohne Grenzen
* Investitionen in eine nachhaltige Energiezukunft
* Armutsbekämpfung und nachhaltige Entwicklung
* Lebensmittelsicherheit
* Gemeinsame Forschung und Innovation
* Förderung der öffentlichen Gesundheit
* Förderung und Schutz der Menschenrechte

Weitere Informationen zu den einzelnen Bereichen findest du hier:

* [https://european-union.europa.eu/priorities-and-actions/actions-topic_de](https://european-union.europa.eu/priorities-and-actions/actions-topic_de)
* [https://europa.eu/youreurope/citizens/index_de.htm](https://europa.eu/youreurope/citizens/index_de.htm)



## Weiterführende Links

* [Wikipedia](https://de.wikipedia.org/wiki/Europ%C3%A4ische_Union)
* [Lernecke der EU](https://learning-corner.learning.europa.eu/index_de)

### Videos

* [Die Europäische Union einfach erklärt - explainity ® Erklärvideos](https://www.youtube.com/watch?v=pQHUOTCRv3E&t=3s)
* [Die Geschichte der Europäischen Union - MrWissen2go](https://www.youtube.com/watch?v=x-HUsTQMQeo)
* [Institutionen der EU - simpleclub](https://www.youtube.com/watch?v=PTSiMOCAqfU&feature=emb_imp_woyt)
* [Ist es Zeit die EU aufzulösen? - Kurzgesagt](https://www.youtube.com/watch?v=8o2yaqECGc0)

## Aktuell
