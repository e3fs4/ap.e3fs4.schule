---
tags:
- FISI
- FIAE
---

# Arbeitszeitmodelle

| Zeitmodell | Vorteile | Nachteile |
| ---------- | -------- | --------- |
| Homeoffice | Flexible Arbeitszeit | Eventuelle Störungen durch Familienangehörigen |
| | Arbeiten von zu Hause | Mitarbeiter sehen sich nicht mehr |
| Gleitzeit | Flexibles Zeitmodell | |
| | Kern- und Rahmenarbeitszeit | |
| Schichtarbeit | Feste geregelte Arbeitszeit | Keine Flexibilität | 