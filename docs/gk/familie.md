---
tags:
- FISI
- FIAE
---

# Familie

!!! note "Definition"

    Die Familie umfasst im Mikro­zensus alle Eltern-Kind-Gemeinschaften, das heißt Ehepaare, nichteheliche (gemischt­geschlechtliche) und gleich­geschlechtliche Lebens­gemeinschaften sowie Allein­erziehende mit Kindern im Haushalt. Einbezogen sind – neben leiblichen Kindern – auch Stief-, Pflege- und Adoptiv­kinder ohne Alters­begrenzung. Damit besteht eine Familie immer aus zwei Generationen: Eltern/-teile und im Haushalt lebende Kinder.

## Familienformen

* Kernfamilie = besteht aus Frau und Mann, welche verheiratet sind und ein beziehungsweise mehrere Kinder haben.
* Ein-Eltern Familie = Alleinerziehnde, d.h. ein Elternteil kümmert sich um die Kinder.
* Pflegefamilie = Pflegeeltern nehmen Kind (Pflegekind) einer anderen Familie zeitweise.
* Regenbogenfamilie = gleichgeschlechtliche Elternteile leben mit Kindern zusammen.
* Großfamilie = besteht aus mindestens drei Generationen.
* Patchworkfamilie = Kinder leben mit einem leiblichen Elternteil und dem neuen Partner der Mutter oder des Vaters zusammen.
* Adoptivfamilie = Pflegeeltern nehmen Kind (Pflegekind) einer anderen Familie auf Dauer auf.
* Singles = Leben alleine.
* kinderlose Paare = besteht aus Frau und Mann

## Familienpolitische Maßnahmen des Sozialstaats

|   | Wer bekommt Hilfe? | Wie lange bekommt jemand Hilfe | Wie hoch ist die Hilfe |
| - | ---------------- | --------------- | ------------ |
| **Elterngeld** | Alleinerziehende Eltern, welche in Deutschland mit einem Kind in ihrer Obhut leben und entweder gar nicht, oder weniger als 30 Stunden die Woche arbeiten | Basiselterngeld: Max. 12 Monate, Partner bekommen jedoch 14; ElterngeldPlus: Maximal 24 Monate | Mindestens 300€ und maximal 1.800€. Mindestens jedoch 65% des Netto-Einkommens des eigenen Jobs. |
| **Kindergeld** | Eltern mit Wohnort in Deutschland oder der EU, welche ein Kind in ihrer Obhut haben welches unter 18 Jahren ist. | Bis das Kind 18 Jahre alt ist oder nicht mehr in der eigenen Obhut. | 1. Kind: 219€;  2. Kind: 219€;   3. Kind: 225€;  4. Kind+: 250€ | 

* **Ehegattensplitting**  
  Sobald zwei Menschen heiraten können sie das Ehegattensplitting nutzen, welches die Steuererklärung verändert und die Menschen Steuern sparen lässt.
  Dabei wird zuerst das Jahreseinkommen zusammengezählt, jedoch werden sie als eine Person gelten und nicht als Zwei.
  Anschließend wird der Betrag halbiert und die Steuern für diese eine Hälfte berechnet, nur um den Endbetrag wieder zu verdoppeln und so die Steuern zu berechnen.  
  Sobald ein Mensch mehr verdient als der andere, lohnt sich das Splitting besonders, da dadurch eine Menge Steuern gespart werden. Es lohnt sich also das in Betracht zu ziehen.
* **Elternzeit**  
  Alleinstehende Eltern können eine bis zu drei Jahre dauernde Auszeit vom Berufsleben veranlassen, in welcher sie nicht bezahlt werden, aber zuhause sind, um ihr Kind zu betreuen.
  Dazu können die Elternteile auch Elterngeld beantragen, um sich finanziell etwas abzusichern.  
  Das Elternteil kann während der Elternzeit jedoch auch in Teilzeit arbeiten und somit auch noch etwas Geld verdienen.
