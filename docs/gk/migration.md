---
tags:
- FISI
- FIAE
---

# Migration

!!! note "Definition"

    Migration bezeichnet im Allgemeinen die längerfristige Verlegung des Lebensmittelpunkts über eine größere Entfernung und administrative Grenze hinweg: etwa vom Dorf in die Stadt, zwischen Landesteilen oder über Staatsgrenzen hinweg. Damit unterscheidet sich diese Form menschlicher Mobilität von anderen, etwa dem täglichen Pendeln zur Arbeit oder touristischem Reisen, denn eine Verlegung des Lebensmittelpunktes findet bei diesen Mobilitätsformen nicht statt. Darüber, wie groß die Entfernung und wie lange der Zeitraum sein muss, um menschliche Bewegung als Migration bezeichnen zu können, gibt es keine allgemein anerkannte Definition. Einen Richtwert bietet die Definition der Vereinten Nationen, die Migration als Wohnsitznahme in einem anderen Land mit einer Dauer von mehr als drei Monaten (Kurzzeitmigration bzw. temporäre Migration) oder mehr als einem Jahr (Langzeit- bzw. dauerhafte Migration) fasst.  
    Damit ist eine weitere Bedingung angesprochen, die häufig mit Migration verbunden wird: Das Passieren einer Staatsgrenze. Neben dieser Internationalen Migration gibt es aber auch die sogenannte Binnenmigration, bei der der Lebensmittelpunkt über eine Grenze innerhalb eines Landes (z.B. Kreis- bzw. Bundesländergrenze) verlegt wird. Wanderungen innerhalb der Europäischen Union können als Zwischenform beider Migrationsarten gelten, da sie durch die Europäische Freizügigkeit so gut wie keinen rechtlichen Beschränkungen unterliegen.  
    Nicht unbedeutend für die Einordnung menschlicher Mobilität als Migration ist schließlich die angenommene oder tatsächliche Motivation, den Aufenthaltsort für längere Zeit zu wechseln. Dies kann zum Beispiel die Suche nach einem besseren Auskommen (Arbeitsmigration), die Gründung einer Familie (Familienmigration), das Streben nach (Aus-)Bildung (Bildungsmigration), die Flucht vor angedrohter oder befürchteter Verfolgung und Gewalt (Flucht- bzw. Gewaltmigration) oder auch die Suche nach Abenteuer oder einem ruhigeren Leben Lifestyle Migration umfassen.

## Unterschied Migrant - Flüchtling

!!! note "Migrant"

    Ein Migrant ist eine Person welche sich dazu freiwillig entschieden hat in das andere Land zu ziehen.

!!! note "Flüchtling"

    Ein Flüchtling ist eine Person, welche dazu gedrängt wurde aus dem eigenen Land zu flüchten in ein anderes.

!!! note "Migrationshintergrund"

    Personen, die nach 1949 nach Deutschland eingewandert sind, und deren Kinder (sowie Kinder mit einem ausländischen Elternteil) - unabhängig von der Staatsangehörigkeit

## Faktoren

### Push-Faktoren

* Krieg
* (religiöse & politische) Verfolgung
* Klima
* Armut
* Arbeitslosigkeit
* fehlende Bildungschancen
* schlechte medizinische Versorgung 
* niedriger Lebensstandard

### Pullfaktoren

* Arbeit
* bessere Wirtschaft
* Frieden
* politische Stabilität
* Sicherheit
* soziale Unterstützung
* Bildungsmöglichkeiten
* gute medizinische Versorgung
* hoher Lebensstandard