---
tags:
- FISI
- FIAE
---

# Sozialstaat

!!! note "Definition"

    Ein Sozialstaat ist ein Staat, der sich um soziale Gerechtigkeit bemüht und sich um die soziale Sicherheit seiner Bürgerinnen und Bürger kümmert. Das [Grundgesetz](gg.md) legt fest, dass die Bundesrepublik Deutschland "ein demokratischer und sozialer Bundesstaat" ist (Art. 20 GG).

## Entstehung Sozialversicherung

Durch die industrielle Revolution im 19. Jahrhunderts wurde eine neue Gesellschaftsklasse geschaffen, die Klasse der Arbeiter.
Sie waren sozial nicht abgesichert, litten bei Krankheit, Invalidität und im Alter unter großer Not.
Der Unmut in der Arbeiterschaft wurde immer größer, viele schlossen sich der Sozialdemokratischen Arbeiterpartei an, 1869 gegründet, die einen immer größeren Einfluss bekam.
1881 trug Kaiser Wilhelm I. schließlich in seiner Reichstags-Rede auf Anraten des Reichskanzlers Bismarck vor, „dass die Heilung der sozialen Schäden nicht ausschließlich im Wege der Repression sozialdemokratischer Ausschreitungen, sondern gleichmäßig auf dem der positiven Förderung des Wohles der Arbeiter zu suchen sein werde".
Bismarck wollte die Arbeiter zufrieden stellen und verhindern, dass die Arbeiterpartei noch mehr Zulauf bekam.
Durch die „Kaiserliche Botschaft“ war der Grundstein für die Gesetze zum Schutz der Arbeiter gegen Krankheit, Unfall, Invalidität und im Alter gelegt.

* 1883 verabschiedete der Reichstag das Krankenversicherungsgesetz, 1884 das Unfallversicherungsgesetz und 1889 das Invaliditäts- und Altersversicherungsgesetz. Ab 1891 wurde Invalidenrenten an Versicherte gezahlt, wenn diese als „dauernd erwerbs­un­fähig" eingestuft wurden. Altersrenten wurden mit Vollendung des 70. Lebensjahrs be­willigt, eine Hinterbliebenenrente gab es zu diesem Zeitpunkt nicht. Finanziert wurden die Renten durch gleich hohe Beiträge der Versicherten und Arbeitgeber sowie einen Reichs­zuschuss - eine Regelung, die vom Prinzip her bis heute geblieben ist.
* 1927 wurde zudem die Arbeitslosenversicherung geschaffen, die Menschen im Falle der Arbeitslosigkeit für eine gewisse Zeit absichert und 1995 die Pflegeversicherung, die bei Pflegebedürftigkeit der Versicherten einspringt.