---
tags:
- FISI
- FIAE
---

# Gemeinschaftskunde

## Lehrplan

* [Junge Menschen in Beruf, Familie und Gesellschaft: Zusammenleben gestalten](./#junge-menschen-in-beruf-familie-und-gesellschaft-zusammenleben-gestalten)
    * [Auszubildende und ihre Lebenswelt](./#auszubildende-und-ihre-lebenswelt)
    * [Strukturwandel der Gesellschaft](./#strukturwandel-der-gesellschaft) 
    * ([Medien und Mediennutzung](./#medien-und-mediennutzung)) 

* [Junge Menschen im Staat: Demokratische Prozesse mitgestalten](./#junge-menschen-im-staat-demokratische-prozesse-mitgestalten)
    * [Partizipation und politischer Entscheidungsprozess](./#partizipation-und-politischer-entscheidungsprozess)
    * [Entwicklung der Demokratie in Deutschland und ihre Gefährdungen](./#entwicklung-der-demokratie-in-deutschland-und-ihre-gefahrdungen)
    * [Grund- und Menschenrechte](./#grund-und-menschenrechte)

* [Junge Menschen in Europa und der Welt: Sich in Europa und der Welt zurechtfinden und engagieren](./#junge-menschen-in-europa-und-der-welt-sich-in-europa-und-der-welt-zurechtfinden-und-engagieren)
    * [Europa im 20. und 21. Jahrhundert](./#europa-im-20-und-21-jahrhundert) 
    * [Globalisierung](./#globalisierung)
    * ([Friedenssicherung und Entwicklungszusammenarbeit](./#friedenssicherung-und-entwicklungszusammenarbeit))

### Junge Menschen in Beruf, Familie und Gesellschaft: Zusammenleben gestalten

Die Schülerinnen und Schüler setzen sich mit dem Zusammenleben und den Veränderungsprozessen in einer modernen pluralistischen Gesellschaft auseinander.
Sie verorten sich im gesellschaftlichen System und reflektieren ihre eigene Position und ihr eigenes Verhalten.
In diesem Zusammenhang erweitern sie ihre Kommunikationsfähigkeit und achten auf einen verantwortungsbewussten Umgang mit medialen Angeboten.
Sie begreifen sich als Teil der Gesellschaft der Bundesrepublik Deutschland und reflektieren unterschiedliche Rollenbilder und Perspektiven.
Sie orientieren sich dabei an demokratischen Grundwerten und erweitern ihre Fähigkeit, Kompromisse einzugehen und als offene, selbstbewusste und tolerante Persönlichkeiten zu handeln.

#### Auszubildende und ihre Lebenswelt

Die Schülerinnen und Schüler

* beschreiben ihre jeweiligen Rollen im Ausbildungsbetrieb, in der Familie und der Gesellschaft.
* beschreiben Lebens- und Familienformen (traditionelle Familie, Patchworkfamilie, Singlehaushalt, eingetragene Lebenspartnerschaft) und setzen sich mit unterschiedlichen Rollenbildern in Familie und Gesellschaft auseinander.
* erörtern Möglichkeiten einer familienfreundlichen Politik.
* entwickeln Mittel der Konfliktbewältigung unter Berücksichtigung gesellschaftlicher Rahmenbedingungen und Interessen der unterschiedlichen Akteure.

#### Strukturwandel der Gesellschaft

Die Schülerinnen und Schüler

* beschreiben die Veränderungen der Bevölkerungsstruktur der Bundesrepublik Deutschland ([demografischer Wandel](dem_wandel.md)) und die Auswirkungen auf ihre Situation.
* setzen sich mit Ursachen und Folgen von Migration (Ein- und Auswanderung, Binnenwanderung) auseinander.
* stellen die Entwicklung der Sozialversicherungen in Deutschland dar (Gründe für die Einführung; Erweiterungen und Beschränkungen) und analysieren den Wandel auf dem Arbeitsmarkt und dessen positive und negative Auswirkungen.
* entwickeln exemplarisch einen nachhaltigen Lösungsweg für ein Problem, welches sich aus dem strukturellen Wandel ergibt.

#### Medien und Mediennutzung

Die Schülerinnen und Schüler

* beschreiben ihr eigenes Nutzungsverhalten von Print- und digitalen Medien (Kommunikation, Informationssuche, Umgang mit personenbezogenen Daten).
* erläutern die Chancen (Information, Weiterbildung) und Risiken (Abhängigkeit, Sucht), die sich aus der Nutzung von Medien ergeben.
* analysieren, welche Einflüsse und Auswirkungen Medien auf das Zusammenleben in einer pluralistischen Gesellschaft haben (Teilhabe, Manipulation), und beurteilen die Qualität verschiedener Medien.
* erörtern, wie ein kritischer und verantwortungsvoller Umgang mit Medien (Persönlichkeitsrechte, Datenschutz, Verwertung von Daten) in der digitalen Welt aussehen kann.

### Junge Menschen im Staat: Demokratische Prozesse mitgestalten

Die Schülerinnen und Schüler erkennen durch die Beschäftigung mit aktuellen politischen Themen, dass sie Teil des politischen Systems der Bundesrepublik Deutschland sind und wissen um ihre Möglichkeiten der Interessenvertretung und der Partizipation.
In diesem Zusammenhang analysieren sie das Zusammenwirken verschiedener politischer Institutionen und sollen auf diesem Weg Verständnis für die Schwierigkeiten des Aushandelns politischer Entscheidungen in einem demokratischen Staat entwickeln.
Anhand der Geschichte Deutschlands im 20. Jahrhundert erkennen sie die Gefährdungen für eine Demokratie und entwickeln Strategien, wie diesen begegnet werden kann.

#### Partizipation und politischer Entscheidungsprozess

Die Schülerinnen und Schüler

* beschreiben ein politisch zu lösendes Problem aus ihrem Erfahrungsbereich.
* erläutern Möglichkeiten politischer Einflussnahme ([Bürgerinitiativen](verbaende.md), Demonstrationen, [Wahlen](demokratie/#wahlen), [Plebiszite](demokratie.md)).
* analysieren unterschiedliche [Interessen (Verbände)](verbaende.md) im politischen Entscheidungsprozess auf Bundesebene ([Bundestag](demokratie/#bundestag) und [Bundesrat](demokratie/#bundestag)).
* beurteilen die Bedeutung wesentlicher Elemente einer Demokratie (Garantie der [Grundrechte](gg.md), Gewaltenteilung, Mehrheitsprinzip) und entwickeln Ideen, wie sie sich in der Demokratie engagieren können.

#### Entwicklung der Demokratie in Deutschland und ihre Gefährdungen

Die Schülerinnen und Schüler

* werten historische Quellen aus ihrem Umfeld aus und setzen sie in Beziehung zur eigenen Gegenwart.
* stellen die Entwicklung der Demokratie in Deutschland dar (Gründung der Bundesrepublik Deutschland, innenpolitische Reformen zur Demokratisierung der Gesellschaft, Voraussetzungen zur Überwindung der Diktatur in der DDR).
* beschreiben Formen des [Extremismus](demokratie/#extremismus) und des [Populismus](demokratie/#populismus) und setzen sich mit extremistischen und populistischen Weltanschauungen auseinander.
* entwickeln Strategien zum kritischen Umgang mit diesen.

#### Grund- und Menschenrechte

Die Schülerinnen und Schüler

* beschreiben [Grundrechte](gg.md), die ihnen besonders wichtig sind.
* erläutern die besondere Stellung der Grund- und Menschenrechte im Grundgesetz undvergleichen diese mit der [UNO](uno.md)-Menschenrechtserklärung.
* setzen sich mit Menschenrechtsverletzungen auseinander und begründen die Bedeutung des Schutzes der Grund- und Menschenrechte für eine zivilisierte Gesellschaft (Europäischer Gerichtshof für Menschenrechte).
* entwickeln Handlungsmöglichkeiten, wie man mit Grundrechtskonflikten umgehen kann.

### Junge Menschen in Europa und der Welt: Sich in Europa und der Welt zurechtfinden und engagieren

Die Schülerinnen und Schüler lernen durch die Beschäftigung mit der Geschichte der europäischen Einigung die Chancen in einem zusammenwachsenden Europa kennen und setzen sich mit aktuellen Problemen und Herausforderungen der europäischen Staatengemeinschaft auseinander.
Der Blick auf globale Zusammenhänge ermöglicht ihnen, ihr Verhalten mit der jeweiligen Situation in anderen Ländern und Kontinenten in Beziehung zu setzen.
Sie erörtern die Konsequenzen des Ressourcenverbrauchs und die Forderung nach einer nachhaltigen Form des Produzierens und Konsumierens.
Des Weiteren befassen sie sich mit Gefährdungen des Friedens und entwickeln persönliche Handlungsoptionen für eine friedliche und gerechte Welt.

#### Europa im 20. und 21. Jahrhundert

Die Schülerinnen und Schüler

* beschreiben die [Auswirkungen der europäischen Einigung auf ihre Lebens- und Arbeitswelt](eu/#eu-im-alltag).
* setzen sich mit Motiven und wesentlichen Etappen des [europäischen Einigungsprozesses](eu/#zeitstrahl) (Römische Verträge, Vertrag von Maastricht, Schengener Abkommen) auseinander.
* erläutern, wie verschiedene [europäische Institutionen](eu/#institutionen) (EU-Parlament, Europäischer Rat, EU-Kommission, Rat der Europäischen Union) gemeinsam europäisches Recht gestalten.
* beurteilen die Bedeutung aktueller Entwicklungen für den europäischen Gedanken.

#### Globalisierung

Die Schülerinnen und Schüler

* beschreiben die Einflüsse der [Globalisierung](globalisierung.md) auf ihr persönliches Leben.
* definieren den Begriff der Globalisierung und beschreiben deren Auswirkungen in den Bereichen Politik, Wirtschaft, Kommunikation, Kultur, Umwelt und Ressourcen.
* analysieren die Zusammenhänge zwischen Globalisierung und dem Lebensstandard in Industrie-, Schwellen- und Entwicklungsländern, insbesondere unter dem Aspekt der Nachhaltigkeit.
* beurteilen Chancen und Risiken der Globalisierung.

#### Friedenssicherung und Entwicklungszusammenarbeit

Die Schülerinnen und Schüler

* beschreiben einen aktuellen internationalen Konflikt.
* analysieren diesen Konflikt unter Berücksichtigung politischer, wirtschaftlicher, religiöser oder ethnischer Ursachen.
* erörtern die Bedeutung der Entwicklungszusammenarbeit (Entwicklungspolitik, NGOs) und internationaler Institutionen ([UNO](uno.md), [NATO](nato.md)) für die Konfliktlösung und Friedenssicherung.
* überprüfen persönliche Handlungsmöglichkeiten, wie sie zu einem gerechten und friedlichen Miteinander in der Welt beitragen können.

### Lehrplan Download

Hier der direkte Link zum Download des Lehrplans als PDF:
[Lehrplan](gk_lehrplan.pdf)