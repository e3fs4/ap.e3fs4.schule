---
tags:
- FISI
- FIAE
---

# Globalisierung

## Was ist Globalisierung


!!! note "Definition Globalisierung"
    Globalisierung ist die internationale Vernetzung und Verflechtung von Kultur, Politik und Wirtschaft.

### Entstehung von Globalisierung

Die Globalisierung wurde durch die Evolution bzw. Revolution der Kommunikation und der Informationstechnologien ermöglicht. Schnelle und stabile Kommunikation rund um den Globus ermöglichte teilweise Handel in Echtzeit. Darüber hinaus war die Globalisierung auch ein Instrument der "Friedenssicherung" (Erwerb von Gütern/Ressourcen durch Handel statt durch Eroberung). Auch politisch wurde die Globalisierung zugunsten globaler Märkte gefördert.

## Auswirkungen der Globalisierung

### Aus Sicht von Unternehmern

!!! quote ""
    Technischer Fortschritt in den Bereichen Information und Kommunikation, Transport und Verkehr, Kapitalmarktinnovationen sowie die zunehmende Liberalisierung des Welthandels haben zu einer zunehmenden weltweiten Vernetzung der Märkte und Gesellschaften geführt. Was aber bedeutet diese Entwicklung konkret für ein Unternehmen?
    Mit der Verflechtung der Märkte besteht die Konkurrenz nun nicht mehr nur aus den Betrieben und Geschäften in der näheren Umgebung, sondern sie umfasst die ganze Welt. Der Wettbewerbsdruck ist demnach enorm. Um sich gegen die immense Konkurrenz durchsetzen zu können, müssen vor allem die Kosten für das Unternehmen so niedrig wie möglich gehalten werden. Viele Unternehmen sehen die Lösung dabei in Rationalisierung, wobei „teure“ Arbeit durch Maschinen ersetzt wird. Eine andere Vorgehensweise, die durch die Globalisierung möglich wurde, ist es, im Ausland zu produzieren. Durch die Auslagerung von Unternehmensbereichen kann auf billigere Arbeitskraft, bessere Standortbedingungen (z.B. Steuern) und andere Vorteile zurückgegriffen werden. So kann entweder das gesamte Produkt im Ausland produziert werden, ein Teil davon oder eine dazugehörige Dienstleistung dort erbracht werden. Viele Firmen haben zum Beispiel ihre Call-Centren und Programmierungsstätten in Indien.
    Die Globalisierung bietet aber auch Vorteile für Unternehmen, die nicht Teile ihres Unternehmens auslagern: Zwischenprodukte und Rohstoffe können weltweit zu den günstigsten Preisen eingekauft werden. So werden arbeitsintensive Vorprodukte häufig günstig aus Ländern mit niedrigeren Lohnkosten eingekauft. Dies hat in den alten Industrieländern einen Strukturwandel hin zu technologieintensiveren Sektoren verursacht, die den Einsatz von immer mehr qualifizierter Arbeit erfordern.
    Insbesondere Unternehmen, die stark auf Export fokussiert sind, erfahren durch die Globalisierung und Öffnung der Märkte eine Erweiterung ihres Absatzmarkts. Durch die Möglichkeit, überall hin auf der Welt zu exportieren, erweitert sich das Potenzial an neuen Kundinnen und Kunden, die Interesse an ihrem Produkt haben könnten. Das steigert demnach den Verkauf und treibt den Umsatz in die Höhe. Deutschland hat auf diese Weise massiv von der Globalisierung profitiert.
    Kleine Unternehmen haben es jedoch schwer, sich gegen die riesige Konkurrenz und den damit verbundenen Preisdruck durchzusetzen. Globale Unternehmen, wie beispielsweise IKEA, haben ihre Filialen mittlerweile auf der ganzen Welt. Dies steigert den Druck für Einzelunternehmen.

| Vorteile | Nachteile |
| -------- | --------- |
| Größere Absatzmärkte  | Größere (Globale) Konkurrenz  |
| Mehr Auswahl an Produktionsstandorten   | Komplexere empfindliche Lieferketten |
| Mehr / Günstige Rohstoffe | Teurere Rationalisierung  |
| Geringere Einkaufspreise | Internationale Krisen |
| Einfacherer Transit von Gütern | Abhängigkeit von anderen Ländern |
| Wirtschaftswachstum | |
| Möglichkeit im Ausland zu produzieren | |

### Aus Sicht von Konsumenten

!!! quote ""
    Durch die Globalisierung verwischen die nationalen Grenzen immer weiter. Preiswerter Transport und die einfache Kommunikation erlauben das globale Einkaufen, beispielsweise über das Internet. Aber was bedeutet diese Entwicklung konkret für die Konsumenten?
    Bananen aus Brasilien, Mangos aus Guatemala, Handys aus Finnland, Spielzeug ausChina, all das macht die Globalisierung möglich, zu einem günstigen Preis. Im Internet kann man sozusagen „grenzenlos“ shoppen. Man ist also als Konsument nicht mehr nur auf die Produkte und Dienstleistungen aus seinem eigenen Land beschränkt, sondern kann Waren und Services von überall auf der Welt kaufen. Das hat natürlich den Vorteil, dass man sich den besten Preis für die beste Qualität selbst aussuchen kann. Und weil die Preise transparent sind – also alle, auch die Konkurrenten, diese Preise sehen können – ist der Preisdruck für alle Produzenten besonders hoch.
    Zwar steigert die Globalisierung das Angebot an verfügbaren Gütern und Dienstleistungen, und das zu immer besseren Preisen durch einen starken Konkurrenzkampf. Jedoch hat die Globalisierung auch Schattenseiten. Denn immer mehr Unternehmen verlagern ihre Produktion ins Ausland, wo sie niedrigere Lohnkosten bezahlen, um im weltweiten Wettbewerb nicht zu verlieren. Viele Menschen mit geringen Qualifikationen verlieren in den alten Industrieländern daher durch die Globalisierung ihre bisherige Arbeit. Dass nun technologieintensivere Sektoren in den Industrienationen im Vordergrund stehen, ist die Folge des Strukturwandels, der sich durch die Globalisierung ergeben hat. Diese „neuen“ Sektoren erfordern eine höhere Qualifikation und mehr KnowHow, die man in den „Billiglohnländern“ nicht finden kann. Somit ist die Qualifikation der Arbeitskräfte besonders wichtig. Aus- und Weiterbildung verringern die Zahl der Geringqualifizierten, die von den negativen Folgen der Globalisierung besonders betroffen sind.
    Ein anderes Problem der Globalisierung ist, dass es für die Unternehmen meist viele Vorteile bringt, Teile der Produktion ins Ausland zu verlagern oder von dorther Zwischenprodukte günstig einzukaufen. Aber häufig werden in diesen Ländern gewisse Standards (z.B. Umweltregeln und Sozialstandards) nicht beachtet. Zudem sind die Bedingungen, unter denen die Menschen in den Produzentenländern arbeiten müssen, teilweise sehr schlecht.

| Vorteile | Nachteile |
| -------- | --------- |
| Kommunikation in die ganze Welt | Niedriglohnarbeiter verlieren ggfs. den Job > Höhere Anforderungen an Qualifikationen in Industrieländern > Auslagerung von Arbeitsplätzen in andere Länder |
| Güter/Dienstleistungen können von überall aus gesichtet und bestellt werden | Eventuell hohe Transport-/Versandkosten |
| Preise näher am Gleichgewichtspreis, da oft viele Anbieter gleicher Produkte (Konkurrenzkampf) und transparenter Preise | Um niderige Preise zu garantieren werden z.B. Umweltregeln und Sozialstandards nicht beachtet / umgangen. |
| Exorbitante Auswahl an Gütern unterschiedlicher Beschaffenheit und Qualität | Unterschiedliche Standards|
| Keine hoher Transportaufwand (Selbstaufwamd), da viele Güter bis vor die Haustür geliefert werden können | Schlechte Arbeitsbedingungen (in Produzentenländern) |

### Auswirkungen auf Schwellenländer

## Weiterführende Links

* [Wikipedia](https://de.wikipedia.org/wiki/Globalisierung)
* [Globalisierung Vor- und Nachteile](https://studyflix.de/wirtschaft/globalisierung-vor-und-nachteile-1909)
* [Globalisierungsindikatoren](https://www.destatis.de/DE/Themen/Wirtschaft/Globalisierungsindikatoren/_inhalt.html)

### Videos

* [Globalisierung einfach erklärt - explainity ® Erklärvideos](https://www.youtube.com/watch?v=aGPABEnTG0g&feature=emb_imp_woyt)
* [Globalisierung: Grundbegriffe der Wirtschaft - simpleclub](https://www.youtube.com/watch?v=CpefvYBMhPE)
* [Globalisierung unter neuen Vorzeichen - bpb](https://www.youtube.com/watch?v=4QQOfP-awNY)
* [Alles was DU zur Globalisierung wissen musst! Dimensionen & Auswirkungen der Globalisierung erklärt - selbstorientiert](https://www.youtube.com/watch?v=hEFNQ6RsSYw)

## Aktuelles

* [Folge des Ukraine-Kriegs: Kommt jetzt die Deglobalisierung?](https://www.zdf.de/nachrichten/wirtschaft/deglobalisierung-ukraine-krieg-russland-100.html)