---
tags:
- FISI
- FIAE
---

# Demografischer Wandel

!!! note "Definition Demografische Entwicklung"

    Die Demografische Entwicklung ist die Entwicklung der Zahl der Bevölkerung und ihrer Altersstruktur.
    Die demografische Entwicklung ist vor allem von der Lebenserwartung und der Zahl der Geburten, aber auch von Faktoren wie Ein- und Auswanderung abhängig.

## Demografische Entwicklung in Deutschland

<img src="../../img/bevoelkerungspyramide_1980.png" alt="Bevölkerungspyramide von 1980" width="48%">
<img src="../../img/bevoelkerungspyramide_2023.png" alt="Bevölkerungspyramide von 1980" width="48%">

[Bevölkerungspyramide selber testen](https://service.destatis.de/bevoelkerungspyramide/#!y=2023)

Die **niedrige Geburtenrate** ist der Faktor, der die **demografische Entwicklung** in Deutschland am stärksten beeinflusst.
Sie führt zu einem Schrumpfen der Bevölkerung und langfristig dazu, dass der Anteil der Jüngeren in der Gesellschaft sinkt.
2,1 Kinder je Frau sind nötig, um die Bevölkerungszahl stabil zu halten.
Seit vierzig Jahren liegt die Zahl darunter, derzeit bei 1,5 Kindern.
Jede vierte Frau bleibt kinderlos.  
Der zweite Faktor ist die **steigende Lebenserwartung**:
Sie bremst den Rückgang der Bevölkerung, aber sie verschiebt zugleich die Alterspyramide und das Durchschnittsalter nach oben.
Es gibt mehr Alte und weniger Junge.  
Die **gestiegene Einwanderung** ist der dritte Faktor:
Zuzug aus dem Ausland kann den Bevölkerungsrückgang bremsen.
Ohne die Zuwanderung der vergangenen Jahrzehnte würden in Deutschland heute viel weniger Menschen leben.
Ein Drittel der Grundschüler hat einen Migrationshintergrund.
Das bedeutet, dass ihre Eltern nach Deutschland eingewandert sind oder die Schüler selbst im Ausland geboren wurden.  
Wie viele Menschen jährlich einwandern müssen, um den Bevölkerungsrückgang auszugleichen, ist allerdings umstritten.
Die Schätzungen reichen von 200 000 bis 500 000 Menschen - nach Abzug derer, die Deutschland verlassen.
So hoch ist die Einwanderung aber nur in Ausnahmefällen, z.B. wenn viele Flüchtlinge kommen.

### Probleme der aktuellen Entwicklung

Die ersten beiden Faktoren (zu wenig Geburten, steigende Lebenserwartung) führen zu **finanziellen Problemen in der Rentenversicherung**.
Die gesetzliche Rentenversicherung ist nähmlich nach dem Grundsatz des **Generationenvertrags** organisiert:
Die Erwachsenen sorgen nicht nur für Erziehung und Unterhalt ihrer Kinder, sondern finanzieren als Arbeitnehmer durch ihre Beiträge zur Rentenversicherung außerdem den Lebensunterhalt der Rentner.
Damit die Belastung für den einzelnen Arbeitnehmer nicht zu groß wird, müssen genügend Beitragszahler vorhanden sein.  
Die derzeitige demografische Entwicklung bedeutet aber:
Die Zahl der Rentner steigt, die Zahl der Beitragszahler sinkt.
Um die Belastung der arbeitenden Generation zu begrenzen, sind die Renten gesenkt worden.
Wer vorzeitig in Rente geht, muss höhere Abschläge in Kauf nehmen.
Zugleich wurde das Rentenalter heraufgesetzt.
Die sinkenden Renten machen eine zusätliche private Vorsorge des Einzelnen für sein Alter nötig.
Diese Vorsorge wird vom Staat gefördert.
Allerdings legen nur wenige so viel Geld zurück, dass sie davon im Alter sorgenfrei leben können.
Auch die niedrigen Zinsen machen die Altersvorsorge schwieriger und teurer.  
Zuwanderung von Arbeitnehmern kann dieses Problem entschärfen.
Die hohe Einwanderung von Flüchtlingen führt zunächst aber nicht zu einer Entlastung der Rentenversicherung - im Gegenteil.
Flüchtlinge sind noch keine Arbeitskräfte.
Und auch bei denen, die bleiben und arbeiten dürfen, kann es viele Jahre dauren, bis sie selber für ihren Lebensunterhalt sorgen können.
So lange sind sie eine **Last für den Sozialstaat**.
Ihre Integration kostet viel Geld.