---
tags:
- FISI
- FIAE
---

# Demokratie

!!! note "Definition Demokratie"
    Das Wort Demokratie kommt aus dem Griechischen und bedeutet Volksherrschaft.
    Nicht ein Einzelner - z.B. ein König - soll die politischen Entscheidungen treffen, sondern das Volk.
    Man spricht deshalb von **Volkssouveränität**.

![Mindmap zur Demokratie](../img/demokratie_mindmap.png)

## Arten

### Direkte oder repräsentative Demokratie?

Im antiken Griechenland bedeutete Demokratie tatsächlich, dass die Bürger - das Bürgerrecht besaßen nur wenige tausend Männer - in den Stadtstaaten direkt an politischen Beratungen und Entscheidungen beteiligt waren. Sie entschieden selbst über Recht und Gesetz.
Das funktioniert in modernen Staaten mit Millionen von Einwohnern nicht mehr. Heutige Verfassungen gehen deshalb einen anderen Weg, den der repräsentativen Demokratie.
In ihr entscheiden die Bürgerinnen und Bürger in Wahlen, durch wen sie in den Parlamenten vertreten sein wollen. Sie wählen Abgeordnete auf Zeit.

### Regeln der repräsentativen Demokratie

In der repräsentativen Demokratie überträgt das Volk seine Herrschaft auf Abgeordnete.
Damit diese ihre Macht nicht missbrauchen, braucht es Regeln.
Diese Regeln sind:

* Wer das Volk vertritt, wird durch **allgemeine Wahlen** bestimmt.
* Die Macht wird nur auf Zeit übertragen. In überschaubaren Zeitabständen (meist 4-5 Jahre) werden neue Repräsentanten gewählt. Die Abgeordneten werden sozusagen an der Leine geführt, die alle vier Jahre eingeholt wird. Das zwingt die Abgeordneten auch, den Wählern Rechenschaft über ihre Arbeit abzulegen und sie von ihren Leistungen zu überzeugen.
* In der **parlamentarischen Demokratie** wählen die Abgeordneten auch die Regierung. Mit der Wahl der Abgeordneten wird gleichzeitig entschieden, wer regiert.
* In modernen Demokratien sind die Abgeordneten in Parteien organisiert. Die Wählerinnen und Wähler orientieren sich bei ihrer Stimmabgabe meist nicht mehr an der Persönlichkeit der Kandidaten, sondern an den Parteien und ihren Zielen: **Parteiendemokratie**.

### Wem sollen die Abgeordneten gehorchen?

Abgeordnete sind Vertreter ihrer Wähler.
Es liegt daher nahe, dass sie keinen eigenen Handlungsspielraum haben, sondern im Auftrag der Wähler handeln: **imperatives [Mandat](https://de.wikipedia.org/wiki/Mandat_(Politik) "Sitz im Parlament")**.
Das Grundgesetz erteilt dieser Vorstellung eine klare Absage.
Es bestimmt, dass die Abgeordneten ein **freies Mandat** haben.
Sie sind an Weisungen nicht gebunden.  
Das freie Mandat ist ein Leitbild.
Die Praxis zeigt, dass Abgeordnete immer vielfältigen Einflüssen ausgesetzt sind.
Ein Abgeordneter allein kann bei parlamentarischen Entscheidungen wenig ausrichten.
Einfluss gewinnt ein Abgeordneter vor allem als Mitglied seiner Fraktion.  
Die Fraktionen sind daran interessiert, möglichst geschlossen aufzutreten.
Das gilt vor allem für die Gesetzgebung. Für die Abgeordneten kann diese **Fraktionsdisziplin** auch ein Fraktionszwang sein.  
Bei Entscheidungen, bei denen es um klare Gewissensfragen geht, geben die Fraktionen die Abstimmung frei.
Dies war z.B. beim Embryonenschutzgesetz der Fall.

#### Imperatives vs. freies Mandat

| Nachteile des imperativen Mandats | Nachteile des freien Mandats |
| --------------------------------- | ---------------------------- |
| Der Abgeordnete vertritt nur die Mehrheit im Wahlkreis. Minderheiten werden nicht repräsentiert. | Keine Rechenschaftspflicht des Abgeordneten gegenüber den Wählern. |
| Auch zwischen den Wahlen sind ständige Abstimmungen im Wahlkreis notwendig, damit der Abgeordnete weiß, was er zu tun hat. | Entfremdung zwischen Abgeordneten und Wählern. |
| Je nach Stimmung in der Bevölkerung muss der Abgeordnete mal so, mal anders entscheiden. | Möglichkeit des Mandatsmissbrauchs, z.B. wenn der Abgeordnete aus der Partei austritt, der er seinen Sitz im Parlament verdankt. |
| Unpopuläre, aber notwendige Maßnahmen können nicht umgesetzt werden. | Das Prinzip des freien Mandats wird beim Verbot verfassungsfeindlicher Parteien durchbrochen: Die Abgeordneten verlieren ihr Mandat. |

## Gefahren für die Demokratie

### Populismus

!!! note "Definition Populismus"
    Populismus ist eine von Opportunismus geprägte, volksnahe, oft demagogische Politik, die darauf abzielt, durch Dramatisierung der politischen Lage die Gunst der Massen (im Hinblick auf Wahlen) zu gewinnen.

![Karikatur zum Populismus](../img/populismus_demokratie.jpg)

[Linkspopulismus](https://www.bpb.de/themen/medien-journalismus/netzdebatte/261244/linkspopulismus-ein-vernachlaessigtes-phaenomen/)

### Extremismus

!!! note "Definition Extremismus"
    Extremismus ist eine extreme, radikale (politische) Einstellung oder Richtung.

## Verfassungsorgane in Deutschland

![Übersicht Verfassungsorgane](../img/verfassungsorgane_de.jpg)

Im Folgenden werden die fünf **[ständigen Verfassungsorgane](https://www.bpb.de/kurz-knapp/lexika/lexikon-in-einfacher-sprache/250071/verfassungsorgane/ "Ständige Verfassungsorgane muss es immer geben")** beschrieben.
Es gibt auch Verfassungsorgane, die nicht immer vorhanden sein müssen.
Ein Beispiel hierfür ist die Bundesversammlung. Die Bundesversammlung gibt es nur, wenn der Bundespräsident gewählt werden muss.

### Bundesverfassungsgericht

Oberstes deutsches Gericht, das darüber wacht, dass sich Parlamente, Regierungen und Gerichte in Deutschland an das Grundgesetz halten.
Es kann zum Beispiel Gesetze aufheben, wenn sie verfassungswidrig sind.
Bevor das Gericht aktiv wird, muss es offiziell angerufen werden, zum Beispiel durch eine Verfassungsbeschwerde.
Die Richterinnen und Richter werden je zur Hälfte vom Bundestag und vom Bundesrat gewählt.
Der Bundespräsident ernennt sie.
Das Bundesverfassungsgericht ist Teil der rechtsprechenden Gewalt.
### Bundesrat

Vertretung der Länder.
Der Bundesrat wirkt bei der Gesetzgebung und Verwaltung des Bundes mit.
Er hat bei Bundesgesetzen ein Mitberatungs- oder Mitentscheidungsrecht.
In welchen Bereichen er mitentscheidet, regelt das Grundgesetz.
Der Bundesrat setzt sich aus Mitgliedern der 16 Landesregierungen zusammen.
Er ist Teil der Legislative.

### Bundespräsident

Das Staatsoberhaupt der Bundesrepublik Deutschland.
Der Bundespräsident repräsentiert den Staat nach innen und außen, hat aber keine Entscheidungsbefugnisse.
Er fertigt die vom Bundestag beschlossenen Gesetze aus und schlägt dem Bundestag die Bundeskanzlerin oder den Bundeskanzler zur Wahl vor.
Auf Vorschlag der Bundeskanzlerin oder des Bundeskanzlers ernennt er die Bundesministerinnen und Bundesminister.
Der Bundespräsident ist Teil der Exekutive.

### Bundestag

Der Deutsche Bundestag ist das Parlament der Bundesrepublik Deutschland und das einzige direkt vom Volk gewählte Verfassungsorgan.
Der Bundestag wählt den Bundeskanzler, berät und beschließt Bundesgesetze und kontrolliert die Arbeit der Bundesregierung.
Er gehört zur Legislative.

### Bundesregierung

Die Bundesregierung ist das verfassungsmäßige Organ der Staatsleitung.
Sie besteht aus der [Bundeskanzlerin](./#Bundeskanzler), die die Richtlinien der Politik bestimmt, und den Bundesministerinnen und Bundesministern, die an diese Richtlinien gebunden sind, im Übrigen aber ihre Bundesministerien selbständig leiten.
Die Bundesregierung ist Teil der vollziehenden Gewalt

## Bundeskanzler*in

<table>
    <caption>Bundeskanzler der Bundesrepublik Deutschland</caption>
    <thead>
        <tr>
            <th colspan="2">Nr.</th>
            <th>Bild</th>
            <th>Name (Lebensdaten)</th>
            <th>Partei</th>
            <th>Amtsantritt</th>
            <th>Ende der Amtszeit</th>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="background:black" width="1"></td>
            <td style="text-align:center">1</td>
            <td align="center"><img alt="Konrad Adenauer" src="../../img/kanzler/konrad_adenauer.jpg" decoding="async" width="70" height="105"></td>
            <td><a href="https://de.wikipedia.org/wiki/Konrad_Adenauer" title="Konrad Adenauer">Konrad Adenauer</a><p>(1876–1967)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Christlich_Demokratische_Union_Deutschlands" title="Christlich Demokratische Union Deutschlands">CDU</a></td>
            <td style="text-align:right">15.&nbsp;September 1949</td>
            <td style="text-align:right">16. Oktober 1963</td>
        </tr>
        <tr>
            <td style="background:black" width="1"></td>
            <td style="text-align:center">2</td>
            <td align="center"><img alt="Ludwig Erhard" src="../../img/kanzler/ludwig_erhard.jpg" decoding="async" width="70" height="112"></td>
            <td><a href="https://de.wikipedia.org/wiki/Ludwig_Erhard" title="Ludwig Erhard">Ludwig Erhard</a><p>(1897–1977)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Christlich_Demokratische_Union_Deutschlands" title="Christlich Demokratische Union Deutschlands">CDU</a></td>
            <td style="text-align:right">16. Oktober 1963</td>
            <td style="text-align:right">1. Dezember 1966</td>
        </tr>
        <tr>
            <td style="background:black" width="1"></td>
            <td style="text-align:center">3</td>
            <td align="center"><img alt="Kurt Georg Kiesinger" src="../../img/kanzler/kurt_georg_kiesinger.jpg" decoding="async" width="70" height="93"></td>
            <td><a href="https://de.wikipedia.org/wiki/Kurt_Georg_Kiesinger" title="Kurt Georg Kiesinger">Kurt Georg Kiesinger</a><p>(1904–1988)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Christlich_Demokratische_Union_Deutschlands" title="Christlich Demokratische Union Deutschlands">CDU</a></td>
            <td style="text-align:right">1. Dezember 1966</td>
            <td style="text-align:right">21. Oktober 1969</td>
        </tr>
        <tr>
            <td style="background:#f00" width="1"></td>
            <td style="text-align:center">4</td>
            <td align="center"><img alt="Willy Brandt" src="../../img/kanzler/willy_brandt.jpg" decoding="async" width="70" height="98"></td>
            <td><a href="https://de.wikipedia.org/wiki/Willy_Brandt" title="Willy Brandt">Willy Brandt</a><p>(1913–1992)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Sozialdemokratische_Partei_Deutschlands" title="Sozialdemokratische Partei Deutschlands">SPD</a></td>
            <td style="text-align:right">21. Oktober 1969</td>
            <td style="text-align:right">7. Mai 1974</td>
        </tr>
        <tr>
            <td style="background:#f00" width="1"></td>
            <td style="text-align:center">5</td>
            <td align="center"><img alt="Helmut Schmidt" src="../../img/kanzler/helmut_schmidt.jpg" decoding="async" width="70" height="99"></td>
            <td><a href="https://de.wikipedia.org/wiki/Helmut_Schmidt" title="Helmut Schmidt">Helmut Schmidt</a><p>(1918–2015)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Sozialdemokratische_Partei_Deutschlands" title="Sozialdemokratische Partei Deutschlands">SPD</a></td>
            <td style="text-align:right">16. Mai 1974</td>
            <td style="text-align:right">1. Oktober 1982</td>
        </tr>
        <tr>
            <td style="background:black" width="1"></td>
            <td style="text-align:center">6</td>
            <td align="center"><img alt="Helmut Kohl" src="../../img/kanzler/helmut_kohl.jpg" decoding="async" width="70" height="89"></td>
            <td><a href="https://de.wikipedia.org/wiki/Helmut_Kohl" title="Helmut Kohl">Helmut Kohl</a><p>(1930–2017)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Christlich_Demokratische_Union_Deutschlands" class="mw-redirect" title="Christlich Demokratische Union">CDU</a></td>
            <td style="text-align:right">1. Oktober 1982</td>
            <td style="text-align:right">27. Oktober 1998</td>
        </tr>
        <tr>
            <td style="background:#f00" width="1"></td>
            <td style="text-align:center">7</td>
            <td align="center"><img alt="Gerhard Schröder" src="../../img/kanzler/gerhard_schroeder.jpg" decoding="async" width="70" height="101"></td>
            <td><a href="https://de.wikipedia.org/wiki/Gerhard_Schr%C3%B6der" title="Gerhard Schröder">Gerhard Schröder</a><p>(* 1944)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Sozialdemokratische_Partei_Deutschlands" title="Sozialdemokratische Partei Deutschlands">SPD</a></td>
            <td style="text-align:right">27. Oktober 1998</td>
            <td style="text-align:right">22. November 2005</td>
        </tr>
        <tr>
            <td style="background:black" width="1"></td>
            <td style="text-align:center">8</td>
            <td align="center"><img alt="Angela Merkel" src="../../img/kanzler/angela_merkel.jpg" decoding="async" width="70" height="93"></td>
            <td><a href="https://de.wikipedia.org/wiki/Angela_Merkel" title="Angela Merkel">Angela Merkel</a><p>(* 1954)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Christlich_Demokratische_Union_Deutschlands" title="Christlich Demokratische Union Deutschlands">CDU</a></td>
            <td style="text-align:right">22. November 2005</td>
            <td style="text-align:right">8. Dezember 2021</td>
        </tr>
        <tr>
            <td style="background:#f00" width="1"></td>
            <td style="text-align:center">9</td>
            <td align="center"><img alt="Olaf Scholz" src="../../img/kanzler/olaf_scholz.jpg" decoding="async" width="70" height="95"></td>
            <td><a href="https://de.wikipedia.org/wiki/Olaf_Scholz" title="Olaf Scholz">Olaf Scholz</a><p>(* 1958)</p></td>
            <td><a href="https://de.wikipedia.org/wiki/Sozialdemokratische_Partei_Deutschlands" title="Sozialdemokratische Partei Deutschlands">SPD</a></td>
            <td style="text-align:right">8. Dezember 2021</td>
            <td style="text-align:right">im Amt</td>
        </tr>
    </tbody>
    <tfoot></tfoot>
</table>

### Aufgaben 

* **Richlinienkompetenz**, d.h er legt die politischen Richlinien der Bundesregierung fest
* Ernennt einen Bundesminister zum Stellvertreter
* Im Verteidigungsfall hat der Bundekanzler die Befehls- und Kommandogewalt über die Armee

## Wahlen

### Bedeutung von Wahlen

* Die Wahl ist das Medium, durch das die politische Selbstbestimmung des Volkes verwirklicht wird.
* Ermöglicht den Menschen, unabhängig von ihrem sozialen Status, Einfluss auf die politische Landschaft zu nehmen.
* Legitimation der Regierenden

//TODO Schaubild

### Wahlgrundsätze der BRD

!!! info "Wichtig"
    Die Abgeordneten des Deutschen Bundestages werden in **allgemeiner**, **unmittelbarer**, **freier**, **gleicher** und **geheimer** Wahl gewählt.

Die Bundestagswahl ist eine **allgemeine** Wahl, weil alle Bürgerinnen und Bürger der Bundesrepublik Deutschland wahlberechtigt sind - unabhängig von Geschlecht, Einkommen, Konfession, Beruf oder politischer Überzeugung.
Das aktive Wahlrecht wird mit Vollendung des 18.  
Auslandsdeutsche sind wahlberechtigt, wenn sie nach Vollendung des 14. Lebensjahres mindestens drei Monate ununterbrochen in Deutschland gelebt haben und dieser Aufenthalt nicht länger als 25 Jahre zurückliegt.

**Unmittelbare** Wahlen sind Wahlen, bei denen die Wählerinnen und Wähler die Abgeordneten direkt (unmittelbar) wählen.
In Deutschland gibt es keine „Wahlmänner“, die - wie in den USA - als Zwischeninstanz fungieren und denen man seine Stimme überträgt.

Eine Wahl ist **frei**, wenn die Bürgerinnen und Bürger in ihrer Wahlentscheidung nicht beeinflusst oder unter Druck gesetzt werden.
Der Grundsatz der Freiheit der Wahl gewährleistet, dass die Wählerinnen und Wähler ihren wahren Willen unverfälscht zum Ausdruck bringen und ihr Wahlrecht ohne Zwang oder sonstige unzulässige Einflussnahme von außen ausüben können.
Dazu gehört auch, dass es keinen Wahlzwang gibt und es jedem Bürger freisteht, an einer Wahl teilzunehmen.
**Gleich" ist eine Wahl, weil jede Stimme gleich viel zählt und jede Art von Gewichtung unzulässig ist.
Oder wie es im Englischen so schön heißt: One man - one vote.

**Geheim** ist eine Wahl, wenn sichergestellt ist, dass der Wähler oder die Wählerin den Stimmzettel unbeobachtet ankreuzen kann.
Die Stimmabgabe erfolgt in Wahlkabinen.
Diese sind von außen nicht einsehbar.
Die ausgefüllten Stimmzettel werden gefaltet in die Wahlurne geworfen.
So kann niemand erkennen, welche Wahlentscheidung der Wähler oder die Wählerin getroffen hat.  
In Deutschland ist auch die Briefwahl möglich, die allerdings beantragt werden muss.
Die Stimme kann dann per Post abgegeben werden.
Dies ermöglicht kranken, behinderten oder sonst am Wahltag verhinderten Personen, ihr Wahlrecht auszuüben.